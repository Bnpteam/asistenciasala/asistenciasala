package pe.gob.bnp.absysnet.ejb.service.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.salalectura.dto.LectorAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorConsultaAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;
import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;

@Local
public interface AbsysnetServiceLocal extends GenericService<LectorAbsysnet>{
	public ResponseObject guardarLector(LectorAbsysnet lector) throws ServiceException;
	public ResponseObject cambiarEstadoLector(LectorAbsysnet lector) throws ServiceException;
	
	//API ASISTENCIA A SALA
	public List<LectorConsultaAbsysnetDto> buscarUsuarios(String numeroDocIdentidad) throws ServiceException;	
	public LectorAbsysnetDto obtenerLectorAbsysnet(Long codBarrasLector) throws ServiceException;
	public ResponseObject actualizarLectorMigradoASIRU(Long codBarrasLector) throws ServiceException;
}
