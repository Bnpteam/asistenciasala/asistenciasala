package pe.gob.bnp.absysnet.ejb.service.remote;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.service.base.GenericService;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.salalectura.dto.LectorAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorConsultaAbsysnetDto;

@Remote
public interface AbsysnetServiceRemote extends GenericService<LectorAbsysnet>{
	public ResponseObject guardarLector(LectorAbsysnet lector) throws ServiceException;
	public ResponseObject cambiarEstadoLector(LectorAbsysnet lector) throws ServiceException;
	
	//API ASISTENCIA SALA
	public List<LectorConsultaAbsysnetDto> buscarUsuarios(String numeroDocIdentidad) throws ServiceException;
	public LectorAbsysnetDto obtenerLectorAbsysnet(Long codBarrasLector) throws ServiceException;
	public ResponseObject actualizarLectorMigradoASIRU(Long codBarrasLector) throws ServiceException;
}
