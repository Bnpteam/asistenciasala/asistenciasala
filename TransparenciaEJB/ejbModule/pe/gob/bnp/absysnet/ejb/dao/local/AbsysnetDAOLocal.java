package pe.gob.bnp.absysnet.ejb.dao.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.dao.base.GenericDAO;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.salalectura.dto.LectorAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorConsultaAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;

@Local
public interface AbsysnetDAOLocal extends GenericDAO<LectorAbsysnet>  {
	public ResponseObject guardarLector(LectorAbsysnet lector) throws PersistenceException;
	public ResponseObject cambiarEstadoLector(LectorAbsysnet lector) throws PersistenceException;
	
	//API ASISTENCIA A SALA
	public List<LectorConsultaAbsysnetDto> buscarUsuarios(String numeroDocIdentidad) throws PersistenceException;
	public LectorAbsysnetDto obtenerLectorAbsysnet(Long codBarrasLector) throws PersistenceException;
	public ResponseObject actualizarLectorMigradoASIRU(Long codBarrasLector) throws PersistenceException;

}
