package pe.gob.bnp.absysnet.ejb.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.absysnet.ejb.dao.local.AbsysnetDAOLocal;
import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.salalectura.dto.LectorAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorConsultaAbsysnetDto;
import pe.gob.servir.systems.util.sql.Conexion;
import pe.gob.servir.systems.util.validator.VO;

@Stateless
public class AbsysnetDAO extends GenericDAOImpl<LectorAbsysnet> implements AbsysnetDAOLocal {
	
	private static final Logger logger = Logger.getLogger(AbsysnetDAO.class);
	
	@PersistenceContext(unitName = "PUAbsysnet")
	private EntityManager em;

	@Override
	public ResponseObject guardarLector(final LectorAbsysnet lector) throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_SIRU.SP_GUARDAR_LECTOR(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
						cstm.setString("E_COD_BIBLIOTECA",lector.getCodBiblioteca() );
						cstm.setString("E_COD_SUCURSAL",lector.getCodSucursal() );
						cstm.setString("E_ASIGNADO_BIBL",lector.getFlagAsignadoBibl() );
						cstm.setString("E_FLAG_ADULTO",lector.getEsAdulto() );
						cstm.setString("E_ESTADO",lector.getEstado() );
						cstm.setDate("E_FECHA_INICIO",VO.getSQLDate(lector.getFechaInicio()));
						cstm.setDate("E_FECHA_FIN",VO.getSQLDate(lector.getFechaFin()));
						cstm.setString("E_TIPO_DOC_IDENT",lector.getTipoDocIdentidad() );
						cstm.setString("E_NUM_DOC_IDENT",lector.getNumDocIdentidad() );
						cstm.setString("E_NOMBRES",lector.getNombres() );
						cstm.setString("E_APELLIDOS",lector.getApellidos() );
						cstm.setDate("E_FECHA_NACIMIENTO",VO.getSQLDate(lector.getFechaNacimiento()));
						cstm.setString("E_SEXO",lector.getSexo() );
						cstm.setString("E_DIRECC_LECTOR",lector.getDireccion() );
						cstm.setString("E_TELF_FIJO",lector.getTelefonoFijo() );
						cstm.setString("E_TELF_MOVIL",lector.getTelefonoMovil() );
						cstm.setString("E_CORREO",lector.getCorreo() );
						cstm.setString("E_CENTRO_ESTUDIOS",lector.getCentroEstudios() );
						cstm.setString("E_DIRECC_CENT_ESTUD",lector.getDireccionCentroEstudios() );
						cstm.setString("E_INSTITUCION_AVAL",lector.getInstitucionAval() );
						cstm.setString("E_TEMA_INVESTIG",lector.getTemaInvestigacion() );
						cstm.setString("E_TIPO_LECTOR",lector.getTipoLector() );
						cstm.setString("E_TIPO_CARNET",lector.getTipoCarnet() );
						cstm.setString("E_DEPART_PROV_LECTOR",lector.getDepartProvincia());
						cstm.setString("E_DISTRITO_LECTOR",lector.getDistrito());
						cstm.setString("E_NIVEL_ESTUDIO",lector.getNivelEstudio());
						cstm.setLong("E_COD_BARRAS_LECTOR",lector.getCodBarrasLector());
						cstm.setString("E_TELF_CENTRO_ESTUDIO",lector.getTelefonoCentroEstudio());
						cstm.setString("E_COD_PAIS",lector.getCodPais());
						cstm.setDate("E_FECHA_SUSPENSION",VO.getSQLDate(lector.getFechaSuspension()));

						cstm.registerOutParameter("MENSAJE", OracleTypes.VARCHAR);	
						cstm.registerOutParameter("S_COD_BARRAS_LECTOR", OracleTypes.NUMBER);

						cstm.execute();
						Object object=cstm.getObject("S_COD_BARRAS_LECTOR");
						BigDecimal decId= new BigDecimal(0);			
						
						String resultado = cstm.getString("MENSAJE");
						lector.setMensajeRespuesta(resultado);
												
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
						Long codBarrasAbsysnet = decId.longValue();
						if (codBarrasAbsysnet!=null && codBarrasAbsysnet >0L) {
							cn.commit();
							lector.setCodBarrasLector(codBarrasAbsysnet);
						}else {
							cn.rollback();
						}
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:AbsysnetDAO->guardarLector:PKG_SIRU.SP_GUARDAR_LECTOR "+ex);
						e.printStackTrace();
						cn.rollback();
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenceException("SQLException:AbsysnetDAO->guardarLector:PKG_SIRU.SP_GUARDAR_LECTOR "+e.getMessage());
		}
		result.setId(lector.getCodBarrasLector());
		result.setMensaje(lector.getMensajeRespuesta());	
		return result;	
	}

	@Override
	public ResponseObject cambiarEstadoLector(final LectorAbsysnet lector) throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_SIRU.SP_CAMBIAR_ESTADO_LECTOR(?,?,?,?,?,?,?,?) }");
						cstm.setLong("E_COD_BARRAS_LECTOR",lector.getCodBarrasLector() );
						cstm.setString("E_TIPO_DOC_IDENT",lector.getTipoDocIdentidad() );
						cstm.setString("E_NUM_DOC_IDENT",lector.getNumDocIdentidad() );
						cstm.setString("E_ESTADO",lector.getEstado() );
						cstm.setDate("E_FECHA_INICIO",VO.getSQLDate(lector.getFechaInicio()) );
						cstm.setDate("E_FECHA_FIN",VO.getSQLDate(lector.getFechaFin()) );
						cstm.setDate("E_FECHA_SUSPENSION",VO.getSQLDate(lector.getFechaSuspension()));

						cstm.registerOutParameter("MENSAJE", OracleTypes.VARCHAR);	
						cstm.execute();	
						String resultado = VO.getString(cstm.getString("MENSAJE"));
						lector.setMensajeRespuesta(resultado);

						if (resultado.equals("EXITO")) {
							cn.commit();
						}else {
							cn.rollback();
						}
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:AbsysnetDAO->cambiarEstadoLector:PKG_SIRU.SP_CAMBIAR_ESTADO_LECTOR "+ex);
						e.printStackTrace();
						cn.rollback();
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenceException("SQLException:AbsysnetDAO->cambiarEstadoLector:PKG_SIRU.SP_CAMBIAR_ESTADO_LECTOR "+e.getMessage());
		}
		result.setId(lector.getCodBarrasLector());
		result.setMensaje(lector.getMensajeRespuesta());	
		return result;	
	}

	@Override
	public ResponseObject insert(LectorAbsysnet entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(LectorAbsysnet entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(LectorAbsysnet entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LectorAbsysnet> list(LectorAbsysnet entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LectorAbsysnet findById(LectorAbsysnet entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LectorConsultaAbsysnetDto> buscarUsuarios(final String numeroDocIdentidad) throws PersistenceException {
		final List<LectorConsultaAbsysnetDto> listarpt = new ArrayList<>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_SIRU.SP_BUSC_LECT_DOC_IDENT" + "(?,?) }");
						cstm.setString("E_NUM_DOC_IDENT", numeroDocIdentidad);
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							
							LectorConsultaAbsysnetDto rpt= new LectorConsultaAbsysnetDto();
							rpt.setApellidos(rs.getString("APELLIDOS"));
							rpt.setCodBarrasLector(rs.getLong("COD_BARRAS_LECTOR"));
							rpt.setCorreoElectronico(rs.getString("CORREO_ELECTRONICO"));
							rpt.setEstado(rs.getString("ESTADO"));
							rpt.setFechaCreacion(rs.getString("FECHA_CREACION"));
							rpt.setFechaFin(rs.getString("FECHA_FIN"));
							rpt.setFechaInicio(rs.getString("FECHA_INICIO"));
							rpt.setLencar(rs.getInt("LENCAR"));
							rpt.setNombres(rs.getString("NOMBRE"));
							rpt.setNumeroDocIdentidad(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setTipoDocIdentidad(rs.getString("TIPO_DOCUMENTO_IDENTIDAD"));
							rpt.setTipoUsuario(rs.getString("TIPO_USUARIO_BIBLIOTECA"));
							rpt.setVigencia(rs.getString("VIGENCIA"));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_SIRU.SP_BUSC_LECT_DOC_IDENT "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_SIRU.SP_BUSC_LECT_DOC_IDENT "+e.getMessage());
		}

		return listarpt;
	}

	@Override
	public LectorAbsysnetDto obtenerLectorAbsysnet(final  Long codBarrasLector) throws PersistenceException {
		final LectorAbsysnetDto rpt = new LectorAbsysnetDto();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_SIRU.SP_GET_LECTOR_X_COD_BARRAS" + "(?,?,?) }");
						cstm.setLong("E_LENLEC", codBarrasLector);
						cstm.registerOutParameter("S_COD_MENSAJE",OracleTypes.VARCHAR);
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();
						String resultado =(String)cstm.getObject("S_COD_MENSAJE");
						rpt.setCodResultadoAbsysnet(resultado);						

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {
							rpt.setTipoUsuarioBiblioteca(rs.getString("TIPO_USUARIO_BIBLIOTECA"));
							rpt.setTipoDocumentoIdentidad(rs.getString("TIPO_DOCUMENTO_IDENTIDAD"));
							rpt.setNumeroDocumentoIdentidad(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setNombres(rs.getString("NOMBRES"));
							rpt.setApellidoPaterno(rs.getString("APELLIDO_PATERNO"));
							rpt.setApellidoMaterno(rs.getString("APELLIDO_MATERNO"));
							rpt.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
							rpt.setSexo(rs.getString("SEXO"));
							rpt.setDireccionDomicilio(rs.getString("DIRECCION_DOMICILIO"));
							rpt.setCodPais(rs.getString("COD_PAIS"));
							rpt.setTelefonoFijo(rs.getString("TELEFONO_FIJO"));
							rpt.setTelefonoCelular(rs.getString("TELEFONO_MOVIL"));
							rpt.setCorreoElectronico(rs.getString("CORREO_ELECTRONICO"));
							rpt.setCentroDeEstudios(rs.getString("NOMBRE_CENTRO_ESTUDIO"));
							rpt.setGradoInstruccion(rs.getString("GRADO_INSTRUCCION"));
							rpt.setDireccionCentroDeEstudio(rs.getString("DIRECCION_CENTRO_ESTUDIO"));
							rpt.setTemaInvestigacion(rs.getString("TEMA_INVESTIGACION"));
							rpt.setEstado(rs.getString("ESTADO"));
							rpt.setFechaInicioVigencia(rs.getDate("FECHA_INICIO"));
							rpt.setFechaRenovacion(rs.getDate("FECHA_RENOVACION"));
							rpt.setFechaFinVigencia(rs.getDate("FECHA_FIN"));
							rpt.setCodBarrasLector(rs.getString("COD_BARRAS_LECTOR"));
							rpt.setFlagVigencia(rs.getString("FLAG_VIGENCIA"));
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_SIRU.SP_GET_LECTOR_X_COD_BARRAS "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_SIRU.SP_GET_LECTOR_X_COD_BARRAS"+e.getMessage());
		}

		return rpt;
	}

	@Override
	public ResponseObject actualizarLectorMigradoASIRU(final Long codBarrasLector) throws PersistenceException {
		final ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_SIRU.SP_UPD_LECT_MIGRADO_A_SIRU(?,?) }");
						cstm.setLong("E_LENLEC",codBarrasLector );
						cstm.registerOutParameter("S_COD_MENSAJE", OracleTypes.VARCHAR);	
						cstm.execute();	
						String resultado = VO.getString(cstm.getString("S_COD_MENSAJE"));
						if (resultado.equals(Constants.CODIGO_EXITO_TRANSACCION)) {
							cn.commit();
						}else {
							cn.rollback();
						}
						result.setMensaje(resultado);
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_SIRU.SP_UPD_LECT_MIGRADO_A_SIRU "+ex);
						e.printStackTrace();
						cn.rollback();
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenceException("PKG_SIRU.SP_UPD_LECT_MIGRADO_A_SIRU "+e.getMessage());
		}
		return result;	
		
	}
	
	


}
