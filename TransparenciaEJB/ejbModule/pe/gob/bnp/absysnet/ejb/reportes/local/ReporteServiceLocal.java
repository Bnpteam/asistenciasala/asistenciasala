package pe.gob.bnp.absysnet.ejb.reportes.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.ejb.service.base.GenericService;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;

@Local
public interface ReporteServiceLocal extends GenericService<FiltroBusquedaReporte> {
	public List<HistorialVigencia> obtenerListaReporteRegistroUsuarios(FiltroBusquedaReporte filtro) throws ServiceException;
	public List<ResumenAtencionDto> obtenerReporteResumenAtencionSalaUsuario(FiltroBusquedaReporte filtro) throws ServiceException;
}
