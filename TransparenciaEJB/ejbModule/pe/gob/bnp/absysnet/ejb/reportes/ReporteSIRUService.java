package pe.gob.bnp.absysnet.ejb.reportes;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.absysnet.ejb.reportes.local.ReporteServiceLocal;
import pe.gob.bnp.absysnet.ejb.reportes.remote.ReporteServiceRemote;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.ejb.dao.repository.ReporteDAOLocal;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;

@Stateless
public class ReporteSIRUService implements ReporteServiceLocal, ReporteServiceRemote {

	@EJB
	private ReporteDAOLocal reporteDAOLocal;

	@Override
	public List<HistorialVigencia> obtenerListaReporteRegistroUsuarios(FiltroBusquedaReporte filtro) throws ServiceException {
		try {
			return this.reporteDAOLocal.obtenerListaReporteRegistroUsuarios(filtro);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}

	@Override
	public ResponseObject insert(FiltroBusquedaReporte entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(FiltroBusquedaReporte entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(FiltroBusquedaReporte entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FiltroBusquedaReporte> list(FiltroBusquedaReporte entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FiltroBusquedaReporte findById(FiltroBusquedaReporte entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ResumenAtencionDto> obtenerReporteResumenAtencionSalaUsuario(FiltroBusquedaReporte filtro) throws ServiceException {
		try {
			return this.reporteDAOLocal.obtenerReporteResumenAtencionSalaUsuario(filtro);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}	

}
