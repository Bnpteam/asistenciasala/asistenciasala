package pe.gob.bnp.dapi.properties;

import java.io.IOException;
import java.util.Properties;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public abstract class Property {

	public static String getValueKey(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream(Constants.PROPERTIES_FILE));
			return properties.getProperty(key);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Constants.EMPTY_STRING;
	}	
}
