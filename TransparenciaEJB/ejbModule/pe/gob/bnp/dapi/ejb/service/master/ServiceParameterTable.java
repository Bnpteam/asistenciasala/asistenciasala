package pe.gob.bnp.dapi.ejb.service.master;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.master.entity.Pais;
import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.dao.repository.ParameterTableDAOLocal;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.ejb.service.local.ServiceParameterTableLocal;
import pe.gob.bnp.dapi.ejb.service.remote.ServiceParameterTableRemote;

@Stateless
public class ServiceParameterTable implements ServiceParameterTableRemote, ServiceParameterTableLocal{ 
	@EJB
	private ParameterTableDAOLocal parameterTableDAOLocal;

	
	@Override
	public List<ParameterTable> listParameterTableIdentificationDocumentType(String tableCode, String filter)
			throws ServiceException {
		try {
			return this.getParameterTableDAOLocal().listParameterTableIdentificationDocumentType(tableCode, filter);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Pais> listCountries() throws ServiceException {
		try {
			return this.getParameterTableDAOLocal().listCountries();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public ParameterTableDAOLocal getParameterTableDAOLocal() {
		return parameterTableDAOLocal;
	}

	public void setParameterTableDAOLocal(ParameterTableDAOLocal parameterTableDAOLocal) {
		this.parameterTableDAOLocal = parameterTableDAOLocal;
	}

	@Override
	public ResponseObject insert(ParameterTable entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(ParameterTable entityT) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(ParameterTable entityT) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ParameterTable> list(ParameterTable entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ParameterTable findById(ParameterTable entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ParameterTable> listParameterTableByCode(String tableCode)
			throws ServiceException {
		try {
			return this.getParameterTableDAOLocal().listParameterTableByCode(tableCode);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
}

