package pe.gob.bnp.dapi.ejb.service.remote;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.bnp.dapi.domain.master.entity.Pais;
import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.service.base.GenericService;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;

@Remote
public interface ServiceParameterTableRemote extends GenericService<ParameterTable>{
	public List<ParameterTable> listParameterTableIdentificationDocumentType(String tableCode, String filter) throws ServiceException;
	public List<Pais> listCountries() throws ServiceException;
	public List<ParameterTable> listParameterTableByCode(String tableCode) throws ServiceException;
	
}
