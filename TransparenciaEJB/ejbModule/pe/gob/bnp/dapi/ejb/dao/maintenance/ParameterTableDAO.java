package pe.gob.bnp.dapi.ejb.dao.maintenance;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.common.util.sql.ConnectionUtil;
import pe.gob.bnp.dapi.common.util.validator.VO;
import pe.gob.bnp.dapi.domain.master.entity.Pais;
import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.dao.repository.ParameterTableDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Stateless
public class ParameterTableDAO  extends GenericDAOImpl<ParameterTable> implements ParameterTableDAOLocal {
	private static final Logger LOGGER = Logger.getLogger(ParameterTableDAO.class);

	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager entityManager;

	@Override 
	public List<Pais> listCountries() throws PersistenceException {
		final List<Pais> listaPais = new ArrayList<Pais>(); 
		listaPais.add(new Pais("AFG","AFGANIST�N"));
		listaPais.add(new Pais("ALB","ALBANIA"));
		listaPais.add(new Pais("DEU","ALEMANIA"));
		listaPais.add(new Pais("AND","ANDORRA"));
		listaPais.add(new Pais("AGO","ANGOLA"));
		listaPais.add(new Pais("AIA","ANGUILA"));
		listaPais.add(new Pais("ATA","ANT�RTIDA"));
		listaPais.add(new Pais("ATG","ANTIGUA Y BARBUDA"));
		listaPais.add(new Pais("SAU","ARABIA SAUDITA"));
		listaPais.add(new Pais("DZA","ARGELIA"));
		listaPais.add(new Pais("ARG","ARGENTINA"));
		listaPais.add(new Pais("ARM","ARMENIA"));
		listaPais.add(new Pais("ABW","ARUBA"));
		listaPais.add(new Pais("AUS","AUSTRALIA"));
		listaPais.add(new Pais("AUT","AUSTRIA"));
		listaPais.add(new Pais("AZE","AZERBAIY�N"));
		listaPais.add(new Pais("BHS","BAHAMAS "));
		listaPais.add(new Pais("BGD","BANGLADESH"));
		listaPais.add(new Pais("BRB","BARBADOS"));
		listaPais.add(new Pais("BHR","BAHREIN"));
		listaPais.add(new Pais("BEL","B�LGICA"));
		listaPais.add(new Pais("BLZ","BELICE"));
		listaPais.add(new Pais("BEN","BENIN"));
		listaPais.add(new Pais("BMU","BERMUDAS"));
		listaPais.add(new Pais("BLR","BELAR�S"));
		listaPais.add(new Pais("BOL","BOLIVIA"));
		listaPais.add(new Pais("BIH","BOSNIA Y HERZEGOVINA"));
		listaPais.add(new Pais("BWA","BOTSWANA"));
		listaPais.add(new Pais("BRA","BRASIL"));
		listaPais.add(new Pais("BRN","BRUNEI DARUSSALAM"));
		listaPais.add(new Pais("BGR","BULGARIA"));
		listaPais.add(new Pais("BFA","BURKINA FASO"));
		listaPais.add(new Pais("BDI","BURUNDI"));
		listaPais.add(new Pais("BTN","BHUT�N"));
		listaPais.add(new Pais("CPV","CABO VERDE"));
		listaPais.add(new Pais("KHM","CAMBOYA"));
		listaPais.add(new Pais("CMR","CAMER�N"));
		listaPais.add(new Pais("CAN","CANAD�"));
		listaPais.add(new Pais("QAT","QATAR"));
		listaPais.add(new Pais("TCD","CHAD"));
		listaPais.add(new Pais("CHL","CHILE"));
		listaPais.add(new Pais("CHN","CHINA"));
		listaPais.add(new Pais("CYP","CHIPRE"));
		listaPais.add(new Pais("COL","COLOMBIA"));
		listaPais.add(new Pais("PRK","COREA DEL NORTE"));
		listaPais.add(new Pais("KOR","COREA DEL SUR"));
		listaPais.add(new Pais("CIV","COSTA DE MARFIL"));
		listaPais.add(new Pais("CRI","COSTA RICA"));
		listaPais.add(new Pais("HRV","CROACIA"));
		listaPais.add(new Pais("CUB","CUBA"));
		listaPais.add(new Pais("CUW","CURA�AO"));
		listaPais.add(new Pais("DNK","DINAMARCA"));
		listaPais.add(new Pais("ECU","ECUADOR"));
		listaPais.add(new Pais("EGY","EGIPTO"));
		listaPais.add(new Pais("SLV","EL SALVADOR"));
		listaPais.add(new Pais("ARE","EMIRATOS �RABES UNIDOS"));
		listaPais.add(new Pais("SVK","ESLOVAQUIA"));
		listaPais.add(new Pais("SVN","ESLOVENIA"));
		listaPais.add(new Pais("ESP","ESPA�A"));
		listaPais.add(new Pais("USA","ESTADOS UNIDOS DE AM�RICA"));
		listaPais.add(new Pais("EST","ESTONIA"));
		listaPais.add(new Pais("ETH","ETIOP�A"));
		listaPais.add(new Pais("PHL","FILIPINAS"));
		listaPais.add(new Pais("FIN","FINLANDIA"));
		listaPais.add(new Pais("FJI","FIJI"));
		listaPais.add(new Pais("FRA","FRANCIA"));
		listaPais.add(new Pais("GAB","GAB�N"));
		listaPais.add(new Pais("GMB","GAMBIA"));
		listaPais.add(new Pais("GEO","GEORGIA"));
		listaPais.add(new Pais("GHA","GHANA"));
		listaPais.add(new Pais("GIB","GIBRALTAR"));
		listaPais.add(new Pais("GRD","GRANADA"));
		listaPais.add(new Pais("GRC","GRECIA"));
		listaPais.add(new Pais("GRL","GROENLANDIA"));
		listaPais.add(new Pais("GLP","GUADELOUPE"));
		listaPais.add(new Pais("GUM","GUAM"));
		listaPais.add(new Pais("GTM","GUATEMALA"));
		listaPais.add(new Pais("GUF","GUAYANA FRANCESA"));
		listaPais.add(new Pais("GGY","GUERNSEY"));
		listaPais.add(new Pais("GIN","GUINEA"));
		listaPais.add(new Pais("GNB","GUINEA BISSAU"));
		listaPais.add(new Pais("GNQ","GUINEA ECUATORIAL"));
		listaPais.add(new Pais("GUY","GUYANA"));
		listaPais.add(new Pais("HTI","HAIT�"));
		listaPais.add(new Pais("HND","HONDURAS"));
		listaPais.add(new Pais("HKG","HONG KONG"));
		listaPais.add(new Pais("HUN","HUNGR�A"));
		listaPais.add(new Pais("IND","INDIA"));
		listaPais.add(new Pais("IDN","INDONESIA"));
		listaPais.add(new Pais("IRQ","IRAQ"));
		listaPais.add(new Pais("IRN","IR�N"));
		listaPais.add(new Pais("IRL","IRLANDA"));
		listaPais.add(new Pais("ISL","ISLANDIA"));
		listaPais.add(new Pais("ISR","ISRAEL"));
		listaPais.add(new Pais("ITA","ITALIA"));
		listaPais.add(new Pais("JAM","JAMAICA"));
		listaPais.add(new Pais("JPN","JAP�N"));
		listaPais.add(new Pais("JEY","JERSEY"));
		listaPais.add(new Pais("JOR","JORDANIA"));
		listaPais.add(new Pais("KAZ","KAZAJIST�N"));
		listaPais.add(new Pais("KEN","KENYA"));
		listaPais.add(new Pais("KGZ","KIRGUIST�N"));
		listaPais.add(new Pais("KIR","KIRIBATI"));
		listaPais.add(new Pais("KWT","KUWAIT"));
		listaPais.add(new Pais("LSO","LESOTHO"));
		listaPais.add(new Pais("LVA","LETONIA"));
		listaPais.add(new Pais("LBN","L�BANO"));
		listaPais.add(new Pais("LBR","LIBERIA"));
		listaPais.add(new Pais("LBY","LIBIA"));
		listaPais.add(new Pais("LIE","LIECHTENSTEIN"));
		listaPais.add(new Pais("LTU","LITUANIA"));
		listaPais.add(new Pais("LUX","LUXEMBURGO"));
		listaPais.add(new Pais("MAC","MACAO"));
		listaPais.add(new Pais("MKD","MACEDONIA"));
		listaPais.add(new Pais("MDG","MADAGASCAR"));
		listaPais.add(new Pais("MYS","MALASIA"));
		listaPais.add(new Pais("MDV","MALDIVAS"));
		listaPais.add(new Pais("MLI","MAL�"));
		listaPais.add(new Pais("MLT","MALTA"));
		listaPais.add(new Pais("MAR","MARRUECOS"));
		listaPais.add(new Pais("MTQ","MARTINIQUE"));
		listaPais.add(new Pais("MUS","MAURICIO"));
		listaPais.add(new Pais("MRT","MAURITANIA"));
		listaPais.add(new Pais("MEX","M�XICO"));
		listaPais.add(new Pais("FSM","MICRONESIA"));
		listaPais.add(new Pais("MDA","MOLDOVIA"));
		listaPais.add(new Pais("MCO","M�NACO"));
		listaPais.add(new Pais("MNG","MONGOLIA"));
		listaPais.add(new Pais("MNE","MONTENEGRO"));
		listaPais.add(new Pais("MSR","MONTSERRAT"));
		listaPais.add(new Pais("MOZ","MOZAMBIQUE"));
		listaPais.add(new Pais("MMR","MYANMAR"));
		listaPais.add(new Pais("NAM","NAMIBIA"));
		listaPais.add(new Pais("NRU","NAURU"));
		listaPais.add(new Pais("NPL","NEPAL"));
		listaPais.add(new Pais("NIC","NICARAGUA"));
		listaPais.add(new Pais("NGA","NIGERIA"));
		listaPais.add(new Pais("NOR","NORUEGA"));
		listaPais.add(new Pais("NZL","NUEVA ZELANDIA"));
		listaPais.add(new Pais("OMN","OM�N"));
		listaPais.add(new Pais("NLD","PA�SES BAJOS"));
		listaPais.add(new Pais("PAK","PAKIST�N"));
		listaPais.add(new Pais("PSE","PALESTINA"));
		listaPais.add(new Pais("PAN","PANAM�"));
		listaPais.add(new Pais("PNG","PAPUA NUEVA GUINEA"));
		listaPais.add(new Pais("PRY","PARAGUAY"));
		listaPais.add(new Pais("PER","PER�"));
		listaPais.add(new Pais("POL","POLONIA"));
		listaPais.add(new Pais("PRT","PORTUGAL"));
		listaPais.add(new Pais("PRI","PUERTO RICO"));
		listaPais.add(new Pais("GBR","REINO UNIDO"));
		listaPais.add(new Pais("COG","EL CONGO"));
		listaPais.add(new Pais("COD","REP�BLICA DEMOCR�TICA DEL CONGO "));
		listaPais.add(new Pais("DOM","DOMINICANA"));
		listaPais.add(new Pais("REU","REUNI�N"));
		listaPais.add(new Pais("RWA","RWANDA"));
		listaPais.add(new Pais("ROU","RUMANIA"));
		listaPais.add(new Pais("RUS","RUSIA"));
		listaPais.add(new Pais("WSM","SAMOA"));
		listaPais.add(new Pais("SEN","SENEGAL"));
		listaPais.add(new Pais("SRB","SERBIA"));
		listaPais.add(new Pais("SLE","SIERRA LEONA"));
		listaPais.add(new Pais("SGP","SINGAPUR"));
		listaPais.add(new Pais("SYR","REP�BLICA �RABE SIRIA"));
		listaPais.add(new Pais("SOM","SOMALIA"));
		listaPais.add(new Pais("LKA","SRI LANKA"));
		listaPais.add(new Pais("ZAF","SUD�FRICA"));
		listaPais.add(new Pais("SDN","SUD�N"));
		listaPais.add(new Pais("SSD","SUD�N DEL SUR"));
		listaPais.add(new Pais("SWE","SUECIA"));
		listaPais.add(new Pais("CHE","SUIZA"));
		listaPais.add(new Pais("SUR","SURINAM"));
		listaPais.add(new Pais("THA","TAILANDIA"));
		listaPais.add(new Pais("TZA","TANZANIA"));
		listaPais.add(new Pais("TJK","TAYIKIST�N"));
		listaPais.add(new Pais("TGO","TOGO"));
		listaPais.add(new Pais("TON","TONGA"));
		listaPais.add(new Pais("TTO","TRINIDAD Y TOBAGO"));
		listaPais.add(new Pais("TUN","T�NEZ"));
		listaPais.add(new Pais("TKM","TURKMENIST�N"));
		listaPais.add(new Pais("TUR","TURQU�A"));
		listaPais.add(new Pais("UKR","UCRANIA"));
		listaPais.add(new Pais("UGA","UGANDA"));
		listaPais.add(new Pais("URY","URUGUAY"));
		listaPais.add(new Pais("UZB","UZBEKIST�N"));
		listaPais.add(new Pais("VAT","VATICANO"));
		listaPais.add(new Pais("VEN","VENEZUELA"));
		listaPais.add(new Pais("VNM","VIETNAM"));
		listaPais.add(new Pais("YEM","YEMEN"));
		listaPais.add(new Pais("ZMB","ZAMBIA"));
		listaPais.add(new Pais("ZWE","ZIMBABWE"));
		return listaPais;
	}
	
	@Override
	public List<ParameterTable> listParameterTableIdentificationDocumentType(final String tableCode, final String filter) throws PersistenceException {
		final List<ParameterTable> parametersTable = new ArrayList<ParameterTable>();
		try {
			Session hbSession = entityManager.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_TBL_PARAMETRO_TDI(?,?,?)}");
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA", tableCode);
						cstm.setString("P_VALOR1", filter);
						cstm.execute();
						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						while (rs.next()) {
							ParameterTable parameterTable = new ParameterTable();
							parameterTable.setParametroId(rs.getLong("PARAMETRO_ID"));
							parameterTable.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							parameterTable.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							parameterTable.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							parameterTable.setNombreLargo(VO.parseEncode(rs.getString("NOMBRE_LARGO")));
							parametersTable.add(parameterTable);
						}

					} catch (Exception e) {
						LOGGER.error("SQLException:PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_TBL_PARAMETRO_TDI " + e.getMessage());
						e.printStackTrace();
					} finally {
						ConnectionUtil.closeSqlConnections(rs, cstm,cn);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Exception:PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_TBL_PARAMETRO_TDI " + e);
		} finally {

		}
		return parametersTable;
	}
	
	@Override
	public ResponseObject insert(ParameterTable entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(ParameterTable entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(ParameterTable entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ParameterTable> list(ParameterTable entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ParameterTable findById(ParameterTable entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnObject insertar(ParameterTable prmT) throws PersistenciaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean actualizar(ParameterTable prmT) throws PersistenciaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean eliminar(ParameterTable prmT) throws PersistenciaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ParameterTable> listar(ParameterTable prmT) throws PersistenciaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ParameterTable buscarXId(ParameterTable prmT) throws PersistenciaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ParameterTable> listParameterTableByCode(final String tableCode) throws PersistenceException {
		final List<ParameterTable> parametersTable = new ArrayList<ParameterTable>();
		try {
			Session hbSession = entityManager.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_TBL_PARAMETRO(?,?)}");
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA", tableCode);
						cstm.execute();
						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						while (rs.next()) {
							ParameterTable parameterTable = new ParameterTable();
							parameterTable.setParametroId(rs.getLong("PARAMETRO_ID"));
							parameterTable.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							parameterTable.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							parameterTable.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							parameterTable.setNombreLargo(VO.parseEncode(rs.getString("NOMBRE_LARGO")));
							parametersTable.add(parameterTable);
						}

					} catch (Exception e) {
						LOGGER.error("SQLException:PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_TBL_PARAMETRO:" + e.getMessage());
						e.printStackTrace();
					} finally {
						ConnectionUtil.closeSqlConnections(rs, cstm,cn);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Exception:PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_TBL_PARAMETRO " + e);
		} finally {

		}
		return parametersTable;

	}
	
}
