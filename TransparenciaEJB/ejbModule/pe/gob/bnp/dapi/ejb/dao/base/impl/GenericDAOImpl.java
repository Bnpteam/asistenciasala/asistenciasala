package pe.gob.bnp.dapi.ejb.dao.base.impl;

import java.io.Serializable;

import pe.gob.bnp.dapi.ejb.dao.base.GenericDAO;


public abstract class GenericDAOImpl <T extends Serializable> implements GenericDAO<T>  {
	private static final long serialVersionUID = 1L;

}
