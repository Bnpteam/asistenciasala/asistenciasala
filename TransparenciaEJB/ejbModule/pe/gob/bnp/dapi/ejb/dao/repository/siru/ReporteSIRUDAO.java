package pe.gob.bnp.dapi.ejb.dao.repository.siru;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.dao.repository.ReporteDAOLocal;
import pe.gob.servir.systems.util.sql.Conexion;
import pe.gob.servir.systems.util.validator.VO;

@Stateless
public class ReporteSIRUDAO extends GenericDAOImpl<FiltroBusquedaReporte> implements ReporteDAOLocal {
	private static final Logger logger = Logger.getLogger(ReporteSIRUDAO.class);
	 
	 
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;	
	 
	@Override
	public List<HistorialVigencia> obtenerListaReporteRegistroUsuarios(final FiltroBusquedaReporte filtro) throws PersistenceException {
		final List<HistorialVigencia> listarpt = new ArrayList<HistorialVigencia>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_REPORTE_REGISTRO_USUARIO" + "( ?, ?, ? ) }");
						cstm.setDate("E_FECHA_DESDE", VO.getSQLDate(filtro.getRangoFechaDesde()));
						cstm.setDate("E_FECHA_HASTA", VO.getSQLDate(filtro.getRangoFechaHasta()));
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							HistorialVigencia rpt= new HistorialVigencia();
							rpt.setFila(VO.getString(rs.getString("FILA")));
							rpt.setFechaTramite(rs.getDate("FECHA_TRAMITE"));
							rpt.setTipoUsuario(VO.getString(rs.getString("TIPO_USUARIO_BIBLIOTECA")));
							rpt.setCodBarrasLector(VO.getString(rs.getString("COD_BARRAS_LECTOR")));
							rpt.setTipoDocumentoIdentidad(VO.getString(rs.getString("TIPO_DOCUMENTO_IDENTIDAD")));
							rpt.setNumeroDocumentoIdentidad(VO.getString(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD")));
							rpt.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
							rpt.setSexo(VO.getString(rs.getString("SEXO")));
							rpt.setTipoRegistro(VO.getString(rs.getString("TIPO_REGISTRO")));
							rpt.setFechaInicio(rs.getDate("FECHA_INICIO"));
							rpt.setFechaFin(rs.getDate("FECHA_FIN"));
							rpt.setGradoInstruccion(VO.getString(rs.getString("GRADO_INSTRUCCION")));
							rpt.setNacionalidad(VO.getString(rs.getString("NACIONALIDAD")));
							rpt.setPais(VO.getString(rs.getString("PAIS")));
							rpt.setDepartamento(VO.getString(rs.getString("DEPARTAMENTO")));
							rpt.setProvincia(VO.getString(rs.getString("PROVINCIA")));
							rpt.setDistrito(VO.getString(rs.getString("DISTRITO")));
							rpt.setEstado(VO.getString(rs.getString("ESTADO")));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_GESTION_CIRCULACION.SP_REPORTE_REGISTRO_USUARIO "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_GESTION_CIRCULACION.SP_REPORTE_REGISTRO_USUARIO "+e.getMessage());
		}
		return listarpt;

	}

	@Override
	public ResponseObject insert(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FiltroBusquedaReporte> list(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FiltroBusquedaReporte findById(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ResumenAtencionDto> obtenerReporteResumenAtencionSalaUsuario(final FiltroBusquedaReporte filtro) throws PersistenceException {
		final List<ResumenAtencionDto> listarpt = new ArrayList<>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_REPORTE_RESUMEN_SALAUSUARIO" + "( ?,?,? ) }");
						cstm.setDate("E_PERIODO_DESDE", VO.getSQLDate(filtro.getRangoFechaDesde()));
						cstm.setDate("E_PERIODO_HASTA", VO.getSQLDate(filtro.getRangoFechaHasta()));
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							ResumenAtencionDto rpt= new ResumenAtencionDto();
							rpt.setYear(VO.getString(rs.getString("YEAR")));
							rpt.setMes(VO.getString(rs.getString("MES")));
							rpt.setTipoUsuario(VO.getString(rs.getString("TIPO_USUARIO_BIBLIOTECA")));
							rpt.setTipoRegistro(VO.getString(rs.getString("TIPO_REGISTRO")));
							rpt.setDia01(VO.getString(rs.getString("DIA01")));
							rpt.setDia02(VO.getString(rs.getString("DIA02")));
							rpt.setDia03(VO.getString(rs.getString("DIA03")));
							rpt.setDia04(VO.getString(rs.getString("DIA04")));
							rpt.setDia05(VO.getString(rs.getString("DIA05")));
							rpt.setDia06(VO.getString(rs.getString("DIA06")));
							rpt.setDia07(VO.getString(rs.getString("DIA07")));
							rpt.setDia08(VO.getString(rs.getString("DIA08")));
							rpt.setDia09(VO.getString(rs.getString("DIA09")));
							rpt.setDia10(VO.getString(rs.getString("DIA10")));
							rpt.setDia11(VO.getString(rs.getString("DIA11")));
							rpt.setDia12(VO.getString(rs.getString("DIA12")));
							rpt.setDia13(VO.getString(rs.getString("DIA13")));
							rpt.setDia14(VO.getString(rs.getString("DIA14")));
							rpt.setDia15(VO.getString(rs.getString("DIA15")));
							rpt.setDia16(VO.getString(rs.getString("DIA16")));
							rpt.setDia17(VO.getString(rs.getString("DIA17")));
							rpt.setDia18(VO.getString(rs.getString("DIA18")));
							rpt.setDia19(VO.getString(rs.getString("DIA19")));
							rpt.setDia20(VO.getString(rs.getString("DIA20")));
							rpt.setDia21(VO.getString(rs.getString("DIA21")));
							rpt.setDia22(VO.getString(rs.getString("DIA22")));
							rpt.setDia23(VO.getString(rs.getString("DIA23")));
							rpt.setDia24(VO.getString(rs.getString("DIA24")));
							rpt.setDia25(VO.getString(rs.getString("DIA25")));
							rpt.setDia26(VO.getString(rs.getString("DIA26")));
							rpt.setDia27(VO.getString(rs.getString("DIA27")));
							rpt.setDia28(VO.getString(rs.getString("DIA28")));
							rpt.setDia29(VO.getString(rs.getString("DIA29")));
							rpt.setDia30(VO.getString(rs.getString("DIA30")));
							rpt.setDia31(VO.getString(rs.getString("DIA31")));
							rpt.setTotalFila(VO.getString(rs.getString("TOTAL_FILA")));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_GESTION_CIRCULACION.SP_REPORTE_RESUMEN_SALAUSUARIO "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_GESTION_CIRCULACION.SP_REPORTE_RESUMEN_SALAUSUARIO "+e.getMessage());
		}
		return listarpt;
	}


}
