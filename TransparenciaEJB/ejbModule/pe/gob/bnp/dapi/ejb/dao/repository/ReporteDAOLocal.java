package pe.gob.bnp.dapi.ejb.dao.repository;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.domain.entity.RegistroUsuario;
import pe.gob.bnp.dapi.ejb.dao.base.GenericDAO;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;

@Local
public interface ReporteDAOLocal extends GenericDAO<FiltroBusquedaReporte> {
	public List<HistorialVigencia> obtenerListaReporteRegistroUsuarios(FiltroBusquedaReporte filtro) throws PersistenceException;
	public List<ResumenAtencionDto> obtenerReporteResumenAtencionSalaUsuario(FiltroBusquedaReporte filtro) throws PersistenceException;
	
}
