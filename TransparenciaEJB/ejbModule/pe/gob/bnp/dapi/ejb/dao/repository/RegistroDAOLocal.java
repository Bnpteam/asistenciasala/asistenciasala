package pe.gob.bnp.dapi.ejb.dao.repository;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.domain.entity.Preference;
import pe.gob.bnp.dapi.domain.entity.RegistroUsuario;
import pe.gob.bnp.dapi.domain.entity.RegistroVisitaASala;
import pe.gob.bnp.dapi.domain.entity.VerificacionIdentidad;
import pe.gob.bnp.dapi.ejb.dao.base.GenericDAO;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.salalectura.domain.entity.MotivoSalida;
import pe.gob.bnp.dapi.salalectura.domain.entity.SesionSalaUsuario;
import pe.gob.bnp.dapi.salalectura.dto.CredencialUsuarioDto;
import pe.gob.bnp.dapi.salalectura.dto.FiltroUsuarioEnSalaDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorConsultaAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorFotoDto;
import pe.gob.bnp.dapi.salalectura.dto.MonitoreoEnSala;
import pe.gob.bnp.dapi.salalectura.dto.TransaccionAsistenciaSala;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;
import pe.gob.bnp.domain.entity.SalaBiblioteca;
import pe.gob.servir.sistemas.transparencia.model.dto.DocumentoIdentidadDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroUsuarioDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroVisitaDto;
import pe.gob.servir.sistemas.transparencia.model.dto.HistorialRegistroDto;

@Local
public interface RegistroDAOLocal extends GenericDAO<RegistroUsuario> {
	public ResponseObject validarDocumentoIdentidad(DocumentoIdentidadDto documentoIdentidadDto) throws PersistenceException;
	public List<RegistroUsuario> listarBandejaRegistrosUsuario(FiltroRegistroUsuarioDto filtro) throws PersistenceException;
	public RegistroUsuario obtenerRegistrosUsuario(Long idRegistroUsuario) throws PersistenceException;
	public ResponseObject actualizarRegistroUsuario(RegistroUsuario registroUsuario) throws PersistenceException;
	public ResponseObject cambiarEstadoRegistroUsuario(RegistroUsuario registroUsuario) throws PersistenceException;
	public List<HistorialRegistroDto> listarHistorialRegistroUsuario(Long idRegistro) throws PersistenceException;
	public List<Preference> listarPreferenciasUsuario(Long idUsuarioBiblioteca) throws PersistenceException;
	public List<RegistroVisitaASala> listarBandejaRegistroVisitasSala(FiltroRegistroVisitaDto filtro) throws PersistenceException;
	public ResponseObject registrarVisitaASala(RegistroVisitaASala usuarioVisitante) throws PersistenceException;
	public ResponseObject registrarVisitaASalaPorNumeroCarnet(RegistroVisitaASala registroVisita) throws PersistenceException;
	public ResponseObject obtenerSalaPorDireccionIP(RegistroVisitaASala registroVisita) throws PersistenceException;
	public ResponseObject registrarUsoVerificacionEntidad(VerificacionIdentidad verificacionIdentidad) throws PersistenceException;
	public ResponseObject registrarCambioTipoRegistroUsuario(RegistroUsuario registroUsuario) throws PersistenceException;
	public List<HistorialVigencia> listarVigenciasUsuario(Long idRegistro) throws PersistenceException;
	
	//API ASISTENCIA A SALA
	public List<SalaBiblioteca> listarSalasDisponibles(String codigoSede) throws PersistenceException;
	public SesionSalaUsuario	validarAccesoReferencistaASala(CredencialUsuarioDto credencialUsuario) throws PersistenceException;
	public TransaccionAsistenciaSala registrarIngresoASala(LectorConsultaAbsysnetDto lector) throws PersistenceException;
	public List<UsuarioEnSala>  listarUsuariosEnSala(FiltroUsuarioEnSalaDto filtro) throws PersistenceException;
	public TransaccionAsistenciaSala registrarSalidaDeSala(UsuarioEnSala usuario) throws PersistenceException;
	public ResponseObject 	registrarUsuarioMigradoDeAbsysnet(LectorAbsysnetDto lector) throws PersistenceException;
	public List<MotivoSalida> listarMaestroMotivosSalidaDeSala() throws PersistenceException;
	public LectorFotoDto obtenerFotosLector(String codBarrasLector) throws PersistenceException;
	public MonitoreoEnSala obtenerListasMonitoreoEnSala() throws PersistenceException;
	
}
