package pe.gob.bnp.dapi.ejb.dao.base;

import java.io.Serializable;
import java.util.List;

import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;

public interface GenericDAO<T extends Serializable> {
	public ResponseObject insert(T entityT) throws PersistenceException;
	public Boolean update(T entityT) throws PersistenceException;
	public Boolean delete(T entityT) throws PersistenceException;
	public List<T> list(T entityT) throws PersistenceException;
	public T findById(T entityT) throws PersistenceException;
}
