package pe.gob.bnp.dapi.ejb.dao.repository;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.dapi.domain.master.entity.Pais;
import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.dao.base.GenericDAO;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;

@Local
public interface ParameterTableDAOLocal extends GenericDAO<ParameterTable>{
	public List<ParameterTable> listParameterTableIdentificationDocumentType(String tableCode, String filter) throws PersistenceException;
	public List<Pais> listCountries() throws PersistenceException;
	public List<ParameterTable> listParameterTableByCode(String tableCode) throws PersistenceException;
	
}

