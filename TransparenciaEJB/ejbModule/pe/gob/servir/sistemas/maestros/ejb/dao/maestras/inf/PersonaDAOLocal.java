package pe.gob.servir.sistemas.maestros.ejb.dao.maestras.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.maestra.Persona;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;

@Local
public interface PersonaDAOLocal extends GenericDAO<Persona>{
		
	public Persona buscarXDni(String prmDni);
	
	public List<UbigeoReniec> listUbigeo(UbigeoReniec prmubigeo) throws PersistenciaException;
	
	public UbigeoReniec buscarxubigeo(UbigeoReniec prmubigeo);
	
	public List<UbigeoReniec> listUbigeoXDescripcion(UbigeoReniec prmubigeo) throws PersistenciaException;
}
