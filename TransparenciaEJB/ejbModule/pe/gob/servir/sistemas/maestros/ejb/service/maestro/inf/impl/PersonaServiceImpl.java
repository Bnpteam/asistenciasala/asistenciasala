package pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.maestros.ejb.dao.maestras.inf.PersonaDAOLocal;
import pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.local.PersonaServiceLocal;
import pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.remoto.PersonaServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.maestra.Persona;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;
import pe.gob.servir.systems.util.retorno.ReturnObject;


@Stateless
public class PersonaServiceImpl implements PersonaServiceRemote, PersonaServiceLocal{
	
	@EJB
	private PersonaDAOLocal personaDAOLocal;

	@Override
	public ReturnObject insertar(Persona prmPersona)  throws ServicioException{
		
		try {
			return personaDAOLocal.insertar(prmPersona);
		} catch (Exception e) {
			
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean actualizar(Persona prmPersona)  throws ServicioException {
		
		try {
			return personaDAOLocal.actualizar(prmPersona);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean eliminar(Persona prmPersona)  throws ServicioException {
		
		try {
			return personaDAOLocal.eliminar(prmPersona);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Persona> listar(Persona prmPersona)  throws ServicioException {
		
		try {
			return personaDAOLocal.listar(prmPersona);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Persona buscarXId(Persona prmPersona)  throws ServicioException {
		
		try {
			return personaDAOLocal.buscarXId(prmPersona);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Persona buscarXDni(String prmDni) {
		
		return personaDAOLocal.buscarXDni(prmDni);
	}
	
	@Override
	public Persona buscarPersonaXDni(String prmDni) {
		
		Persona rsPersona = new Persona();
		try {
			rsPersona = this.buscarXDni(prmDni);
			if ((rsPersona!=null)&&((rsPersona.getNroDocumento()!=null)&&(rsPersona.getNroDocumento().length()>0))){
				return rsPersona;
			}
			return null;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public PersonaDAOLocal getPersonaDAOLocal() {
		return personaDAOLocal;
	}

	public void setPersonaDAOLocal(PersonaDAOLocal personaDAOLocal) {
		this.personaDAOLocal = personaDAOLocal;
	}

	@Override
	public List<UbigeoReniec> listUbigeo(UbigeoReniec prmubigeo) throws ServicioException {
		try {
			return personaDAOLocal.listUbigeo(prmubigeo);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public UbigeoReniec buscarxubigeo(UbigeoReniec prmubigeo) {
		// 
		return null;
	}

	@Override
	public List<UbigeoReniec> listUbigeoXDescripcion(UbigeoReniec prmubigeo)
			throws ServicioException {
		try {
			return personaDAOLocal.listUbigeoXDescripcion(prmubigeo);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	
	
}
