package pe.gob.servir.sistemas.transparencia.ejb.service.base.impl;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;

public abstract class GenericServiceImpl <T extends Serializable> implements GenericService<T>{

}
