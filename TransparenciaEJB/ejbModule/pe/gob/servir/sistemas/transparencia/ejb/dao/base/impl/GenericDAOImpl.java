package pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;

public abstract class GenericDAOImpl <T extends Serializable> implements GenericDAO<T> {
	private static final long serialVersionUID = 1L;

}
