package pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.MovimientoSolicitudDAOLocal;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteVerRespuestas;
import pe.gob.servir.sistemas.transparencia.model.negocio.SolicitudMovimiento;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class MovimientoSolicitudDAOImpl  extends GenericDAOImpl<SolicitudMovimiento> implements MovimientoSolicitudDAOLocal{

	private static final Logger logger = Logger.getLogger(MovimientoSolicitudDAOImpl.class);	
	@PersistenceContext(unitName = "PUCirculacion")
	
	private EntityManager em;
	@Override
	public ReturnObject insertar(final SolicitudMovimiento prmObj) throws PersistenciaException {
		
		ReturnObject result = new ReturnObject();
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_INSERT_SOLIC_MOVIM"
								+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
						
						cstm.setLong("E_SOLICITUD_TRANSPARENCIA_ID", prmObj.getSolicitudTransparenciaId());
						cstm.setLong("P_USUARIO_ID_EMISOR", prmObj.getUsuarioIdEmisor());
						cstm.setLong("P_PERFIL_ID_EMISOR", prmObj.getPerfilIdEmisor());
						String undRemitenteId = prmObj.getT01UndOrgEmiId();
						cstm.setString("P_T01_UND_ORG_EMISOR", undRemitenteId);
						cstm.setString("E_SITUACION_SOLICITUD_ID", prmObj.getSituacionSolicitudId());
						cstm.setString("E_ESTADO_SOLICITUD_ID", prmObj.getEstadoSolicitudId());
						cstm.setString("P_T01_UND_ORG_RECEPTOR", prmObj.getT01UndOrgRecId());	
						cstm.setLong("E_PERFIL_ID_RECEPTOR", prmObj.getPerfilIdReceptor());
						cstm.setLong("E_USUARIO_ID_RECEPTOR", prmObj.getUsuarioIdReceptor());
						cstm.setString("E_CORREO", prmObj.getCorreo().trim().toLowerCase());
						cstm.setString("E_ASUNTO", prmObj.getAsunto().trim());
						cstm.setString("E_DETALLE_RESPUESTA", prmObj.getDetalleRespuesta());
						cstm.setString("E_DETALLE_ASIGNACION", prmObj.getDetalleAsignacion());
						cstm.setString("E_LISTA_PROYECCION_UND_ORG", prmObj.getListaProyeccionUndOrg());
						
						cstm.setLong("E_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());

						cstm.registerOutParameter("S_SOLICITUD_MOVIMIENTO_ID", OracleTypes.NUMBER);	
						
						cstm.execute();
						Object object=cstm.getObject("S_SOLICITUD_MOVIMIENTO_ID");
						BigDecimal decId= new BigDecimal(0);						
												
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
						Integer id = decId.intValue();
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:MovimientoSolicitudDAOImpl:insert "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:MovimientoSolicitudDAOImpl:insert "+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setSw(prmObj.getResultTransaction());	
		return result;	
	}
	
	@Override
	public ReturnObject insertarMultiDesdeJefe(final SolicitudMovimiento prmObj) throws PersistenciaException {
		
		ReturnObject result = new ReturnObject();
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_INSERT_SOLIC_MOVIM_XAREA"
								+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
										
						cstm.setLong("P_SOLICITUD_TRANSPARENCIA_ID", prmObj.getSolicitudTransparenciaId());
						cstm.setLong("P_USUARIO_ID_EMISOR", prmObj.getUsuarioIdEmisor());
						cstm.setLong("P_PERFIL_ID_EMISOR", prmObj.getPerfilIdEmisor());
						cstm.setString("P_T01_UND_ORG_EMISOR", prmObj.getT01UndOrgEmiId());
						cstm.setString("P_SITUACION_SOLICITUD_ID", prmObj.getSituacionSolicitudId());
						cstm.setString("P_ESTADO_SOLICITUD_ID", prmObj.getEstadoSolicitudId());
						cstm.setString("P_T01_UND_ORG_RECEPTOR", prmObj.getT01UndOrgRecId());	
						cstm.setLong("P_PERFIL_ID_RECEPTOR", prmObj.getPerfilIdReceptor());
						cstm.setLong("P_USUARIO_ID_RECEPTOR", prmObj.getUsuarioIdReceptor());
						cstm.setString("P_CORREO", prmObj.getCorreo());
						cstm.setString("P_ASUNTO", prmObj.getAsunto());
						cstm.setString("P_DETALLE_RESPUESTA", prmObj.getDetalleRespuesta());
						cstm.setString("P_DETALLE_ASIGNACION", prmObj.getDetalleAsignacion());
						cstm.setLong("P_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());

						cstm.registerOutParameter("P_SOLICITUD_MOVIMIENTO_ID", OracleTypes.NUMBER);	
						cstm.registerOutParameter("P_PERSONA_RECEPTORA", OracleTypes.VARCHAR);	
						
						cstm.execute();
						
						Object object=cstm.getObject("P_SOLICITUD_MOVIMIENTO_ID");
						Object object2=cstm.getObject("P_PERSONA_RECEPTORA");
						BigDecimal decId= new BigDecimal(0);						
												
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
						Integer id = decId.intValue();
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setAsunto(object2.toString());
							prmObj.setResultTransaction(true);
						} else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:MovimientoSolicitudDAOImpl:insert "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:MovimientoSolicitudDAOImpl:insert "+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setMsg(prmObj.getAsunto());
		result.setSw(prmObj.getResultTransaction());	
		return result;	
	}
	
	@Override
	public ReturnObject validarCodigoFormulario(final String prmCodigo) throws PersistenciaException {
		
		final ReturnObject result = new ReturnObject();
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_VALIDXCODIGO_TBL_SOLI_TRAN"
								+ " ( ?, ?) }");
										
						cstm.setString("P_CODIGO", prmCodigo);

						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);	
						
						cstm.execute();

						Object object = cstm.getObject("P_RETORNO");
						
						if (object != null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret > 0)) {
								result.setSw(true);
							}else{
								result.setSw(false);
							}							
						}else{
							result.setSw(false);
						}
						
					} catch (SQLException e) {
						logger.error("SQLException:MovimientoSolicitudDAOImpl:validarCodigoFormulario "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
						Conexion.closeConnection(cn);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:MovimientoSolicitudDAOImpl:validarCodigoFormulario "+e.getMessage());
		}

		return result;
	}
	
	@Override
	public List<SolicitudMovimiento> lstLinTiempoXAreas1(final SolicitudMovimiento tmpObj, final String mostrarEstados)
			throws PersistenciaException {
		final List<SolicitudMovimiento> listarpt = new ArrayList<SolicitudMovimiento>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_LISTA_LIN_TMPO_AREA_RESP1" + "( ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						Long soliTransId = tmpObj.getSolicitudTransparenciaId();
						cstm.setLong("P_SOLICITUD_TRANSPARENCIA_ID", soliTransId);
						String undOrgReceptorId = tmpObj.getT01UndOrgRecId();
						cstm.setString("P_T01_UND_ORG_RECEPTOR", undOrgReceptorId);
						
						cstm.setString("P_MOSTRAR_ESTADOS", mostrarEstados);
						
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							SolicitudMovimiento rpt= new SolicitudMovimiento();
							rpt.setId(rs.getLong("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setSolicitudTransparenciaId(rs.getLong("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setPerfilIdEmisor(rs.getLong("R_PERFIL_ID_EMISOR"));
							rpt.setPerfilDetaEmisor(rs.getString("R_PERFIL_DETA_EMISOR"));
							rpt.setT01UndOrgEmiId(rs.getString("R_T01_UND_ORG_EMIS_ID"));
							rpt.setT01UndOrgEmiDeta(rs.getString("R_T01_UND_ORG_EMI_DETA"));
							rpt.setSituacionSolicitudId(rs.getString("R_SITUACION_SOLICITUD_ID"));
							rpt.setEstadoSolicitudId(rs.getString("R_ESTADO_SOLICITUD_ID"));
							rpt.setT01UndOrgRecId(rs.getString("R_T01_UND_ORG_RECE_ID"));
							rpt.setT01UndOrgRecDeta(rs.getString("R_T01_UND_ORG_REC_DETA"));
							rpt.setPerfilIdReceptor(rs.getLong("R_PERFIL_ID"));
							rpt.setPerfilDetaReceptor(rs.getString("R_PERFIL_DETA"));
							rpt.setUsuarioIdReceptor(rs.getLong("R_USUARIO_ID"));
							rpt.setDetalleRespuesta(rs.getString("R_DETALLE_RESPUESTA"));
							rpt.setDetalleAsignacion(rs.getString("R_DETALLE_ENVIO"));
							rpt.setUsuarioIdRegistro(rs.getLong("R_USUARIO_ID_REGISTRO"));
							rpt.setFechaRegistro(rs.getDate("R_FECHA_REGISTRO"));
											
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstLineaTiempoXAreas "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstLineaTiempoXAreas "+e.getMessage());
		}

		return listarpt;
	}
	
	@Override
	public List<SolicitudMovimiento> lstLinTiempoXAreas2(final SolicitudMovimiento tmpObj, final String mostrarEstados)
			throws PersistenciaException {
		final List<SolicitudMovimiento> listarpt = new ArrayList<SolicitudMovimiento>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_LISTA_LIN_TMPO_AREA_RESP2" + "( ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						Long soliTransId = tmpObj.getSolicitudTransparenciaId();
						cstm.setLong("P_SOLICITUD_TRANSPARENCIA_ID", soliTransId);
						String undOrgReceptorId = tmpObj.getT01UndOrgRecId();
						cstm.setString("P_T01_UND_ORG_RECEPTOR", undOrgReceptorId);
						
						cstm.setString("P_MOSTRAR_ESTADOS", mostrarEstados);
						
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							SolicitudMovimiento rpt= new SolicitudMovimiento();
							rpt.setId(rs.getLong("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setSolicitudTransparenciaId(rs.getLong("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setPerfilIdEmisor(rs.getLong("R_PERFIL_ID_EMISOR"));
							rpt.setPerfilDetaEmisor(rs.getString("R_PERFIL_DETA_EMISOR"));
							rpt.setT01UndOrgEmiId(rs.getString("R_T01_UND_ORG_EMIS_ID"));
							rpt.setT01UndOrgEmiDeta(rs.getString("R_T01_UND_ORG_EMI_DETA"));
							rpt.setSituacionSolicitudId(rs.getString("R_SITUACION_SOLICITUD_ID"));
							rpt.setEstadoSolicitudId(rs.getString("R_ESTADO_SOLICITUD_ID"));
							rpt.setT01UndOrgRecId(rs.getString("R_T01_UND_ORG_RECE_ID"));
							rpt.setT01UndOrgRecDeta(rs.getString("R_T01_UND_ORG_REC_DETA"));
							rpt.setPerfilIdReceptor(rs.getLong("R_PERFIL_ID"));
							rpt.setPerfilDetaReceptor(rs.getString("R_PERFIL_DETA"));
							rpt.setUsuarioIdReceptor(rs.getLong("R_USUARIO_ID"));
							rpt.setDetalleRespuesta(rs.getString("R_DETALLE_RESPUESTA"));
							rpt.setDetalleAsignacion(rs.getString("R_DETALLE_ENVIO"));
							rpt.setUsuarioIdRegistro(rs.getLong("R_USUARIO_ID_REGISTRO"));
							rpt.setFechaRegistro(rs.getDate("R_FECHA_REGISTRO"));
							rpt.setUsuarioIdEmisor(rs.getLong("R_USUARIO_ID_EMISOR"));
											
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstLineaTiempoXAreas "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstLineaTiempoXAreas "+e.getMessage());
		}

		return listarpt;
	}
	
	@Override
	public List<ReporteVerRespuestas> lstVerRespuestas(final SolicitudMovimiento tmpObj)
			throws PersistenciaException {
		final List<ReporteVerRespuestas> listarpt = new ArrayList<ReporteVerRespuestas>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_LISTA_VER_RESPUESTA" + "( ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						Long soliTransId = tmpObj.getSolicitudTransparenciaId();
						cstm.setLong("P_SOLICITUD_TRANSPARENCIA_ID", soliTransId);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteVerRespuestas rpt= new ReporteVerRespuestas();
							rpt.setSolicitudMovimientoId(rs.getLong("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setSolicitudTransparenciaId(rs.getLong("R_SOLICITUD_TRANSPARENCIA_ID"));
							//> EMISOR.
							rpt.setT01UndOrgEmiId(rs.getString("R_T01_UND_ORG_EMISOR"));
							rpt.setT01UndOrgEmiDeta(rs.getString("R_T01_UND_ORG_EMI_DETA"));
							rpt.setPerfilIdEmisor(rs.getLong("R_PERFIL_ID_EMISOR"));
							rpt.setPerfilDetaEmisor(rs.getString("R_PERFIL_ID_EMI_DETA"));
							rpt.setUsuarioIdEmisor(rs.getLong("R_USUARIO_ID_EMISOR"));
							
							rpt.setSituSoliIdEmision(rs.getLong("R_SITU_SOLI_ID_EMI"));
							rpt.setEstaSoliIdEmision(rs.getLong("R_ESTA_SOLI_ID_EMI"));
							
							rpt.setDetalleAsignacion(rs.getString("R_DETALLE_DERIV_EMISOR"));
							
							//rpt.setUsuarioIdRegistroEmi(rs.getLong(""));
							rpt.setFechaRegistroEmision(rs.getString("R_FECHA_REGISTRO_EMISION"));
							//> RECEPTOR.
							rpt.setT01UndOrgRecId(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setT01UndOrgRecDeta(rs.getString("R_T01_UND_ORG_REC_DETA"));
							rpt.setPerfilIdReceptor(rs.getLong("R_PERFIL_ID_RECEPTOR"));
							rpt.setPerfilDetaReceptor(rs.getString("R_T01_UND_ORG_REC_DETA"));
							rpt.setUsuarioIdReceptor(rs.getLong("R_USUARIO_ID_RECEPTOR"));
							
							rpt.setDetalleRespuesta(rs.getString("R_DETALLE_DERIV_RECEPTOR"));
							
							//rpt.setSituSoliIdRecepcion(rs.getLong(""));
							//rpt.setEstaSoliIdRecepcion(rs.getLong(""));
							
							rpt.setUsuarioIdRegistroRec(rs.getLong("R_USUARIO_ID_REGISRO_REC"));
							rpt.setFechaRegistroRecepcion(rs.getString("R_FECHA_REGISTRO_RECEPCION"));
											
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstVerRespuestas "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstLineaTiempoXAreas "+e.getMessage());
		}

		return listarpt;
	}
	
	@Override
	public Boolean actualizar(SolicitudMovimiento prmT) throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public Boolean eliminar(SolicitudMovimiento prmT) throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public List<SolicitudMovimiento> listar(SolicitudMovimiento prmT)
			throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public SolicitudMovimiento buscarXId(SolicitudMovimiento prmT)
			throws PersistenciaException {
		// 
		return null;
	}
	
}
