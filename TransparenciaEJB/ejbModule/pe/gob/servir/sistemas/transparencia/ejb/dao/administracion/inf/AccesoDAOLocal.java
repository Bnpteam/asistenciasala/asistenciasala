package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Acceso;

@Local
public interface AccesoDAOLocal extends GenericDAO<Acceso>{
	
	public List<Acceso> buscarAccesos(Long perfilId) throws PersistenciaException;

	public List<Acceso> buscarMenus(Long perfilId) throws PersistenciaException;

	public List<Acceso> buscarSubMenus(Long perfilId,Long componentePadreId) throws PersistenciaException;

}
