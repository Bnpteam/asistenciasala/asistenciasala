package pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.MovimientoDetalleDAOLocal;
import pe.gob.servir.sistemas.transparencia.model.negocio.DetalleMovimiento;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class MovimientoDetalleDAOImpl  extends GenericDAOImpl<DetalleMovimiento> implements MovimientoDetalleDAOLocal{

	private static final Logger logger = Logger.getLogger(MovimientoDetalleDAOImpl.class);	
	@PersistenceContext(unitName = "PUCirculacion")
	
	private EntityManager em;
	@Override
	public ReturnObject insertar(final DetalleMovimiento prmObj) throws PersistenciaException {
		
		ReturnObject result = new ReturnObject();
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_INSERT_DETALLE_MOVIM"
								+ " ( ?, ?, ?, ?, ?, ?) }");
						
						Long solMov = prmObj.getSolicitudMovimientoId();
						cstm.setLong("P_SOLICITUD_MOVIMIENTO_ID", solMov);
						String tipoDoc = prmObj.getTipoDocumentoId();
						cstm.setString("P_TIPO_DCUMENTO_ID", tipoDoc);	
						String nombDoc = prmObj.getNombreDocumento();
						cstm.setString("P_NOMBRE_DOCUMENTO", nombDoc);
						String uuidDoc = prmObj.getUuidDocumento();
						cstm.setString("P_UUID_DOCUMENTO", uuidDoc);
						Long usuReg = prmObj.getUsuarioIdRegistro();
						cstm.setLong("P_USUARIO_ID_REGISTRO", usuReg);
						cstm.registerOutParameter("P_DETALLE_MOVIMIENTO_ID", OracleTypes.NUMBER);	
						
						cstm.execute();
						Object object=cstm.getObject("P_DETALLE_MOVIMIENTO_ID");
						BigDecimal decId= new BigDecimal(0);						
												
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
						Integer id = decId.intValue();
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:MovimientoSolicitudDAOImpl:insert "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:MovimientoDetalleDAOImpl:insert "+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setSw(prmObj.getResultTransaction());	
		return result;	
	}
	@Override
	public Boolean actualizar(DetalleMovimiento prmT) throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public Boolean eliminar(final DetalleMovimiento prmT) throws PersistenciaException {
		boolean sw=false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_ELIMINAR_DETALL_MOVIM"
								+ "( ?, ?, ? ) }");
						Long idDetMov = prmT.getId();
						cstm.setLong("P_DETALLE_MOVIMIENTO_ID", idDetMov);
						cstm.setLong("P_USUARIO_ID_MODIFICACION", prmT.getUsuarioIdModificacion());
						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);
																									
						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) { // DEV 0
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {//PASA AL OTRO
								prmT.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmT.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmT.setResultTransaction(false);
						}	

												
					} catch (SQLException e) {
						cn.rollback();
						logger.error("SQLException:MovimientoDetalleDAOImpl:eliminar "+e.getMessage());
						prmT.setResultTransaction(false);
						prmT.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			prmT.setResultTransaction(false);
			prmT.setStackException(e,true);
			throw new PersistenciaException("Exception:MovimientoDetalleDAOImpl:eliminar "+e.getMessage());
		}
		
		sw = prmT.getResultTransaction();
		return sw;
	}
	@Override
	public List<DetalleMovimiento> listar(DetalleMovimiento prmT)
			throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public DetalleMovimiento buscarXId(final DetalleMovimiento prmT)
			throws PersistenciaException {
		final DetalleMovimiento tmp = new DetalleMovimiento();	
		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_BUSCARXID_DETALL_MOVIM ( ?, ?) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						Long detMov = prmT.getId();
						cstm.setLong("P_DETALLE_MOVIMIENTO_ID", detMov);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							tmp.setId(rs.getLong("R_DETALLE_MOVIMIENTO_ID"));
							tmp.setSolicitudMovimientoId(rs.getLong("R_SOLICITUD_MOVIMIENTO_ID"));
							tmp.setTipoDocumentoId(rs.getString("R_TIPO_DCUMENTO_ID"));
							tmp.setNombreDocumento(rs.getString("R_NOMBRE_DOCUMENTO"));
							tmp.setUuidDocumento(rs.getString("R_UUID_DOCUMENTO"));
							tmp.setEstado(rs.getString("R_ESTADO"));
							tmp.setUsuarioIdRegistro(rs.getLong("R_USUARIO_ID_REGISTRO"));
							tmp.setFechaRegistro(rs.getDate("R_FECHA_REGISTRO"));
							
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:MovimientoDetalleDAOImpl:buscarXId "+ ex);
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("SQLException:MovimientoDetalleDAOImpl:buscarXId "+e.getMessage());
		}

		return tmp;
	}
	
	@Override
	public List<DetalleMovimiento> lstDocumentosIngresados(final Long prmIdSolicitudMov)
			throws PersistenciaException {
		final List<DetalleMovimiento> listarpt = new ArrayList<DetalleMovimiento>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_DOCS_INGR_X_SOLIC" 
						+ "( ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						Long idSolicitudMov = prmIdSolicitudMov;
						cstm.setLong("P_SOLICITUD_MOVIMIENTO_ID", idSolicitudMov);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							DetalleMovimiento rpt= new DetalleMovimiento();
							rpt.setId(rs.getLong("R_DETALLE_MOVIMIENTO_ID"));
							rpt.setSolicitudMovimientoId(rs.getLong("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01UndOrgEmiId(rs.getString("R_T01_UND_ORG_EMI_ID"));
							rpt.setT01UndOrgEmiDetaCort(rs.getString("R_T01_UND_ORG_EMI_DET_CORT"));
							rpt.setT01UndOrgEmiDetaLarg(rs.getString("R_T01_UND_ORG_EMI_DET_LARG"));
							rpt.setTipoDocumentoId(rs.getString("R_TIPO_DCUMENTO_ID"));
							rpt.setNombreDocumento(rs.getString("R_NOMBRE_DOCUMENTO"));
							rpt.setUuidDocumento(rs.getString("R_UUID_DOCUMENTO"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setUsuarioIdRegistro(rs.getLong("R_USUARIO_ID_REGISTRO"));
							rpt.setFechaRegistro(rs.getDate("R_FECHA_REGISTRO"));
									
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:MovimientoDetalleDAOImpl:lstDocumentosIngresados "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:MovimientoDetalleDAOImpl:lstDocumentosIngresados "+e.getMessage());
		}

		return listarpt;
	}
	// LISTA LOS DOCUMENTOS DE RESPUESTA PARA MOSTRARLO EN EL POPUP DE ROL ASISTENTE OAJ.
	@Override
	public List<DetalleMovimiento> lstDocsDeRespuesta(final String prmIdSolisMovs)
			throws PersistenciaException {
		final List<DetalleMovimiento> listarpt = new ArrayList<DetalleMovimiento>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_DOCS_DRESP_X_SOLICTS" 
						+ "( ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String IdSolisMovs = prmIdSolisMovs;
						cstm.setString("P_SOLICTS_MOVIMNTOS_IDS", IdSolisMovs); // CHANGE
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							DetalleMovimiento rpt= new DetalleMovimiento();
							rpt.setId(rs.getLong("R_DETALLE_MOVIMIENTO_ID"));
							rpt.setSolicitudMovimientoId(rs.getLong("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01UndOrgEmiId(rs.getString("R_T01_UND_ORG_EMI_ID"));
							rpt.setT01UndOrgEmiDetaCort(rs.getString("R_T01_UND_ORG_EMI_DET_CORT"));
							rpt.setT01UndOrgEmiDetaLarg(rs.getString("R_T01_UND_ORG_EMI_DET_LARG"));
							rpt.setTipoDocumentoId(rs.getString("R_TIPO_DCUMENTO_ID"));
							rpt.setNombreDocumento(rs.getString("R_NOMBRE_DOCUMENTO"));
							rpt.setUuidDocumento(rs.getString("R_UUID_DOCUMENTO"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setUsuarioIdRegistro(rs.getLong("R_USUARIO_ID_REGISTRO"));
							rpt.setFechaRegistro(rs.getDate("R_FECHA_REGISTRO"));
									
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:MovimientoDetalleDAOImpl:lstDocumentosIngresados "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:MovimientoDetalleDAOImpl:lstDocumentosIngresados "+e.getMessage());
		}

		return listarpt;
	}
	
}
