package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Componente;

@Remote
public interface ComponenteServiceRemote extends GenericService<Componente>{

	public List<Componente> listarBandejaPorTipoConsultaExt(String tipoConsExt) throws ServicioException;
	
}
