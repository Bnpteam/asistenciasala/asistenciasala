package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.PerfilDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.local.PerfilServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.PerfilServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Stateless
public class PerfilServiceImpl implements PerfilServiceRemote, PerfilServiceLocal {

	@EJB
	private PerfilDAOLocal perfilDAOLocal;
	
	@Override
	public ReturnObject insertar(Perfil prmObj) throws ServicioException {	
		try {
			return this.getPerfilDAOLocal().insertar(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
		
	}

	@Override
	public Boolean actualizar(Perfil prmObj) throws ServicioException {
		try {
			return this.getPerfilDAOLocal().actualizar(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean eliminar(Perfil prmObj) throws ServicioException {
		try {
			return this.getPerfilDAOLocal().eliminar(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Perfil> listar(Perfil prmObj) throws ServicioException {
		try {
			return this.getPerfilDAOLocal().listar(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Perfil buscarXId(Perfil prmObj) throws ServicioException {
		try {
			return this.getPerfilDAOLocal().buscarXId(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Perfil> listarTodos() throws ServicioException {
		try {
			return this.getPerfilDAOLocal().listarTodos();
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Perfil> listarPerfilesPorBandeja(String bandeja) throws ServicioException {
		try {
			return this.getPerfilDAOLocal().listarPerfilesPorBandeja(bandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	public PerfilDAOLocal getPerfilDAOLocal() {
		return perfilDAOLocal;
	}

	public void setPerfilDAOLocal(PerfilDAOLocal perfilDAOLocal) {
		this.perfilDAOLocal = perfilDAOLocal;
	}

	@Override
	public List<Perfil> listar() throws ServicioException {
		try {
			return this.getPerfilDAOLocal().listar();
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Perfil> listarPerfilesTodasBandejas() throws ServicioException {
		try {
			return this.getPerfilDAOLocal().listarPerfilesTodasBandejas();
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
}
