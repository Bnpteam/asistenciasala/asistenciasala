package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Acceso;

@Local
public interface AccesoServiceLocal extends GenericService<Acceso> {

	public List<Acceso> buscarAccesos(Long perfilId) throws ServicioException;

	public List<Acceso> buscarMenus(Long perfilId) throws ServicioException;
	
	public List<Acceso> buscarSubMenus(Long perfilId, Long componentePadreId) throws ServicioException;
	
}
