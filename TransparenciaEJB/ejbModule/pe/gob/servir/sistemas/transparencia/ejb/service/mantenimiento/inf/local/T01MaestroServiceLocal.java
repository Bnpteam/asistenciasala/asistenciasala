package pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;

@Local
public interface T01MaestroServiceLocal extends GenericService<T01Maestro>{
	
	public List<T01Maestro> listarXCodigoTabla(String codigoTabla) throws ServicioException;
	
	public List<T01Maestro> listarXCodigoTablaXValor1(String codigoTabla,String valor1) throws ServicioException;
	
	public List<T01Maestro> listarXCodigoTablaNL(String codigoTabla) throws ServicioException;
	
	public List<T01Maestro> listarXUndOrgCnSecretarias(int prmCodPerfilUser) throws ServicioException;
	
	public List<T01Maestro> listarXCodigoTablaRESP(String codigoTabla) throws ServicioException;
	
	public List<T01Maestro> listarDiasxValor(String codigoTabla) throws ServicioException;

	public List<T01Maestro> listarTodos() throws ServicioException;
	
	public List<T01Maestro> listarSeries() throws ServicioException;
	
	public List<T01Maestro> listarSubSeries(String serie) throws ServicioException;
	
	public boolean validarNombreCorto(T01Maestro t01Maestro) throws ServicioException;

	public boolean validarNombreCortoValor1(T01Maestro t01Maestro) throws ServicioException;

	public T01Maestro insertarDirecto(T01Maestro t01Maestro) throws ServicioException;
	
	public List<T01Maestro> listarItemXValor1(T01Maestro t01Maestro) throws ServicioException;

	public Boolean actualizarPlantilla(T01Maestro t01Maestro) throws ServicioException;
}
