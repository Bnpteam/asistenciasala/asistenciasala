package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Acceso;

@Remote
public interface AccesoServiceRemote extends GenericService<Acceso> {

	public List<Acceso> buscarAccesos(Long perfilId) throws ServicioException;

	public List<Acceso> buscarMenus(Long perfilId) throws ServicioException;
	
	public List<Acceso> buscarSubMenus(Long perfilId, Long componentePadreId) throws ServicioException;
	
}
