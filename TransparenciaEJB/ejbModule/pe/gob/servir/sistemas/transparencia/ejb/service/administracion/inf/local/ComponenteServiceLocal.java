package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Componente;

@Local
public interface ComponenteServiceLocal extends GenericService<Componente> {

	public List<Componente> listarBandejaPorTipoConsultaExt(String tipoConsExt) throws ServicioException;
	
}
