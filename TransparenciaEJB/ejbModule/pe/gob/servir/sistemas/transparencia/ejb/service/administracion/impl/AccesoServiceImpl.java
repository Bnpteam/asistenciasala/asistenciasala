package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.AccesoDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.local.AccesoServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.AccesoServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Acceso;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Stateless
public class AccesoServiceImpl implements AccesoServiceRemote, AccesoServiceLocal {

	@EJB
	private AccesoDAOLocal accesoDAOLocal;
	
	@Override
	public ReturnObject insertar(Acceso prmObj) throws ServicioException {
		try {
			return this.getAccesoDAOLocal().insertar(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean actualizar(Acceso prmObj) throws ServicioException {
		try {
			return this.getAccesoDAOLocal().actualizar(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean eliminar(Acceso prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public List<Acceso> listar(Acceso prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Acceso buscarXId(Acceso prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public List<Acceso> buscarAccesos(Long perfilId) throws ServicioException {
		try {
			return this.getAccesoDAOLocal().buscarAccesos(perfilId);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Acceso> buscarMenus(Long perfilId) throws ServicioException {
		try {
			return this.getAccesoDAOLocal().buscarMenus(perfilId);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Acceso> buscarSubMenus(Long perfilId, Long componentePadreId)
			throws ServicioException {
		try {
			return this.getAccesoDAOLocal().buscarSubMenus(perfilId, componentePadreId);
		} catch (Exception e) {
			throw new ServicioException(e);
		}	
	}

	public AccesoDAOLocal getAccesoDAOLocal() {
		return accesoDAOLocal;
	}

	public void setAccesoDAOLocal(AccesoDAOLocal accesoDAOLocal) {
		this.accesoDAOLocal = accesoDAOLocal;
	}
	
}
