package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.local;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;

@Local
public interface SolicitudServiceLocal  extends GenericService<Solicitud> {

	//public ReturnObject actualizar2(Solicitud prmT) throws ServicioException;
	
}
