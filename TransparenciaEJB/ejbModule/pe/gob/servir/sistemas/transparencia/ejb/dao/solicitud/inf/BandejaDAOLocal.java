package pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;

@Local
public interface BandejaDAOLocal extends GenericDAO<ReporteBandeja> {
	
	public List<ReporteBandeja> lstReporteBandejaSecretariaOAJ(ReporteBandeja tmpRptBandeja) throws PersistenciaException;
	
	public List<ReporteBandeja> lstReporteBandejaAsistenteOAJ(ReporteBandeja tmpRptBandeja) throws PersistenciaException;
	
	public List<ReporteBandeja> lstReporteBandejaJefeOAJ(ReporteBandeja tmpRptBandeja) throws PersistenciaException;
	
	public List<ReporteBandeja> lstReporteBandejaJefeOAJ2(ReporteBandeja tmpRptBandeja) throws PersistenciaException;

	public List<ReporteBandeja> lstReporteBandejaSecretariaUO(ReporteBandeja tmpRptBandeja) throws PersistenciaException;

	public List<ReporteBandeja> lstReporteBandejaResponsableUO(ReporteBandeja tmpRptBandeja) throws PersistenciaException;

	public ReporteBandeja lstUltimoEnvioMsj(ReporteBandeja tmpRptBandeja) throws PersistenciaException;
	
	public List<ReporteBandeja> lstReporteBandejaSeguimiento(ReporteBandeja tmpRptBandeja) throws PersistenciaException;

}
