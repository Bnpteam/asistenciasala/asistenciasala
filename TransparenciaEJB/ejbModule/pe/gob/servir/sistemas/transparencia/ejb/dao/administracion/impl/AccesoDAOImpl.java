package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.AccesoDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Acceso;
import pe.gob.servir.sistemas.transparencia.model.administracion.Componente;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class AccesoDAOImpl extends GenericDAOImpl<Acceso> implements AccesoDAOLocal {

	private static final Logger logger = Logger.getLogger(AccesoDAOImpl.class);
	
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;
	
	@Override
	public ReturnObject insertar(final Acceso prmObj) throws PersistenciaException {
		ReturnObject result = new ReturnObject();
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_INSERTAR_TBL_ACCESO"
								+ "( ?, ?, ?, ?, ? ) }");
					
						cstm.registerOutParameter("P_ACCESO_ID", OracleTypes.NUMBER);
						
						cstm.setLong("P_PERFIL_ID", prmObj.getPerfil().getId());
						cstm.setLong("P_COMPONENTE_ID", prmObj.getComponente().getId());
						
						cstm.setString("P_ASIGNADO", (prmObj.isAsignado())?"1":"0");

						cstm.setLong("P_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());
																											
						cstm.execute();
						
						BigDecimal decId= new BigDecimal(0);
						
						Object object=cstm.getObject("P_ACCESO_ID");

						if (object!=null) {
							decId = (BigDecimal)object;	
						}
												
						Integer id = decId.intValue();
						
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}

												
					} catch (SQLException e) {
						logger.error("SQLException:AccesoDAOImpl:insert "+e.getMessage());
						cn.rollback();
						e.printStackTrace();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:AccesoDAOImpl:insert "+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setSw(prmObj.getResultTransaction());	
		return result;
	}

	@Override
	public Boolean actualizar(final Acceso prmObj) throws PersistenciaException {
		
		boolean sw=false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_ACTUALIZAR_TBL_ACCESO"
								+ "( ?, ?, ?, ? ) }");
						
						cstm.setLong("P_ACCESO_ID", prmObj.getId());
						
						cstm.setString("P_ASIGNADO", (prmObj.isAsignado())?"1":"0");

						cstm.setLong("P_USUARIO_ID_MODIFICACION", prmObj.getUsuarioIdModificacion());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);
																									
						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	

												
					} catch (SQLException e) {
						cn.rollback();
						logger.error("SQLException:AccesoDAOImpl:actualizar "+e.getMessage());
						e.printStackTrace();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:AccesoDAOImpl:actualizar "+e.getMessage());
		}
		
		sw = prmObj.getResultTransaction();
		return sw;
	}

	@Override
	public Boolean eliminar(final Acceso prmObj) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public List<Acceso> listar(final Acceso prmObj) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public Acceso buscarXId(final Acceso prmObj) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public List<Acceso> buscarAccesos(final Long perfilId) throws PersistenciaException {
		final List<Acceso> lstAcceso = new ArrayList<Acceso>();
		try {
			Session hbSession = em.unwrap(Session.class);	
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_BUSCR_ACCS_XPERFL_TBL_ACCES( ?, ? )}");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setLong("P_PERFIL_ID", perfilId);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {

							Acceso acceso = new Acceso();
							acceso.setId(rs.getLong("ACCESO_ID"));
							acceso.setComponente(new Componente());
							acceso.getComponente().setId(rs.getLong("COMPONENTE_ID"));
							acceso.getComponente().setNombreInterno(rs.getString("NOMBRE_INTERNO"));
							acceso.getComponente().setNombreExterno(rs.getString("NOMBRE_EXTERNO"));
							Object asignado = rs.getString("ASIGNADO");
							boolean sw = (asignado==null)?false:(asignado.equals("1")?true:false);
							acceso.setAsignado(sw);							
							lstAcceso.add(acceso);
						}						
					} catch (Exception e) {
						logger.error("SQLException:AccesoDAOImpl:buscarAccesos "+e.getMessage());
						e.printStackTrace();
					}
					finally{
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});	
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("SQLException:AccesoDAOImpl:buscarAccesos "+e.getMessage());	
		}
		
		return lstAcceso;
	}

	@Override
	public List<Acceso> buscarMenus(final Long perfilId) throws PersistenciaException {
		final List<Acceso> lstAcceso = new ArrayList<Acceso>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_BUSCAR_MENU_TBL_ACCESO"
								+ "( ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("P_PERFIL_ID", perfilId);

						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {							
							Acceso acceso= new Acceso();
							acceso.setId(rs.getLong("ACCESO_ID"));
							Componente componente= new Componente();
							componente.setId(rs.getLong("COMPONENTE_ID"));			
							componente.setNombreExterno(rs.getString("NOMBRE_EXTERNO"));
							acceso.setComponente(componente);
							Object obj=rs.getString("ASIGNADO");
							boolean sw= (obj==null)?false:(obj.equals("1")?true:false);
							acceso.setAsignado(sw);
							lstAcceso.add(acceso);
						
						}
					} catch (Exception e) {
						e.printStackTrace();
						logger.error("SQLException:AccesoDAOImpl:buscarMenus "+e.getMessage());
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("SQLException:AccesoDAOImpl:buscarMenus "+e.getMessage());
		}

		return lstAcceso;
	}

	@Override
	public List<Acceso> buscarSubMenus(final Long perfilId, final Long componentePadreId)
			throws PersistenciaException {
		final List<Acceso> lstAcceso = new ArrayList<Acceso>();
		
		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_BUSCAR_SUBMENU_TBL_ACCESO"
								+ "( ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("P_PERFIL_ID", perfilId);
						cstm.setLong("P_COMPONENTE_PADRE_ID", componentePadreId);

						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Acceso acceso= new Acceso();
							acceso.setId(rs.getLong("ACCESO_ID"));
							Componente componente= new Componente();
							componente.setId(rs.getLong("COMPONENTE_ID"));
							componente.setPadreId(rs.getString("COMPONENTE_PADRE_ID"));
							componente.setNombreExterno(rs.getString("NOMBRE_EXTERNO"));
							acceso.setComponente(componente);
							Object obj=rs.getString("ASIGNADO");
							boolean sw= (obj==null)?false:(obj.equals("1")?true:false);
							acceso.setAsignado(sw);
							lstAcceso.add(acceso);
						
						}
					} catch (Exception e) {
						e.printStackTrace();
						logger.error("SQLException:AccesoDAOImpl:buscarSubMenus "+e.getMessage());
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("SQLException:AccesoDAOImpl:buscarSubMenus "+e.getMessage());
		}

		return lstAcceso;
	}

}
