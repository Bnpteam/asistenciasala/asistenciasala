package pe.gob.servir.sistemas.transparencia.ejb.dao.mantenimiento.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;

@Local
public interface T01MaestroDAOLocal extends GenericDAO<T01Maestro>{

	
	public List<T01Maestro> listarXCodigoTabla(String codigoTabla) throws PersistenciaException;
	
	public List<T01Maestro> listarXCodigoTablaXValor1(String codigoTabla,String valor1) throws PersistenciaException;
	
	public List<T01Maestro> listarXCodigoTablaNL(String codigoTabla) throws PersistenciaException;
	
	public List<T01Maestro> listarXUndOrgCnSecretarias(int prmCodPerfilUser) throws PersistenciaException;
	
	public List<T01Maestro> listarXCodigoTablaResp(String codigoTabla) throws PersistenciaException;

	public List<T01Maestro> listarDiasxValor(String codigoTabla) throws PersistenciaException;

	public List<T01Maestro> listarTodos() throws PersistenciaException;

	public List<T01Maestro> listarSeries() throws PersistenciaException;
	
	public List<T01Maestro> listarSubSeries(String serie) throws PersistenciaException;

	public boolean validarNombreCorto(T01Maestro t01Maestro) throws PersistenciaException;
		
	public boolean validarNombreCortoValor1(T01Maestro t01Maestro) throws PersistenciaException;

	public T01Maestro insertarDirecto(T01Maestro t01Maestro) throws PersistenciaException;
	
	public List<T01Maestro> listarItemXValor1(T01Maestro t01Maestro) throws PersistenciaException;
	
	public Boolean actualizarPlantilla(T01Maestro t01Maestro) throws PersistenciaException;
	
}
