package pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import pe.gob.servir.sistemas.transparencia.ejb.dao.mantenimiento.inf.T01MaestroDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.local.T01MaestroServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.remoto.T01MaestroServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.systems.util.retorno.ReturnObject;


@Stateless
public class T01MaestroServiceImpl implements T01MaestroServiceRemote, T01MaestroServiceLocal {

	@EJB
	private T01MaestroDAOLocal t01MaestroDAOLocal;
	
	@Override
	public ReturnObject insertar(T01Maestro prmT01Maestro) throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().insertar(prmT01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}

	}

	@Override
	public Boolean actualizar(T01Maestro prmT01Maestro) throws ServicioException {		
		try {
			return this.getT01MaestroDAOLocal().actualizar(prmT01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}

	}

	@Override
	public Boolean eliminar(T01Maestro prmT01Maestro) throws ServicioException {
		
		try {
			return this.getT01MaestroDAOLocal().eliminar(prmT01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<T01Maestro> listar(T01Maestro prmT01Maestro) throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listar(prmT01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
		
	}

	@Override
	public T01Maestro buscarXId(T01Maestro prmT01Maestro) throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().buscarXId(prmT01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
		
	}
	
	@Override
	public List<T01Maestro> listarTodos() throws ServicioException{
		try {
			return this.getT01MaestroDAOLocal().listarTodos();
		} catch (Exception e) {
			throw new ServicioException(e);
		}
		
	}
	
	public List<T01Maestro> listarSeries() throws ServicioException{
		try {
			return this.getT01MaestroDAOLocal().listarSeries();
		} catch (Exception e) {
			throw new ServicioException(e);
		}	
	}
	public List<T01Maestro> listarSubSeries(String serie) throws ServicioException{
		try {
			return this.getT01MaestroDAOLocal().listarSubSeries(serie);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<T01Maestro> listarXCodigoTabla(String codigoTabla)
			throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listarXCodigoTabla(codigoTabla);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<T01Maestro> listarXCodigoTablaXValor1(String codigoTabla,
			String valor1) throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listarXCodigoTablaXValor1(codigoTabla,valor1);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<T01Maestro> listarItemXValor1(T01Maestro t01Maestro) throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listarItemXValor1(t01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public boolean validarNombreCorto(T01Maestro t01Maestro)throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().validarNombreCorto(t01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	@Override
	public boolean validarNombreCortoValor1(T01Maestro t01Maestro)throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().validarNombreCortoValor1(t01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}


	@Override
	public T01Maestro insertarDirecto(T01Maestro t01Maestro)
			throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().insertarDirecto(t01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	

	@Override
	public List<T01Maestro> listarXCodigoTablaNL(String codigoTabla) 
			throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listarXCodigoTablaNL(codigoTabla);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<T01Maestro> listarXUndOrgCnSecretarias(int prmCodPerfilUser) 
			throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listarXUndOrgCnSecretarias(prmCodPerfilUser);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean actualizarPlantilla(T01Maestro t01Maestro)
			throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().actualizarPlantilla(t01Maestro);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	
	public T01MaestroDAOLocal getT01MaestroDAOLocal() {
		return t01MaestroDAOLocal;
	}

	public void setT01MaestroDAOLocal(T01MaestroDAOLocal t01MaestroDAOLocal) {
		this.t01MaestroDAOLocal = t01MaestroDAOLocal;
	}

	@Override
	public List<T01Maestro> listarDiasxValor(String codigoTabla)
			throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listarDiasxValor(codigoTabla);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<T01Maestro> listarXCodigoTablaRESP(String codigoTabla)
			throws ServicioException {
		try {
			return this.getT01MaestroDAOLocal().listarXCodigoTablaResp(codigoTabla);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}




}
