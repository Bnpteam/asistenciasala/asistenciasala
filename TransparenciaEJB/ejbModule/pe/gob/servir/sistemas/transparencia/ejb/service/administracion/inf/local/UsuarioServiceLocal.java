package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Local
public interface UsuarioServiceLocal extends GenericService<Usuario> {

	public Usuario validarAcceso(Usuario usuario) throws ServicioException;
	
	public Boolean cambiarClave(Usuario usuario) throws ServicioException; 
	
	public ReturnObject validarUsuario(final Usuario prmUsuario) throws ServicioException;
	
	public Usuario obtenerUsuarioAsignado(String prmBandeja) throws ServicioException;

	public List<Usuario> listarUsuariosPorPerfil(Perfil prmPerfil) throws ServicioException;
	
	public List<Usuario> listarUsuarioXPerfilXArea(Usuario prmPerfil) throws ServicioException;
	
	public List<Usuario> listarUsuariosDispPorPerfil(Perfil prmPerfil) throws ServicioException;
	
	public List<Usuario> listarUsuariosPorBandeja(String bandeja) throws ServicioException;
	
	public List<Usuario> listarTodos() throws ServicioException;
	
	public Boolean consultarExistencia(Usuario usuario) throws ServicioException;
	
	public List<Usuario> listarUsuarioXPerfilXAreaAlternativo(Usuario prmObj)  throws ServicioException;

}
