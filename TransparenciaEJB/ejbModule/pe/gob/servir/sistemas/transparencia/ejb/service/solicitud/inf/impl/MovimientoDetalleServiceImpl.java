package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.MovimientoDetalleDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.local.MovimientoDetalleServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoDetalleServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.negocio.DetalleMovimiento;
import pe.gob.servir.systems.util.retorno.ReturnObject;


@Stateless
public class MovimientoDetalleServiceImpl implements MovimientoDetalleServiceRemote,
	MovimientoDetalleServiceLocal {

	@EJB
	private MovimientoDetalleDAOLocal movimientoDetalleDAOLocal;

	@Override
	public ReturnObject insertar(DetalleMovimiento prmT) throws ServicioException {
		try {
			return this.getMovimientoDetalleDAOLocal().insertar(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean actualizar(DetalleMovimiento prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Boolean eliminar(DetalleMovimiento prmT) throws ServicioException {
		try {
			return this.getMovimientoDetalleDAOLocal().eliminar(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<DetalleMovimiento> listar(DetalleMovimiento prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public DetalleMovimiento buscarXId(DetalleMovimiento prmT) throws ServicioException {
		try {
			return this.getMovimientoDetalleDAOLocal().buscarXId(prmT);
			
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<DetalleMovimiento> lstDocumentosIngresados(Long prmIdSolicitudMov) throws ServicioException {
		try {
			return this.getMovimientoDetalleDAOLocal().lstDocumentosIngresados(prmIdSolicitudMov);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<DetalleMovimiento> lstDocsDeRespuesta(String prmIdSolisMovs) throws ServicioException {
		try {
			return this.getMovimientoDetalleDAOLocal().lstDocsDeRespuesta(prmIdSolisMovs);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	public MovimientoDetalleDAOLocal getMovimientoDetalleDAOLocal() {
		return movimientoDetalleDAOLocal;
	}

	public void setMovimientoDetalleDAOLocal(
			MovimientoDetalleDAOLocal movimientoDetalleDAOLocal) {
		this.movimientoDetalleDAOLocal = movimientoDetalleDAOLocal;
	}

}
