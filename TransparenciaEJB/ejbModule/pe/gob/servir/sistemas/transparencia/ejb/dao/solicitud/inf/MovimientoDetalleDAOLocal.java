package pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.negocio.DetalleMovimiento;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Local
public interface MovimientoDetalleDAOLocal extends GenericDAO<DetalleMovimiento> {
	public ReturnObject insertar(DetalleMovimiento prmObj) throws PersistenciaException;
	
	public List<DetalleMovimiento> lstDocumentosIngresados(Long prmIdSolicitudMov) throws PersistenciaException;

	public List<DetalleMovimiento> lstDocsDeRespuesta(String prmIdSolisMovs) throws PersistenciaException;
}
