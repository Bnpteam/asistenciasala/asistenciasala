package pe.gob.servir.sistemas.transparencia.ejb.service.smtp;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import pe.gob.servir.sistemas.transparencia.model.dto.CorreoModel;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import pe.gob.servir.systems.util.resource.ResourceUtil;
import pe.gob.servir.systems.util.smtp.SmtpObject;

import com.sun.mail.smtp.SMTPMessage;


public class SmtpSendServiceImpl implements Serializable, SmtpSendService{

	private static final long serialVersionUID = -5155736630400764588L;
	private static final Logger logger = Logger.getLogger(SmtpSendServiceImpl.class);
	// 1. Env�o de correo SIN adjuntos.
    @SuppressWarnings("finally")
	private static synchronized boolean Mail(String titulo, String mensaje, String paraEmail) 
    {
        boolean envio = false;

        final String accountMail = ResourceUtil.getKey("config","mail.accountMail");
        final String accountPass = ResourceUtil.getKey("config","mail.accountPass"); 
        final String personalName = ResourceUtil.getKey("config","mail.personalName");
        System.out.println("0001"); 
        try {

            //Propiedades de la conexion
            Properties propiedades = new Properties();
            propiedades.setProperty("mail.smtp.host", ResourceUtil.getKey("config","mail.host"));
            propiedades.setProperty("mail.smtp.port", ResourceUtil.getKey("config","mail.port"));
            propiedades.setProperty("mail.smtp.auth", ResourceUtil.getKey("config","mail.auth"));
            propiedades.setProperty("mail.smtp.starttls.enable", ResourceUtil.getKey("config","mail.starttls.enable"));
            System.out.println("0"); 
            //Preparamos la Sesion autenticando al usuario
            Session session = Session.getInstance(propiedades, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(accountMail, accountPass);
                }
            });
            System.out.println("1"); 
            //Preparamos el Mensaje SIN ADJUNTOS
            MimeMessage message = new MimeMessage(session);
            message.setSender(new InternetAddress(accountMail));
            message.setSubject(titulo);
            message.setContent(mensaje, "text/html; charset=utf-8");
            
			message.setFrom(new InternetAddress(accountMail,personalName));
            message.setReplyTo(InternetAddress.parse(accountMail));
            System.out.println("2"); 
            if (paraEmail.indexOf(',') > 0) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(paraEmail));
            } else {
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(paraEmail));
            }
            System.out.println("3"); 
            //env�o del mensaje
            //Transport.send(message);
            Transport transport = session.getTransport("smtp");
            message.saveChanges();
            Transport.send(message);
            System.out.println("4"); 
            transport.close();
            System.out.println("5"); 
            envio = true;
            System.out.println("6"); 
        } catch (MessagingException e) {
            envio = false;
            e.printStackTrace();
            System.out.println("7"); 
        } finally {
            return envio;
        }
    }
    // 2. Env�o de correo CON adjuntos.
    @SuppressWarnings("finally")
	private static synchronized boolean Mail(String titulo, String mensaje, String paraEmail, ArrayList<FileAlfresco> prmLstFileAlfresco) 
    {
        boolean envio = false;

        final String accountMail = ResourceUtil.getKey("config","mail.accountMail");
        final String accountPass = ResourceUtil.getKey("config","mail.accountPass"); 
        final String personalName = ResourceUtil.getKey("config","mail.personalName");
        System.out.println("0002"); 
        try {

            //Propiedades de la conexion
            Properties propiedades = new Properties();
            propiedades.setProperty("mail.smtp.host", ResourceUtil.getKey("config","mail.host"));
            propiedades.setProperty("mail.smtp.port", ResourceUtil.getKey("config","mail.port"));
            propiedades.setProperty("mail.smtp.auth", ResourceUtil.getKey("config","mail.auth"));
            propiedades.setProperty("mail.smtp.starttls.enable", ResourceUtil.getKey("config","mail.starttls.enable"));
            System.out.println("0"); 
            //Preparamos la Sesion autenticando al usuario
            Session session = Session.getInstance(propiedades, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(accountMail, accountPass);
                }
            });
            System.out.println("1"); 
            //Preparamos el Mensaje CON ADJUNTOS
            MimeMessage message = new MimeMessage(session);
            MimeMultipart multiParte = new MimeMultipart();
            
	            MimeBodyPart  texto = new MimeBodyPart();
	            texto.setContent(mensaje, "text/html; charset=utf-8");
	            
	            multiParte.addBodyPart(texto);
	            System.out.println("2"); 
	            //int cont = 0;
	            for (FileAlfresco tmpFileAlfresco: prmLstFileAlfresco) {
	            	MimeBodyPart  adjunto = new MimeBodyPart();
	            	String fullRuta = tmpFileAlfresco.getRutaServidorTemp();
	            	adjunto.setDataHandler(new DataHandler(new FileDataSource(fullRuta))); //Seteamos la ruta del archivo obtenido.
	            	String nombreDoc = tmpFileAlfresco.getFileName();
		            adjunto.setFileName(nombreDoc); //Seteamos el nombre.
		            multiParte.addBodyPart(adjunto);
		            //cont ++;
		            //logger.debug("FILES ENVIADOS SMTP: " + cont);
		            /*
		            File file = new File(tmpFileAlfresco.getRutaServidorTemp());
		            file.delete();*/
	            }
            
            message.setSender(new InternetAddress(accountMail));
            message.setSubject(titulo);
            //message.setContent(mensaje, "text/html; charset=utf-8");
            System.out.println("3"); 
			message.setFrom(new InternetAddress(accountMail,personalName));
            message.setReplyTo(InternetAddress.parse(accountMail));

            if (paraEmail.indexOf(',') > 0) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(paraEmail));
            } else {
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(paraEmail));
            }
            
            message.setContent(multiParte);
            System.out.println("4"); 
            //env�o del mensaje
            //Transport.send(message);
            Transport transport = session.getTransport("smtp");
            message.saveChanges();
            Transport.send(message);
            System.out.println("5"); 
            transport.close();
            envio = true;
            System.out.println("6"); 
            //Borrando los archivos alojados en la carpeta temporales.
            for (FileAlfresco tmpFileAlfresco: prmLstFileAlfresco) {
	            /**/
	            File file = new File(tmpFileAlfresco.getRutaServidorTemp());
	            file.delete();
            }
        } catch (MessagingException e) {
            envio = false;
            System.out.println("7"); 
          //Borrando los archivos alojados en la carpeta temporales.
            for (FileAlfresco tmpFileAlfresco: prmLstFileAlfresco) {
	            /**/
	            File file = new File(tmpFileAlfresco.getRutaServidorTemp());
	            file.delete();
            }
            e.printStackTrace();
        } finally {
            return envio;
        }
    }
    // 1. Env�o de correo SIN adjuntos.
    public boolean envioCorreoUsuario(String titulo, String mensaje, String paraEmail)
    {
    	return SmtpSendServiceImpl.Mail(titulo, mensaje, paraEmail);
    }
	// 2. Env�o de correo CON adjuntos.
    public boolean envioCorreoUsuario(String titulo, String mensaje, String paraEmail, ArrayList<FileAlfresco> prmLstFileAlfresco)
    {
    	return SmtpSendServiceImpl.Mail(titulo, mensaje, paraEmail, prmLstFileAlfresco);
    }
    
    public static void main(String[] args) {
		// 	
    	SmtpSendService service = new SmtpSendServiceImpl();
    	logger.info("Enviando correo usuario o a administrado.");
		logger.info(service.envioCorreoUsuario("MENSAJE DE PRUEBA TRANSPARENCIA", "CUERPO DEL MENSAJE", "@gmail.com"));
	}
	@Override
	public boolean envioCorreoUsuarioSinTemplate(CorreoModel correo) {
		Session session = buildMailSession(correo.getSmtpObject());
		return SmtpSendServiceImpl.sendMailWithoutTemplate(correo, session);
	}
	
	  public static Session buildMailSession(final SmtpObject smtpObject) {
		    Properties mailProps = new Properties();
		    mailProps.put("mail.transport.protocol", "smtp");
		    mailProps.put("mail.host", smtpObject.getHost());
		   // mailProps.put("mail.from", "desarrollo03.otie@bnp.gob.pe");
		    mailProps.put("mail.smtp.starttls.enable", smtpObject.getEnable());
		    mailProps.put("mail.smtp.port", smtpObject.getPort());
		    mailProps.put("mail.smtp.auth", smtpObject.getAuth());
		    // final, because we're using it in the closure below
		    final PasswordAuthentication usernamePassword = new PasswordAuthentication(
		    		smtpObject.getAccountMail(), smtpObject.getAccountPass());
		    Authenticator auth = new Authenticator() {
		      protected PasswordAuthentication getPasswordAuthentication() {
		        return usernamePassword;
		      }
		    };
		    Session session = Session.getInstance(mailProps, auth);
		    session.setDebug(true);
		    return session;
	  }  
	  
		public static boolean sendMailWithoutTemplate(CorreoModel correo, Session session){
			boolean envio = false;

			try {
				    SMTPMessage msg = new SMTPMessage(session);
				    MimeMultipart content = new MimeMultipart("related");
				    
				    // HTML part
				    MimeBodyPart textPart = new MimeBodyPart();
				    textPart.setText(correo.getTemplateTexto(),"US-ASCII", "html");			    
				    content.addBodyPart(textPart);

				    // Image part
				    MimeBodyPart imagePart = new MimeBodyPart();
				    imagePart.attachFile(correo.getSmtpObject().getImageInlinePath() + correo.getSmtpObject().getImageInlineName());
				    imagePart.setContentID("<" + correo.getSmtpObject().getImageCID() + ">");
				    imagePart.setHeader("Content-Transfer-Encoding", "base64");
				    imagePart.setDisposition(MimeBodyPart.INLINE);
				    content.addBodyPart(imagePart);
				    
				    msg.setContent(content);
				    msg.setSubject(correo.getAsunto());
				    
					msg.setFrom(new InternetAddress(correo.getSmtpObject().getAccountMail(),correo.getSmtpObject().getPersonalName()));
					msg.setSender(new InternetAddress(correo.getSmtpObject().getAccountMail()));
					msg.setReplyTo(InternetAddress.parse(correo.getSmtpObject().getAccountMail()));
					
					if( correo != null && correo.getPara() != null &&  correo.getPara().length > 0 ){
						 for (String para: correo.getPara()) {
							 msg.addRecipient(Message.RecipientType.TO, new InternetAddress(para));
						 }
						 if(correo.getConCopia()!=null && correo.getConCopia().length>0){
							 for(String correoCc: correo.getConCopia()){
								 msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(correoCc));
							 }
						 }
							 
					}
				    Transport.send(msg);
				    envio = true;
			} catch (AddressException e) {
			   logger.error("ERROR: AddressException Mail " + e.getMessage(),e);
			   envio = false;
			} catch (MessagingException e) {
			   logger.error("ERROR: MessagingException Mail " + e.getMessage(),e);
			   envio = false;
			} catch (Exception e) {
			   logger.error("ERROR: Exception Mail " + e.getMessage(),e);
			   envio = false;
			}
			return envio;
	    }	  
}

