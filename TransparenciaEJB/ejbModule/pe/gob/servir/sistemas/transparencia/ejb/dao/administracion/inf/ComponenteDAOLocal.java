package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Componente;

@Local
public interface ComponenteDAOLocal extends GenericDAO<Componente>{

	public List<Componente> listarBandejaPorTipoConsultaExt(String tipoConsExt) throws PersistenciaException;
	
}
