package pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.impl.UsuarioDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.SolicitudDAOLocal;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;
import pe.gob.servir.systems.util.validator.VO;

@Stateless
public class SolicitudDAOImpl extends GenericDAOImpl<Solicitud> implements SolicitudDAOLocal {

	
	private static final Logger logger = Logger.getLogger(UsuarioDAOImpl.class);
	
	@PersistenceContext(unitName = "PUTransparencia")
	private EntityManager em;
	
	@Override
	public ReturnObject insertar(final Solicitud prmObj) throws PersistenciaException {
		
		ReturnObject result = new ReturnObject();
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_INSERTAR_SOLICITUD"
								+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
										
						cstm.setString("E_TIPO_DOCUMENTO_IDENTIDAD_ID", prmObj.getTipoDocumentoIdentidadId());
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", prmObj.getNumeroDocumentoIdentidad());
						cstm.setString("E_APELLIDO_PATERNO", prmObj.getApellidoPaterno().trim().toUpperCase());
						cstm.setString("E_APELLIDO_MATERNO", prmObj.getApellidoMaterno().trim().toUpperCase());
						cstm.setString("E_NOMBRES", prmObj.getNombres().trim().toUpperCase());
						cstm.setString("E_DIRECCION_DOMICILIO", prmObj.getDireccionDomicilio().trim().toUpperCase());
						cstm.setString("E_CODDEP", prmObj.getCoddep());
						cstm.setString("E_CODPRO", prmObj.getCodpro());
						cstm.setString("E_CODDIS", prmObj.getCoddis());
						cstm.setString("E_TELEFONO", prmObj.getTelefono());
						cstm.setString("E_CELULAR", prmObj.getCelular());
						cstm.setString("E_CORREO_ELECTRONICO", prmObj.getCorreo_electronico().trim().toLowerCase());					
						cstm.setString("E_DETALLE_SOLICITUD", prmObj.getDetalleSolicitud());
						cstm.setString("E_DEPENDENCIA_DE_INFORMACION", prmObj.getDependenciaDeInformacion());
						cstm.setString("E_FORMA_DE_ENTREGA", prmObj.getFormaDeEntrega());
						cstm.setString("E_FLAG_AUTORIZA_RECOJO", prmObj.getFlag_autoriza_recojo());
						cstm.setString("E_PERS_AUTO_NRO_DOCUMENTO", prmObj.getPersAutonroDocumento());
						cstm.setString("E_PERS_AUTO_NOMBRE_APELLIDOS", prmObj.getPersAutoNombreApellidos().trim().toUpperCase());
						cstm.setString("E_OBSERVACION", prmObj.getObservacion().trim().toUpperCase());
						cstm.setString("E_FLAG_DECLARACION_JURADA", prmObj.getFlagDeclaracioJurada());
						cstm.setString("E_FLAG_NOTIF_ELECTRONICA", prmObj.getFlagNotifElectronica());
						cstm.setLong("E_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());
						cstm.registerOutParameter("S_SOLICITUD_TRANSPARENCIA_ID", OracleTypes.NUMBER);
						cstm.registerOutParameter("S_CORRELATIVO", OracleTypes.NUMBER);	
						
						cstm.execute();
						Object object=cstm.getObject("S_CORRELATIVO");
						BigDecimal decId= new BigDecimal(0);						
												
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
						Integer id = decId.intValue();
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:SolicitudDAOImpl:insert "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					/*} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						logger.error("SQLException:SolicitudDAOImpl:insert "+ e.getMessage());*/
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:SolicitudDAOImpl:insert "+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setSw(prmObj.getResultTransaction());	
		return result;	
	}
	
	@Override
	public Boolean actualizar(final Solicitud prmT) throws PersistenciaException {
		boolean sw=false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_ACTUALIZAR_SOLICITUD"
								+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
						
						cstm.setLong("P_SOLICITUD_TRANSPARENCIA_ID", prmT.getId());
						cstm.setLong("P_VERSION_SOLICITUD", prmT.getVersion_solicitud());
						cstm.setString("P_TIPO_DOCUMENTO_IDENTIDAD_ID", prmT.getTipoDocumentoIdentidadId());
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", prmT.getNumeroDocumentoIdentidad());
						if(VO.isEmpty(prmT.getApellidoPaterno())){ prmT.setApellidoPaterno(""); } //Evita nulls para uperCase
						cstm.setString("P_APELLIDO_PATERNO", prmT.getApellidoPaterno().trim().toUpperCase());
						if(VO.isEmpty(prmT.getApellidoMaterno())){ prmT.setApellidoMaterno(""); } //Evita nulls para uperCase
						cstm.setString("P_APELLIDO_MATERNO", prmT.getApellidoMaterno().trim().toUpperCase());
						if(VO.isEmpty(prmT.getNombres())){ prmT.setNombres(""); } //Evita nulls para uperCase
						cstm.setString("P_NOMBRES", prmT.getNombres().trim().toUpperCase());
						if(VO.isEmpty(prmT.getDireccionDomicilio())){ prmT.setDireccionDomicilio(""); } //Evita nulls para uperCase
						cstm.setString("P_DIRECCION_DOMICILIO", prmT.getDireccionDomicilio().trim().toUpperCase());
						cstm.setString("P_CODDEP", prmT.getCoddep());
						cstm.setString("P_CODPRO", prmT.getCodpro());
						cstm.setString("P_CODDIS", prmT.getCoddis());
						cstm.setString("P_TELEFONO", prmT.getTelefono());
						cstm.setString("P_CELULAR", prmT.getCelular());
						if(VO.isEmpty(prmT.getCorreo_electronico())){ prmT.setCorreo_electronico(""); } //Evita nulls para uperCase
						cstm.setString("P_CORREO_ELECTRONICO", prmT.getCorreo_electronico().trim().toLowerCase());					
						if(VO.isEmpty(prmT.getDetalleSolicitud())){ prmT.setDetalleSolicitud(""); } //Evita nulls para uperCase
						cstm.setString("P_DETALLE_SOLICITUD", prmT.getDetalleSolicitud().trim());
						cstm.setString("P_DEPENDENCIA_DE_INFORMACION", prmT.getDependenciaDeInformacion());
						cstm.setString("P_FORMA_DE_ENTREGA", prmT.getFormaDeEntrega());
						cstm.setString("P_FLAG_AUTORIZA_RECOJO", prmT.getFlag_autoriza_recojo());
						if(VO.isEmpty(prmT.getPersAutonroDocumento())){ prmT.setPersAutonroDocumento(""); }
						cstm.setString("P_PERS_AUTO_NRO_DOCUMENTO", prmT.getPersAutonroDocumento());
						if(VO.isEmpty(prmT.getPersAutoNombreApellidos())){ prmT.setPersAutoNombreApellidos(""); } //Evita nulls
						cstm.setString("P_PERS_AUTO_NOMBRE_APELLIDOS", prmT.getPersAutoNombreApellidos().trim().toUpperCase());
						if(VO.isEmpty(prmT.getObservacion())){ prmT.setObservacion(""); } //Evita nulls
						cstm.setString("P_OBSERVACION", prmT.getObservacion().trim().toUpperCase());
						cstm.setString("P_FLAG_DECLARACION_JURADA", prmT.getFlagDeclaracioJurada());
						cstm.setString("P_FLAG_NOTIF_ELECTRONICA", prmT.getFlagNotifElectronica());
						cstm.setLong("P_USUARIO_ID_MODIFICACION", prmT.getUsuarioIdModificacion());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);						
																									
						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmT.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmT.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmT.setResultTransaction(false);
						}	

						System.out.println("ACTUALIZAR");			
					} catch (SQLException e) {
						cn.rollback();
						logger.error("SQLException:DocumentoDAOImpl:actualizar "+e.getMessage());
						prmT.setResultTransaction(false);
						prmT.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			prmT.setResultTransaction(false);
			prmT.setStackException(e,true);
			throw new PersistenciaException("Exception:SolicitudDAOImpl:actualizar "+e.getMessage());
		}
		
		sw = prmT.getResultTransaction();
		return sw;
	}

	@Override
	public Boolean eliminar(Solicitud prmT) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public List<Solicitud> listar(Solicitud prmT) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public Solicitud buscarXId(final Solicitud prmT) throws PersistenciaException {
		final Solicitud tmpSolicitud = new Solicitud();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_TRANSPARENCIA.SP_BUSCARXID_SOLICITUD( ?, ?) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("P_SOLICITUD_TRANSPARENCIA_ID", prmT.getId());
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							tmpSolicitud.setId(rs.getLong("R_SOLICITUD_TRANSPARENCIA_ID"));
							tmpSolicitud.setCorrelativo(rs.getLong("R_CORRELATIVO_SOLICITUD"));
							tmpSolicitud.setVersion_solicitud(rs.getInt("R_VERSION_SOLICITUD"));
							//Punto I.
							tmpSolicitud.setFuncionarioResponsable("OFICINA DE ASESORIA JUR�DICA");
							//Punto II.
							tmpSolicitud.setTipoDocumentoIdentidadId(rs.getString("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							tmpSolicitud.setNumeroDocumentoIdentidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							tmpSolicitud.setApellidoPaterno(rs.getString("R_APELLIDO_PATERNO"));
							tmpSolicitud.setApellidoMaterno(rs.getString("R_APELLIDO_MATERNO"));
							tmpSolicitud.setNombres(rs.getString("R_NOMBRES"));
							tmpSolicitud.setDireccionDomicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							tmpSolicitud.setCoddep(rs.getString("R_CODDEP"));
							tmpSolicitud.setCodpro(rs.getString("R_CODPRO"));
							String codDis = rs.getString("R_CODDIS");
							tmpSolicitud.setCoddis(codDis);
							//COD DEP PRO DIS
							tmpSolicitud.setTelefono(rs.getString("R_TELEFONO"));
							tmpSolicitud.setCelular(rs.getString("R_CELULAR"));
							tmpSolicitud.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							//Punto III.
							tmpSolicitud.setDetalleSolicitud(rs.getString("R_DETALLE_SOLICITUD"));
							//Punto IV.
							tmpSolicitud.setDependenciaDeInformacion(rs.getString("R_DEPENDENCIA_DE_INFO_ID"));
							//Punto V.
							tmpSolicitud.setFormaDeEntrega(rs.getString("R_FORMA_DE_ENTREGA"));
							//Punto VI.
							tmpSolicitud.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							tmpSolicitud.setPersAutonroDocumento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							tmpSolicitud.setPersAutoNombreApellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							//Punto VII.
							tmpSolicitud.setObservacion(rs.getString("R_OBSERVACION"));
							//Punto VIII.
							tmpSolicitud.setFlagDeclaracioJurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							
							
						}
					} catch (Exception e) {
						logger.error("SQLException:SolicitudDAOImpl:buscarXId "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:SolicitudDAOImpl:buscarXId "+e);
		}

		return tmpSolicitud;
	}

}
