package pe.gob.bnp.dapi.domain.entity;

import java.io.InputStream;
import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class PersonalPhoto implements Serializable {

	private static final long serialVersionUID = 6L;
	private String fotoId;
	private byte[] fotoAdjunta;
	private String rutaFoto;
	//private InputStream streamFile;	
	
	public PersonalPhoto() {
		this.fotoId = Constants.EMPTY_STRING;
		this.fotoAdjunta = null;
		this.rutaFoto = Constants.EMPTY_STRING;
		//this.streamFile = null;
	}

	public PersonalPhoto(String fotoId, byte[] fotoAdjunta, String rutaFoto) {
		this.fotoId = fotoId;
		this.fotoAdjunta = fotoAdjunta;
		this.rutaFoto = rutaFoto;
	}
	public void init() {
		this.fotoId = Constants.EMPTY_STRING;
		this.fotoAdjunta = null;
		this.rutaFoto = Constants.EMPTY_STRING;
	}	
	
	public String getFotoId() {
		return fotoId;
	}
	public void setFotoId(String fotoId) {
		this.fotoId = fotoId;
	}
	public byte[] getFotoAdjunta() {
		return fotoAdjunta;
	}
	public void setFotoAdjunta(byte[] fotoAdjunta) {
		this.fotoAdjunta = fotoAdjunta;
	}
	public String getRutaFoto() {
		return rutaFoto;
	}
	public void setRutaFoto(String rutaFoto) {
		this.rutaFoto = rutaFoto;
	}

	
}
