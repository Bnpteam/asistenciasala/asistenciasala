package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;
import java.util.Date;

public class HistorialVigencia implements Serializable {
	private static final long serialVersionUID = 1L;
	private Date fechaTramite;
	private String tipoRegistro;
	private String estado;
	private String tipoUsuario;
	private String vigencia;
	private Date fechaInicio;
	private Date fechaFin;
	private String usuarioRegistroAuditoria;
	private String fila;
	private String tipoUsuarioBiblioteca;
	private String codBarrasLector;
	private String tipoDocumentoIdentidad;
	private String numeroDocumentoIdentidad;
	private Date fechaNacimiento;
	private String sexo;
	private String gradoInstruccion;
	private String nacionalidad;
	private String pais;
	private String departamento;
	private String provincia;
	private String distrito;
	private String fechaTramiteString;
	private String fechaNacimientoString;
	private String fechaInicioString;
	private String fechaFinString;
	
	public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getUsuarioRegistroAuditoria() {
		return usuarioRegistroAuditoria;
	}
	public void setUsuarioRegistroAuditoria(String usuarioRegistroAuditoria) {
		this.usuarioRegistroAuditoria = usuarioRegistroAuditoria;
	}
	public String getFila() {
		return fila;
	}
	public void setFila(String fila) {
		this.fila = fila;
	}
	public String getTipoUsuarioBiblioteca() {
		return tipoUsuarioBiblioteca;
	}
	public void setTipoUsuarioBiblioteca(String tipoUsuarioBiblioteca) {
		this.tipoUsuarioBiblioteca = tipoUsuarioBiblioteca;
	}
	public String getCodBarrasLector() {
		return codBarrasLector;
	}
	public void setCodBarrasLector(String codBarrasLector) {
		this.codBarrasLector = codBarrasLector;
	}
	public String getTipoDocumentoIdentidad() {
		return tipoDocumentoIdentidad;
	}
	public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
		this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
	}
	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}
	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getGradoInstruccion() {
		return gradoInstruccion;
	}
	public void setGradoInstruccion(String gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public Date getFechaTramite() {
		return fechaTramite;
	}
	public void setFechaTramite(Date fechaTramite) {
		this.fechaTramite = fechaTramite;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getFechaTramiteString() {
		return fechaTramiteString;
	}
	public void setFechaTramiteString(String fechaTramiteString) {
		this.fechaTramiteString = fechaTramiteString;
	}
	public String getFechaNacimientoString() {
		return fechaNacimientoString;
	}
	public void setFechaNacimientoString(String fechaNacimientoString) {
		this.fechaNacimientoString = fechaNacimientoString;
	}
	public String getFechaInicioString() {
		return fechaInicioString;
	}
	public void setFechaInicioString(String fechaInicioString) {
		this.fechaInicioString = fechaInicioString;
	}
	public String getFechaFinString() {
		return fechaFinString;
	}
	public void setFechaFinString(String fechaFinString) {
		this.fechaFinString = fechaFinString;
	}
	
	
}
