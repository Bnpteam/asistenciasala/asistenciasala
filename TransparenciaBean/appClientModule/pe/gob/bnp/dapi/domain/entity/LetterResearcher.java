package pe.gob.bnp.dapi.domain.entity;

public class LetterResearcher {
	private String cartaSustentoId;
	private byte[] documentoAdjuntado;
	private String rutaDocumento;
	
	
	public String getRutaDocumento() {
		return rutaDocumento;
	}
	public void setRutaDocumento(String rutaDocumento) {
		this.rutaDocumento = rutaDocumento;
	}
	public String getCartaSustentoId() {
		return cartaSustentoId;
	}
	public void setCartaSustentoId(String cartaSustentoId) {
		this.cartaSustentoId = cartaSustentoId;
	}
	public byte[] getDocumentoAdjuntado() {
		return documentoAdjuntado;
	}
	public void setDocumentoAdjuntado(byte[] documentoAdjuntado) {
		this.documentoAdjuntado = documentoAdjuntado;
	}
	
	
}
