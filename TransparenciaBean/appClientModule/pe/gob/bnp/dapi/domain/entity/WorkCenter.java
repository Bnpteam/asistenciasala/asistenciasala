package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class WorkCenter implements Serializable {

	private static final long serialVersionUID = 4L;
	private Long centroLaboralId;
	private String centroLaboralNombre;
	private String tipo;
	private String telefono;
	private Address direccion;


	public WorkCenter() {
		this.centroLaboralId = Constants.EMPTY_LONG;
		this.centroLaboralNombre = Constants.EMPTY_STRING;
		this.tipo = Constants.EMPTY_STRING;
		this.direccion = new Address();
		this.telefono = Constants.EMPTY_STRING;
	}
	
	public WorkCenter(Long centroLaboralId, String centroLaboralNombre, String tipo, Address direccion, String telefono ) {
		this.centroLaboralId = centroLaboralId;
		this.centroLaboralNombre = centroLaboralNombre;
		this.tipo = tipo;
		this.direccion = direccion;
		this.telefono = telefono;
	}
	
	public void init() {
		this.centroLaboralId = Constants.EMPTY_LONG;
		this.centroLaboralNombre = Constants.EMPTY_STRING;
		this.tipo = Constants.EMPTY_STRING;
		this.telefono = Constants.EMPTY_STRING;
		this.direccion.init();
	}

	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Long getCentroLaboralId() {
		return centroLaboralId;
	}

	public void setCentroLaboralId(Long centroLaboralId) {
		this.centroLaboralId = centroLaboralId;
	}

	public String getCentroLaboralNombre() {
		return centroLaboralNombre;
	}

	public void setCentroLaboralNombre(String centroLaboralNombre) {
		this.centroLaboralNombre = centroLaboralNombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Address getDireccion() {
		return direccion;
	}

	public void setDireccion(Address direccion) {
		this.direccion = direccion;
	}

}
