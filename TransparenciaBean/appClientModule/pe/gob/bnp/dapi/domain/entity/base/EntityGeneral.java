package pe.gob.bnp.dapi.domain.entity.base;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Properties;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class EntityGeneral implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long usuarioIdRegistro;
	private Date fechaRegistro;
	private Long usuarioIdModificacion;
	private Date fechaModificacion;

	private Boolean resultadoTransaccion;
	private Exception excepcion;
	private Boolean existeExcepcion;
	private String mensajeRespuesta;


	
	public EntityGeneral() {
		this.usuarioIdRegistro = Constants.EMPTY_LONG;
		this.fechaRegistro = new Date();
		this.usuarioIdModificacion = Constants.EMPTY_LONG;
		this.fechaModificacion = new Date();
		this.resultadoTransaccion = false;
		this.excepcion = new Exception();
		this.existeExcepcion = false;
		this.mensajeRespuesta = Constants.EMPTY_STRING;
	}

	public void setStackException(Exception exception,Boolean swException) {
		this.setExcepcion(exception);
		this.setExisteExcepcion(swException);
	}
	
	public Boolean getExisteExcepcion() {
		return existeExcepcion;
	}

	public void setExisteExcepcion(Boolean existeExcepcion) {
		this.existeExcepcion = existeExcepcion;
	}

	public Boolean getResultadoTransaccion() {
		return resultadoTransaccion;
	}

	public void setResultadoTransaccion(Boolean resultadoTransaccion) {
		this.resultadoTransaccion = resultadoTransaccion;
	}

	public Exception getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(Exception excepcion) {
		this.excepcion = excepcion;
	}

	public Long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}

	public void setUsuarioIdRegistro(Long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}

	public void setUsuarioIdModificacion(Long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}

	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}

	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream(Constants.PROPERTIES_FILE));
			return properties.getProperty(key);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Constants.EMPTY_STRING;
	}	
	
}
