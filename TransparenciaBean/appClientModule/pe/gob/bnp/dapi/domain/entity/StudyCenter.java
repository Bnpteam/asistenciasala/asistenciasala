package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class StudyCenter implements Serializable {

	private static final long serialVersionUID = 5L;
	private Long centroEstudioId;
	private String centroEstudioNombre;
	private String facultadId;
	private String facultadNombre;
	private String especialidadId;
	private String especialidadNombre;
	private String telefono;
	private Address direccion;
	private String tipo;
	
	public StudyCenter() {
		this.centroEstudioId = Constants.EMPTY_LONG;
		this.centroEstudioNombre = Constants.EMPTY_STRING;
		this.facultadId = Constants.EMPTY_STRING;
		this.facultadNombre = Constants.EMPTY_STRING;
		this.especialidadId = Constants.EMPTY_STRING;
		this.especialidadNombre = Constants.EMPTY_STRING;
		this.telefono = Constants.EMPTY_STRING;
		this.direccion = new Address();
		this.tipo = Constants.EMPTY_STRING;
	}

	public void init() {
		this.centroEstudioId = Constants.EMPTY_LONG;
		this.centroEstudioNombre = Constants.EMPTY_STRING;
		this.facultadId = Constants.EMPTY_STRING;
		this.facultadNombre = Constants.EMPTY_STRING;
		this.especialidadId = Constants.EMPTY_STRING;
		this.especialidadNombre = Constants.EMPTY_STRING;
		this.telefono = Constants.EMPTY_STRING;
		this.tipo = Constants.EMPTY_STRING;
		this.direccion.init();
	}
	
	public StudyCenter(Long centroEstudioId, String centroEstudioNombre, String facultadId, String facultadNombre,
			String especialidadId, String especialidadNombre, String telefono, Address direccion, String tipo) {
		this.centroEstudioId = centroEstudioId;
		this.centroEstudioNombre = centroEstudioNombre;
		this.facultadId = facultadId;
		this.facultadNombre = facultadNombre;
		this.especialidadId = especialidadId;
		this.especialidadNombre = especialidadNombre;
		this.telefono = telefono;
		this.direccion = direccion;
		this.tipo = tipo;
	}

	public Long getCentroEstudioId() {
		return centroEstudioId;
	}

	public void setCentroEstudioId(Long centroEstudioId) {
		this.centroEstudioId = centroEstudioId;
	}

	public String getCentroEstudioNombre() {
		return centroEstudioNombre;
	}

	public void setCentroEstudioNombre(String centroEstudioNombre) {
		this.centroEstudioNombre = centroEstudioNombre;
	}

	public String getFacultadId() {
		return facultadId;
	}

	public void setFacultadId(String facultadId) {
		this.facultadId = facultadId;
	}

	public String getFacultadNombre() {
		return facultadNombre;
	}

	public void setFacultadNombre(String facultadNombre) {
		this.facultadNombre = facultadNombre;
	}

	public String getEspecialidadId() {
		return especialidadId;
	}

	public void setEspecialidadId(String especialidadId) {
		this.especialidadId = especialidadId;
	}

	public String getEspecialidadNombre() {
		return especialidadNombre;
	}

	public void setEspecialidadNombre(String especialidadNombre) {
		this.especialidadNombre = especialidadNombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Address getDireccion() {
		return direccion;
	}

	public void setDireccion(Address direccion) {
		this.direccion = direccion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
