package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.domain.entity.base.EntityGeneral;

public class Sala extends EntityGeneral implements Serializable{
	private static final long serialVersionUID = 1L;
	private int idSala;
	private String nombreSala;
	private String direccionIp;
	private String direccionMac;
	private Sede sede;
	
	public Sala() {
		this.sede = new Sede(); 
	}
	public Sede getSede() {
		return sede;
	}
	public void setSede(Sede sede) {
		this.sede = sede;
	}
	public int getIdSala() {
		return idSala;
	}
	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
	public String getNombreSala() {
		return nombreSala;
	}
	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}
	public String getDireccionIp() {
		return direccionIp;
	}
	public void setDireccionIp(String direccionIp) {
		this.direccionIp = direccionIp;
	}
	public String getDireccionMac() {
		return direccionMac;
	}
	public void setDireccionMac(String direccionMac) {
		this.direccionMac = direccionMac;
	}

	
}
