package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class Address implements Serializable {

	private static final long serialVersionUID = 142342L;
	private String codPais;
	private String nombrePais;
	private String codDep;
	private String nombreDep;
	private String codPro;
	private String nombrePro;
	private String codDis;
	private String nombreDis;
	private String nombreCiudad;
	private String direccion;

	public Address() {
		this.codPais = Constants.EMPTY_STRING;
		this.nombrePais = Constants.EMPTY_STRING;
		this.codDep = Constants.EMPTY_STRING;
		this.nombreDep = Constants.EMPTY_STRING;
		this.codPro = Constants.EMPTY_STRING;
		this.nombrePro = Constants.EMPTY_STRING;
		this.codDis = Constants.EMPTY_STRING;
		this.nombreDis = Constants.EMPTY_STRING;
		this.direccion = Constants.EMPTY_STRING;
		this.nombreCiudad = Constants.EMPTY_STRING;
	}
	
	public void init(){
		this.codPais = Constants.EMPTY_STRING;
		this.nombrePais = Constants.EMPTY_STRING;
		this.codDep = Constants.EMPTY_STRING;
		this.nombreDep = Constants.EMPTY_STRING;
		this.codPro = Constants.EMPTY_STRING;
		this.nombrePro = Constants.EMPTY_STRING;
		this.codDis = Constants.EMPTY_STRING;
		this.nombreDis = Constants.EMPTY_STRING;
		this.direccion = Constants.EMPTY_STRING;
		this.nombreCiudad = Constants.EMPTY_STRING;
	}
	
	public String getNombreCiudad() {
		return nombreCiudad;
	}


	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}


	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getNombrePais() {
		return nombrePais;
	}

	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}

	public String getCodDep() {
		return codDep;
	}

	public void setCodDep(String codDep) {
		this.codDep = codDep;
	}

	public String getNombreDep() {
		return nombreDep;
	}

	public void setNombreDep(String nombreDep) {
		this.nombreDep = nombreDep;
	}

	public String getCodPro() {
		return codPro;
	}

	public void setCodPro(String codPro) {
		this.codPro = codPro;
	}

	public String getNombrePro() {
		return nombrePro;
	}

	public void setNombrePro(String nombrePro) {
		this.nombrePro = nombrePro;
	}

	public String getCodDis() {
		return codDis;
	}

	public void setCodDis(String codDis) {
		this.codDis = codDis;
	}

	public String getNombreDis() {
		return nombreDis;
	}

	public void setNombreDis(String nombreDis) {
		this.nombreDis = nombreDis;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

}
