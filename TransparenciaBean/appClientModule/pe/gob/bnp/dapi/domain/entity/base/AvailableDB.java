package pe.gob.bnp.dapi.domain.entity.base;

public class AvailableDB {
	private String estado;

	public AvailableDB() {
		this.estado = "OFFLINE";
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
