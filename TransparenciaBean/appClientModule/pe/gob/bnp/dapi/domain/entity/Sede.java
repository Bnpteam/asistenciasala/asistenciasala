package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.domain.entity.base.EntityGeneral;

public class Sede extends EntityGeneral implements Serializable {
	private static final long serialVersionUID = 2L;
	private String idSede;
	private String nombreSede;
	
	public String getIdSede() {
		return idSede;
	}
	public void setIdSede(String idSede) {
		this.idSede = idSede;
	}
	public String getNombreSede() {
		return nombreSede;
	}
	public void setNombreSede(String nombreSede) {
		this.nombreSede = nombreSede;
	}
	
}
