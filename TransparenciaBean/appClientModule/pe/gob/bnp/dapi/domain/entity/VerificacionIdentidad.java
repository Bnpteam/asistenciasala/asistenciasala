package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.domain.entity.base.EntityGeneral;

public class VerificacionIdentidad extends EntityGeneral implements Serializable{
	private static final long serialVersionUID = 1L;
	private String ipLector;
	private String entidadConsultada;
	private String urlConsultada;
	private String numDocConsultado;
	private String tipoDocConsultado;
	private String apellidoConsultado;
	private String origenConsulta;
	private String credencialUsuario;
	
	public VerificacionIdentidad() {
		this.ipLector 			= Constants.EMPTY_STRING;
		this.entidadConsultada 	= Constants.EMPTY_STRING;
		this.urlConsultada 		= Constants.EMPTY_STRING;
		this.numDocConsultado 	= Constants.EMPTY_STRING;
		this.tipoDocConsultado 	= Constants.EMPTY_STRING;
		this.apellidoConsultado = Constants.EMPTY_STRING;
		this.origenConsulta 	= Constants.EMPTY_STRING;
		this.credencialUsuario 	= Constants.EMPTY_STRING;
	}

	
	public VerificacionIdentidad(String ipLector, String entidadConsultada,
			String urlConsultada, String numDocConsultado,
			String tipoDocConsultado, String apellidoConsultado,
			String origenConsulta, String credencialUsuario) {
		this.ipLector = ipLector;
		this.entidadConsultada = entidadConsultada;
		this.urlConsultada = urlConsultada;
		this.numDocConsultado = numDocConsultado;
		this.tipoDocConsultado = tipoDocConsultado;
		this.apellidoConsultado = apellidoConsultado;
		this.origenConsulta = origenConsulta;
		this.credencialUsuario 	= credencialUsuario;
	}


	public String getIpLector() {
		return ipLector;
	}

	public void setIpLector(String ipLector) {
		this.ipLector = ipLector;
	}

	public String getEntidadConsultada() {
		return entidadConsultada;
	}

	public void setEntidadConsultada(String entidadConsultada) {
		this.entidadConsultada = entidadConsultada;
	}

	public String getUrlConsultada() {
		return urlConsultada;
	}

	public void setUrlConsultada(String urlConsultada) {
		this.urlConsultada = urlConsultada;
	}

	public String getNumDocConsultado() {
		return numDocConsultado;
	}

	public void setNumDocConsultado(String numDocConsultado) {
		this.numDocConsultado = numDocConsultado;
	}

	public String getTipoDocConsultado() {
		return tipoDocConsultado;
	}

	public void setTipoDocConsultado(String tipoDocConsultado) {
		this.tipoDocConsultado = tipoDocConsultado;
	}

	public String getApellidoConsultado() {
		return apellidoConsultado;
	}

	public void setApellidoConsultado(String apellidoConsultado) {
		this.apellidoConsultado = apellidoConsultado;
	}

	public String getOrigenConsulta() {
		return origenConsulta;
	}

	public void setOrigenConsulta(String origenConsulta) {
		this.origenConsulta = origenConsulta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getCredencialUsuario() {
		return credencialUsuario;
	}


	public void setCredencialUsuario(String credencialUsuario) {
		this.credencialUsuario = credencialUsuario;
	}

	
}
