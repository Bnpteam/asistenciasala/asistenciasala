package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class CredencialUsuarioDto implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long idUsuario;
	public String usuarioRed;
	public String ipMaquina;
	public Long idSala;
	
	public CredencialUsuarioDto() {
		this.idUsuario = Constants.EMPTY_LONG;
		this.usuarioRed = Constants.EMPTY_STRING;
		this.ipMaquina = Constants.EMPTY_STRING;
		this.idSala = Constants.EMPTY_LONG;
	}
	
	

	public CredencialUsuarioDto(Long idUsuario, String usuarioRed,
			String ipMaquina, Long idSala) {
		this.idUsuario = idUsuario;
		this.usuarioRed = usuarioRed;
		this.ipMaquina = ipMaquina;
		this.idSala = idSala;
	}



	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuarioRed() {
		return usuarioRed;
	}

	public void setUsuarioRed(String usuarioRed) {
		this.usuarioRed = usuarioRed;
	}

	public String getIpMaquina() {
		return ipMaquina;
	}

	public void setIpMaquina(String ipMaquina) {
		this.ipMaquina = ipMaquina;
	}

	public Long getIdSala() {
		return idSala;
	}

	public void setIdSala(Long idSala) {
		this.idSala = idSala;
	}
	
	
	
}