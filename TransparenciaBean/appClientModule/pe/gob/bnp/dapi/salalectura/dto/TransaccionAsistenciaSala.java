package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class TransaccionAsistenciaSala implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tipoTransaccion;
	private String codigoResultadoTransaccion;
	private String descripcionResultadoTransaccion;
	private List<UsuarioEnSala> listaUsuariosEnSala;
	
	public TransaccionAsistenciaSala() {
		this.tipoTransaccion 					=	Constants.EMPTY_STRING;
		this.codigoResultadoTransaccion 		=	Constants.EMPTY_STRING;
		this.descripcionResultadoTransaccion 	=	Constants.EMPTY_STRING;
		this.listaUsuariosEnSala 				= 	new ArrayList<>();
	}
	
	public TransaccionAsistenciaSala(String tipoTransaccion) {
		this.tipoTransaccion 					=	tipoTransaccion;
		this.codigoResultadoTransaccion 		=	Constants.EMPTY_STRING;
		this.descripcionResultadoTransaccion 	=	Constants.EMPTY_STRING;
		this.listaUsuariosEnSala 				= 	new ArrayList<>();
	}	
	
	public String getTipoTransaccion() {
		return tipoTransaccion;
	}

	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public String getCodigoResultadoTransaccion() {
		return codigoResultadoTransaccion;
	}

	public void setCodigoResultadoTransaccion(String codigoResultadoTransaccion) {
		this.codigoResultadoTransaccion = codigoResultadoTransaccion;
	}

	public String getDescripcionResultadoTransaccion() {
		if (this.codigoResultadoTransaccion.equals("0000")){
			if (this.tipoTransaccion.equals("Ingreso")){
				this.descripcionResultadoTransaccion = "EXITO: Se registr� el ingreso del usuario de forma exitosa.";
			}
			if (this.tipoTransaccion.equals("Salida")){
				this.descripcionResultadoTransaccion = "EXITO: Se registr� el salida del usuario de forma exitosa.";
			}
		}
		if (this.codigoResultadoTransaccion.equals("0008")){
			this.descripcionResultadoTransaccion = "ERROR: La sala solo permite ingreso de usuarios investigadores.";
		}
		if (this.codigoResultadoTransaccion.equals("0007")){
			String detalleOtroIngreso = this.descripcionResultadoTransaccion;
			this.descripcionResultadoTransaccion = "ERROR: El usuario (su n�mero de documento de identidad) ya tiene registrado un ingreso a la Sala "+detalleOtroIngreso;
		}
		if (this.codigoResultadoTransaccion.equals("0006")){
			this.descripcionResultadoTransaccion = "ERROR: No se permite el ingreso fuera del horario definido para la sala.";
		}
		if (this.codigoResultadoTransaccion.equals("0005")){
			this.descripcionResultadoTransaccion = "ERROR: La sala no esta habilitada para ingreso hoy.";
		}
		if (this.codigoResultadoTransaccion.equals("0004")){
			this.descripcionResultadoTransaccion = "ERROR: La sala no tiene horarios habilitados.";
		}
		if (this.codigoResultadoTransaccion.equals("0003")){
			String detalleOtroIngreso = this.descripcionResultadoTransaccion;
			this.descripcionResultadoTransaccion = "ERROR: El usuario ya tiene registrado un ingreso a la Sala "+detalleOtroIngreso;
		}
		if (this.codigoResultadoTransaccion.equals("0002")){
			this.descripcionResultadoTransaccion = "ERROR: No se encontro el id del usuario en SIRU.";
		}
		if (this.codigoResultadoTransaccion.equals("0001")){
			this.descripcionResultadoTransaccion = "ERROR: No se encontr� el codigo del lector del usuario en SIRU.";
		}	
		if (this.codigoResultadoTransaccion.equals("S001")){
			this.descripcionResultadoTransaccion = "ERROR: No se puede registrar la salida fuera del horario de atenci�n de la sala.";
		}			
		if (this.codigoResultadoTransaccion.equals("S002")){
			this.descripcionResultadoTransaccion = "ERROR: Salida ya fue registrada anteriormente.";
		}			
		return descripcionResultadoTransaccion;
	}

	public void setDescripcionResultadoTransaccion(
			String descripcionResultadoTransaccion) {
		this.descripcionResultadoTransaccion = descripcionResultadoTransaccion;
	}

	public List<UsuarioEnSala> getListaUsuariosEnSala() {
		return listaUsuariosEnSala;
	}

	public void setListaUsuariosEnSala(List<UsuarioEnSala> listaUsuariosEnSala) {
		this.listaUsuariosEnSala = listaUsuariosEnSala;
	}
	
	

}
