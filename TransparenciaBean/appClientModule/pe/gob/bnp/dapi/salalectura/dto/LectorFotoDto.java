package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class LectorFotoDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String		fileNameReniec;
	private String		fileExtensionReniec;
	private byte[]		rawFileReniec;
	private String		fileNameAdjuntada;
	private String		fileExtensionAdjuntada;
	private byte[]		rawFileAdjuntada;
	
	
	
	public LectorFotoDto() {
		this.fileNameReniec 	= Constants.EMPTY_STRING;
		this.fileExtensionReniec 	= Constants.EMPTY_STRING;
		this.rawFileReniec 	= null;
		this.fileNameAdjuntada 	= Constants.EMPTY_STRING;
		this.fileExtensionAdjuntada 	= Constants.EMPTY_STRING;
		this.rawFileAdjuntada 	= null;
	}



	public String getFileNameReniec() {
		return fileNameReniec;
	}



	public void setFileNameReniec(String fileNameReniec) {
		this.fileNameReniec = fileNameReniec;
	}



	public String getFileExtensionReniec() {
		return fileExtensionReniec;
	}



	public void setFileExtensionReniec(String fileExtensionReniec) {
		this.fileExtensionReniec = fileExtensionReniec;
	}



	public byte[] getRawFileReniec() {
		return rawFileReniec;
	}



	public void setRawFileReniec(byte[] rawFileReniec) {
		this.rawFileReniec = rawFileReniec;
	}



	public String getFileNameAdjuntada() {
		return fileNameAdjuntada;
	}



	public void setFileNameAdjuntada(String fileNameAdjuntada) {
		this.fileNameAdjuntada = fileNameAdjuntada;
	}



	public String getFileExtensionAdjuntada() {
		return fileExtensionAdjuntada;
	}



	public void setFileExtensionAdjuntada(String fileExtensionAdjuntada) {
		this.fileExtensionAdjuntada = fileExtensionAdjuntada;
	}



	public byte[] getRawFileAdjuntada() {
		return rawFileAdjuntada;
	}



	public void setRawFileAdjuntada(byte[] rawFileAdjuntada) {
		this.rawFileAdjuntada = rawFileAdjuntada;
	}

	
	
}
