package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class ReferencistasProgramadosPorSalaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String piso;
	private String sala;
	private String userName;
	private String referencista;
	private String estado;
	
	public ReferencistasProgramadosPorSalaDto() {
		this.piso = Constants.EMPTY_STRING;
		this.sala = Constants.EMPTY_STRING;
		this.userName = Constants.EMPTY_STRING;
		this.referencista = Constants.EMPTY_STRING;
		this.estado = Constants.EMPTY_STRING;
	}
	public String getPiso() {
		return piso;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getReferencista() {
		return referencista;
	}
	public void setReferencista(String referencista) {
		this.referencista = referencista;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	

}
