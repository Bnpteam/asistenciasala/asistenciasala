package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class MonitoreoEnSala implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<ResumenUsuariosActivoPorSalaDto> listaResumenUsuariosActivoPorSala;
	private List<ResumenUsuariosPorSalaMesActualDto> listaResumenUsuariosPorSalaMesActual;
	private List<ReferencistasLogueadosPorSalaDto> listaReferencistasLogueadosPorSala;
	private List<ReferencistasProgramadosPorSalaDto> listaReferencistasProgramadosPorSala;
	private String fechaActualizacion;
	
	public MonitoreoEnSala() {
		this.listaResumenUsuariosActivoPorSala = new ArrayList<>();
		this.listaResumenUsuariosPorSalaMesActual = new ArrayList<>();
		this.listaReferencistasLogueadosPorSala = new ArrayList<>();
		this.listaReferencistasProgramadosPorSala = new ArrayList<>();
		this.fechaActualizacion = Constants.EMPTY_STRING;
	}

	public List<ResumenUsuariosActivoPorSalaDto> getListaResumenUsuariosActivoPorSala() {
		return listaResumenUsuariosActivoPorSala;
	}

	public void setListaResumenUsuariosActivoPorSala(
			List<ResumenUsuariosActivoPorSalaDto> listaResumenUsuariosActivoPorSala) {
		this.listaResumenUsuariosActivoPorSala = listaResumenUsuariosActivoPorSala;
	}

	public List<ResumenUsuariosPorSalaMesActualDto> getListaResumenUsuariosPorSalaMesActual() {
		return listaResumenUsuariosPorSalaMesActual;
	}

	public void setListaResumenUsuariosPorSalaMesActual(
			List<ResumenUsuariosPorSalaMesActualDto> listaResumenUsuariosPorSalaMesActual) {
		this.listaResumenUsuariosPorSalaMesActual = listaResumenUsuariosPorSalaMesActual;
	}

	public List<ReferencistasLogueadosPorSalaDto> getListaReferencistasLogueadosPorSala() {
		return listaReferencistasLogueadosPorSala;
	}

	public void setListaReferencistasLogueadosPorSala(
			List<ReferencistasLogueadosPorSalaDto> listaReferencistasLogueadosPorSala) {
		this.listaReferencistasLogueadosPorSala = listaReferencistasLogueadosPorSala;
	}

	public List<ReferencistasProgramadosPorSalaDto> getListaReferencistasProgramadosPorSala() {
		return listaReferencistasProgramadosPorSala;
	}

	public void setListaReferencistasProgramadosPorSala(
			List<ReferencistasProgramadosPorSalaDto> listaReferencistasProgramadosPorSala) {
		this.listaReferencistasProgramadosPorSala = listaReferencistasProgramadosPorSala;
	}

	public String getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

}
