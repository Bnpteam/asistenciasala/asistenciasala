package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class ResumenUsuariosActivoPorSalaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String piso;
	private String sala;
	private String usuariosEnSala;
	private String usuariosTotal;
	
	
	
	public ResumenUsuariosActivoPorSalaDto() {
		this.piso 		= Constants.EMPTY_STRING;
		this.sala 		= Constants.EMPTY_STRING;
		this.usuariosEnSala = Constants.EMPTY_STRING;
		this.usuariosTotal 	= Constants.EMPTY_STRING;
	}
	
	public String getPiso() {
		return piso;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	public String getUsuariosEnSala() {
		return usuariosEnSala;
	}
	public void setUsuariosEnSala(String usuariosEnSala) {
		this.usuariosEnSala = usuariosEnSala;
	}
	public String getUsuariosTotal() {
		return usuariosTotal;
	}
	public void setUsuariosTotal(String usuariosTotal) {
		this.usuariosTotal = usuariosTotal;
	}
	
	
}
