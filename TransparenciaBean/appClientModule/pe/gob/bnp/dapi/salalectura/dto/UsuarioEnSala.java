package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class UsuarioEnSala implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long idRegistroAsistencia;
	private Long idRegistroUsuario;
	private String nombreUsuario;
	private String tipoDocIdentidad;
	private String numeroDocIdentidad;
	private String tipoUsuario;
	private String fechaIngreso;
	private String horaIngreso;
	private Date fechaHoraIngreso;
	private String estadoAsistencia;
	private String userNameReferencistaIngreso;
	
	private Long idSesionReferencistaRegistraSalida;
	private Long idSalidaMotivo;
	private String descripcionSalidaMotivo;
	private String salidaJustificación;
	private Long	codBarrasLector;
	private String apellidosUsuario;
	
	public UsuarioEnSala() {
		this.idRegistroAsistencia 	= Constants.EMPTY_LONG;
		this.idRegistroUsuario 		= Constants.EMPTY_LONG;
		this.nombreUsuario 			= Constants.EMPTY_STRING;
		this.tipoDocIdentidad 		= Constants.EMPTY_STRING;
		this.numeroDocIdentidad 	= Constants.EMPTY_STRING;
		this.tipoUsuario 			= Constants.EMPTY_STRING;
		this.fechaIngreso 			= Constants.EMPTY_STRING;
		this.horaIngreso 			= Constants.EMPTY_STRING;
		this.fechaHoraIngreso 		= null;
		this.estadoAsistencia 		= Constants.EMPTY_STRING;
		this.userNameReferencistaIngreso 			= Constants.EMPTY_STRING;
		this.idSesionReferencistaRegistraSalida 	= Constants.EMPTY_LONG;
		this.idSalidaMotivo							= Constants.EMPTY_LONG;
		this.descripcionSalidaMotivo				= Constants.EMPTY_STRING;
		this.salidaJustificación					= Constants.EMPTY_STRING;
		this.codBarrasLector		= Constants.EMPTY_LONG;
		this.apellidosUsuario 		= Constants.EMPTY_STRING;
	}

	public Long getIdRegistroAsistencia() {
		return idRegistroAsistencia;
	}

	public void setIdRegistroAsistencia(Long idRegistroAsistencia) {
		this.idRegistroAsistencia = idRegistroAsistencia;
	}

	public Long getIdRegistroUsuario() {
		return idRegistroUsuario;
	}

	public void setIdRegistroUsuario(Long idRegistroUsuario) {
		this.idRegistroUsuario = idRegistroUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getTipoDocIdentidad() {
		return tipoDocIdentidad;
	}

	public void setTipoDocIdentidad(String tipoDocIdentidad) {
		this.tipoDocIdentidad = tipoDocIdentidad;
	}

	public String getNumeroDocIdentidad() {
		return numeroDocIdentidad;
	}

	public void setNumeroDocIdentidad(String numeroDocIdentidad) {
		this.numeroDocIdentidad = numeroDocIdentidad;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getHoraIngreso() {
		return horaIngreso;
	}

	public void setHoraIngreso(String horaIngreso) {
		this.horaIngreso = horaIngreso;
	}

	public Date getFechaHoraIngreso() {
		return fechaHoraIngreso;
	}

	public void setFechaHoraIngreso(Date fechaHoraIngreso) {
		this.fechaHoraIngreso = fechaHoraIngreso;
	}

	public String getEstadoAsistencia() {
		return estadoAsistencia;
	}

	public void setEstadoAsistencia(String estadoAsistencia) {
		this.estadoAsistencia = estadoAsistencia;
	}

	public String getUserNameReferencistaIngreso() {
		return userNameReferencistaIngreso;
	}

	public void setUserNameReferencistaIngreso(String userNameReferencistaIngreso) {
		this.userNameReferencistaIngreso = userNameReferencistaIngreso;
	}

	public Long getIdSesionReferencistaRegistraSalida() {
		return idSesionReferencistaRegistraSalida;
	}

	public void setIdSesionReferencistaRegistraSalida(
			Long idSesionReferencistaRegistraSalida) {
		this.idSesionReferencistaRegistraSalida = idSesionReferencistaRegistraSalida;
	}

	public Long getIdSalidaMotivo() {
		return idSalidaMotivo;
	}

	public void setIdSalidaMotivo(Long idSalidaMotivo) {
		this.idSalidaMotivo = idSalidaMotivo;
	}

	public String getDescripcionSalidaMotivo() {
		return descripcionSalidaMotivo;
	}

	public void setDescripcionSalidaMotivo(String descripcionSalidaMotivo) {
		this.descripcionSalidaMotivo = descripcionSalidaMotivo;
	}

	public String getSalidaJustificación() {
		return salidaJustificación;
	}

	public void setSalidaJustificación(String salidaJustificación) {
		this.salidaJustificación = salidaJustificación;
	}

	public Long getCodBarrasLector() {
		return codBarrasLector;
	}

	public void setCodBarrasLector(Long codBarrasLector) {
		this.codBarrasLector = codBarrasLector;
	}

	public String getApellidosUsuario() {
		return apellidosUsuario;
	}

	public void setApellidosUsuario(String apellidosUsuario) {
		this.apellidosUsuario = apellidosUsuario;
	}
	
	
	
}
