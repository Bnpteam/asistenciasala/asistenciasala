package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class LectorAbsysnetDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tipoUsuarioBiblioteca;
	private String tipoDocumentoIdentidad; 
	private String numeroDocumentoIdentidad;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private Date fechaNacimiento;
	private String sexo;
	private String direccionDomicilio;
	private String codPais;
	private String telefonoFijo;
	private String telefonoCelular;
	private String correoElectronico;
	private String centroDeEstudios;
	private String gradoInstruccion;
	private String direccionCentroDeEstudio;
	private String temaInvestigacion;
	private String estado;
	private Date   fechaInicioVigencia;
	private Date   fechaRenovacion;
	private Date   fechaFinVigencia;
	private String codBarrasLector;
	private Long   idUsuarioRegistro;
	private String flagVigencia;
	private String codResultadoAbsysnet;
	private String codResultadoSIRU;
	
	public LectorAbsysnetDto() {
		this.tipoUsuarioBiblioteca 			= Constants.EMPTY_STRING;
		this.tipoDocumentoIdentidad 		= Constants.EMPTY_STRING;
		this.numeroDocumentoIdentidad 		= Constants.EMPTY_STRING;
		this.nombres 						= Constants.EMPTY_STRING;
		this.apellidoPaterno 				= Constants.EMPTY_STRING;
		this.apellidoMaterno 				= Constants.EMPTY_STRING;
		this.fechaNacimiento 				= null;
		this.sexo 							= Constants.EMPTY_STRING;
		this.direccionDomicilio 			= Constants.EMPTY_STRING;
		this.codPais 						= Constants.EMPTY_STRING;
		this.telefonoFijo 					= Constants.EMPTY_STRING;
		this.telefonoCelular 				= Constants.EMPTY_STRING;
		this.correoElectronico 				= Constants.EMPTY_STRING;
		this.centroDeEstudios 				= Constants.EMPTY_STRING;
		this.gradoInstruccion 				= Constants.EMPTY_STRING;
		this.direccionCentroDeEstudio 		= Constants.EMPTY_STRING;
		this.temaInvestigacion 				= Constants.EMPTY_STRING;
		this.estado 						= Constants.EMPTY_STRING;
		this.fechaInicioVigencia 			= null;
		this.fechaRenovacion 				= null;
		this.fechaFinVigencia 				= null;
		this.idUsuarioRegistro 				= Constants.EMPTY_LONG;
		this.codResultadoAbsysnet			= Constants.EMPTY_STRING;
		this.codResultadoSIRU				= Constants.EMPTY_STRING;
		this.flagVigencia 					= Constants.EMPTY_STRING;
	}

	public String getTipoUsuarioBiblioteca() {
		return tipoUsuarioBiblioteca;
	}

	public void setTipoUsuarioBiblioteca(String tipoUsuarioBiblioteca) {
		this.tipoUsuarioBiblioteca = tipoUsuarioBiblioteca;
	}

	public String getTipoDocumentoIdentidad() {
		return tipoDocumentoIdentidad;
	}

	public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
		this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
	}

	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}

	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getDireccionDomicilio() {
		return direccionDomicilio;
	}

	public void setDireccionDomicilio(String direccionDomicilio) {
		this.direccionDomicilio = direccionDomicilio;
	}

	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getCentroDeEstudios() {
		return centroDeEstudios;
	}

	public void setCentroDeEstudios(String centroDeEstudios) {
		this.centroDeEstudios = centroDeEstudios;
	}

	public String getGradoInstruccion() {
		return gradoInstruccion;
	}

	public void setGradoInstruccion(String gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}

	public String getDireccionCentroDeEstudio() {
		return direccionCentroDeEstudio;
	}

	public void setDireccionCentroDeEstudio(String direccionCentroDeEstudio) {
		this.direccionCentroDeEstudio = direccionCentroDeEstudio;
	}

	public String getTemaInvestigacion() {
		return temaInvestigacion;
	}

	public void setTemaInvestigacion(String temaInvestigacion) {
		this.temaInvestigacion = temaInvestigacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaRenovacion() {
		return fechaRenovacion;
	}

	public void setFechaRenovacion(Date fechaRenovacion) {
		this.fechaRenovacion = fechaRenovacion;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getCodBarrasLector() {
		return codBarrasLector;
	}

	public void setCodBarrasLector(String codBarrasLector) {
		this.codBarrasLector = codBarrasLector;
	}

	public Long getIdUsuarioRegistro() {
		return idUsuarioRegistro;
	}

	public void setIdUsuarioRegistro(Long idUsuarioRegistro) {
		this.idUsuarioRegistro = idUsuarioRegistro;
	}

	public String getCodResultadoAbsysnet() {
		return codResultadoAbsysnet;
	}

	public void setCodResultadoAbsysnet(String codResultadoAbsysnet) {
		this.codResultadoAbsysnet = codResultadoAbsysnet;
	}

	public String getCodResultadoSIRU() {
		return codResultadoSIRU;
	}

	public void setCodResultadoSIRU(String codResultadoSIRU) {
		this.codResultadoSIRU = codResultadoSIRU;
	}

	public String getFlagVigencia() {
		return flagVigencia;
	}

	public void setFlagVigencia(String flagVigencia) {
		this.flagVigencia = flagVigencia;
	}
	
	
	
}
