package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class ReferencistasLogueadosPorSalaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String userName;
	private String referencista;
	private String sala;
	private String piso;
	private String hora;
	private String usuariosRegistrados;
	
	public ReferencistasLogueadosPorSalaDto() {
		this.userName = Constants.EMPTY_STRING;
		this.referencista = Constants.EMPTY_STRING;
		this.sala = Constants.EMPTY_STRING;
		this.piso = Constants.EMPTY_STRING;
		this.hora = Constants.EMPTY_STRING;
		this.usuariosRegistrados = Constants.EMPTY_STRING;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReferencista() {
		return referencista;
	}

	public void setReferencista(String referencista) {
		this.referencista = referencista;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getUsuariosRegistrados() {
		return usuariosRegistrados;
	}

	public void setUsuariosRegistrados(String usuariosRegistrados) {
		this.usuariosRegistrados = usuariosRegistrados;
	}
	
	

}
