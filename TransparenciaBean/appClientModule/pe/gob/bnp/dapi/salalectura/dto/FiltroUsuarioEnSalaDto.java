package pe.gob.bnp.dapi.salalectura.dto;
import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;


public class FiltroUsuarioEnSalaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long idSala;
	private String estadoAsistencia;
	
	public FiltroUsuarioEnSalaDto() {
		this.idSala 			= Constants.EMPTY_LONG;
		this.estadoAsistencia 	= Constants.EMPTY_STRING;
	}

	public Long getIdSala() {
		return idSala;
	}

	public void setIdSala(Long idSala) {
		this.idSala = idSala;
	}

	public String getEstadoAsistencia() {
		return estadoAsistencia;
	}

	public void setEstadoAsistencia(String estadoAsistencia) {
		this.estadoAsistencia = estadoAsistencia;
	}
	
	
	
	

}
