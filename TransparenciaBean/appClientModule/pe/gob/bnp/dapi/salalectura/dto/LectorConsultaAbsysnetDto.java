package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class LectorConsultaAbsysnetDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long codBarrasLector;
	private String tipoUsuario;
	private String fechaCreacion;
	private String fechaInicio;
	private String fechaFin;
	private String vigencia;
	private String estado;
	private String nombres;
	private String apellidos;
	private String tipoDocIdentidad;
	private String numeroDocIdentidad;
	private String correoElectronico;
	private int lencar;
	private Long idSesionUsuario;
	
	
	
	public LectorConsultaAbsysnetDto() {
		this.codBarrasLector 	= 	Constants.EMPTY_LONG;
		this.tipoUsuario 		= 	Constants.EMPTY_STRING;
		this.fechaCreacion 		= 	Constants.EMPTY_STRING;
		this.fechaInicio 		= 	Constants.EMPTY_STRING;
		this.fechaFin 			= 	Constants.EMPTY_STRING;
		this.vigencia 			= 	Constants.EMPTY_STRING;
		this.estado 			= 	Constants.EMPTY_STRING;
		this.nombres 			= 	Constants.EMPTY_STRING;
		this.apellidos 			= 	Constants.EMPTY_STRING;
		this.tipoDocIdentidad 	= 	Constants.EMPTY_STRING;
		this.numeroDocIdentidad = 	Constants.EMPTY_STRING;
		this.correoElectronico 	= 	Constants.EMPTY_STRING;
		this.lencar 			= 	Constants.EMPTY_INTEGER;
		this.idSesionUsuario	=   Constants.EMPTY_LONG;
	}
	
	public boolean getFlagSiruBoolean(){
		boolean resultado = false;
		if (getFlagSiru().equals("SI")){
			resultado = true;
		}else{
			resultado = false;
		}
		return resultado;
	}
	
	public String getFlagSiru() {
		String resultado = "";
		if (this.lencar==1){
			resultado="SI";
		}else{
			resultado="NO";
		}
		return resultado;
	}
	
	public boolean mostrarBotonIngresoSala(){
		boolean resultado = false;
		if (this.estado.equals(Constants.REGISTER_USER_STATE_ENABLED) && this.vigencia.equals("Vigente")){
			resultado=true;
		}
		return resultado;
	}
	
	public String getColorRegistroLector(){
		String resultado = "black";
		if (!this.estado.equals(Constants.REGISTER_USER_STATE_ENABLED) || !this.vigencia.equals("Vigente")){
			resultado="red";
		}
		return resultado;
	}

	public String getFlagSuspendido() {
		String resultado = "";
		if (this.estado.equals(Constants.REGISTER_USER_STATE_SUSPENDED)){
			resultado="SI";
		}else{
			resultado="NO";
		}
		return resultado;
	}
	
	public String getFlagHabilitado() {
		String resultado = "";
		if (this.estado.equals(Constants.REGISTER_USER_STATE_ENABLED)){
			resultado="SI";
		}else{
			resultado="NO";
		}
		return resultado;
	}	
	
	public String getColorEstadoSuspendido() {
		String resultado = "";
		if (this.getFlagSuspendido().equals("SI")){
			resultado="red";
		}else{
			resultado="black";
		}
		return resultado;
	}	

	public String getColorEstadoVigencia() {
		String resultado = "";
		if (this.vigencia.equals("Vigente")){
			resultado="green";
		}else{
			resultado="red";
		}
		return resultado;
	}	
	
	public Long getCodBarrasLector() {
		return codBarrasLector;
	}
	public void setCodBarrasLector(Long codBarrasLector) {
		this.codBarrasLector = codBarrasLector;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getTipoDocIdentidad() {
		return tipoDocIdentidad;
	}
	public void setTipoDocIdentidad(String tipoDocIdentidad) {
		this.tipoDocIdentidad = tipoDocIdentidad;
	}
	public String getNumeroDocIdentidad() {
		return numeroDocIdentidad;
	}
	public void setNumeroDocIdentidad(String numeroDocIdentidad) {
		this.numeroDocIdentidad = numeroDocIdentidad;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public int getLencar() {
		return lencar;
	}

	public void setLencar(int lencar) {
		this.lencar = lencar;
	}

	public Long getIdSesionUsuario() {
		return idSesionUsuario;
	}

	public void setIdSesionUsuario(Long idSesionUsuario) {
		this.idSesionUsuario = idSesionUsuario;
	}

	
	
}
