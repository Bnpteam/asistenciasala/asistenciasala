package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class ResumenUsuariosPorSalaMesActualDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String piso;
	private String sala;
	private String usuariosTotalMes;
	private String usuariosPromedioMes;
	
	public ResumenUsuariosPorSalaMesActualDto() {
		this.piso = Constants.EMPTY_STRING;
		this.sala = Constants.EMPTY_STRING;
		this.usuariosTotalMes = Constants.EMPTY_STRING;
		this.usuariosPromedioMes = Constants.EMPTY_STRING;
	}
	
	public String getPiso() {
		return piso;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	public String getUsuariosTotalMes() {
		return usuariosTotalMes;
	}
	public void setUsuariosTotalMes(String usuariosTotalMes) {
		this.usuariosTotalMes = usuariosTotalMes;
	}
	public String getUsuariosPromedioMes() {
		return usuariosPromedioMes;
	}
	public void setUsuariosPromedioMes(String usuariosPromedioMes) {
		this.usuariosPromedioMes = usuariosPromedioMes;
	}	

	
}
