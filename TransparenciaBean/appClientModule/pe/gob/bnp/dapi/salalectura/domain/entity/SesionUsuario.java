package pe.gob.bnp.dapi.salalectura.domain.entity;

import java.io.Serializable;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class SesionUsuario implements Serializable {
	private static final long serialVersionUID = 1L;
	private Date fechaLogin;
	private Long idSesionUsuario;
	private String fechaLoginTexto;
	private String ipMaquina;
	
	public SesionUsuario() {
		this.fechaLogin = null;
		this.idSesionUsuario = Constants.EMPTY_LONG;
		this.fechaLoginTexto = Constants.EMPTY_STRING;
		this.ipMaquina = Constants.EMPTY_STRING;
	}

	public Date getFechaLogin() {
		return fechaLogin;
	}

	public void setFechaLogin(Date fechaLogin) {
		this.fechaLogin = fechaLogin;
	}

	public Long getIdSesionUsuario() {
		return idSesionUsuario;
	}

	public void setIdSesionUsuario(Long idSesionUsuario) {
		this.idSesionUsuario = idSesionUsuario;
	}

	public String getFechaLoginTexto() {
		return fechaLoginTexto;
	}

	public void setFechaLoginTexto(String fechaLoginTexto) {
		this.fechaLoginTexto = fechaLoginTexto;
	}

	public String getIpMaquina() {
		return ipMaquina;
	}

	public void setIpMaquina(String ipMaquina) {
		this.ipMaquina = ipMaquina;
	}
	
	
}
