package pe.gob.bnp.dapi.salalectura.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.domain.entity.SalaBiblioteca;

public class SesionSalaUsuario implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long idUsuarioSalaId;
	private SesionUsuario sesionUsuario;
	private SalaBiblioteca sala;
	private String codigoRptaValidacion;
	
	public SesionSalaUsuario() {
		this.idUsuarioSalaId = Constants.EMPTY_LONG;
		this.sesionUsuario = new SesionUsuario();
		this.sala = new SalaBiblioteca();
		this.codigoRptaValidacion = Constants.EMPTY_STRING;
	}
	
	public String getDescripcionRptaValidacion(){
		String respuesta = Constants.EMPTY_STRING;
		if (codigoRptaValidacion == null || codigoRptaValidacion.equals("")||codigoRptaValidacion.equals("-")){
			respuesta = "Solicitud sin c�digo de respuesta";
		}
		
		if (codigoRptaValidacion.equals("0000")){
			respuesta = "Se ejecut� la solicitud de forma exitosa.";
		}
		
		if (codigoRptaValidacion.equals("0001")){
			respuesta = "No tiene habilitado el acceso a la Sala.";
		}

		if (codigoRptaValidacion.equals("0002")){
			respuesta = "Su computadora no esta habilitada para ingresar a la Sala seleccionada (IP no coincide).";
		}
		
		return respuesta;
	}

	public Long getIdUsuarioSalaId() {
		return idUsuarioSalaId;
	}

	public void setIdUsuarioSalaId(Long idUsuarioSalaId) {
		this.idUsuarioSalaId = idUsuarioSalaId;
	}

	public SesionUsuario getSesionUsuario() {
		return sesionUsuario;
	}

	public void setSesionUsuario(SesionUsuario sesionUsuario) {
		this.sesionUsuario = sesionUsuario;
	}

	public SalaBiblioteca getSala() {
		return sala;
	}

	public void setSala(SalaBiblioteca sala) {
		this.sala = sala;
	}

	public String getCodigoRptaValidacion() {
		return codigoRptaValidacion;
	}

	public void setCodigoRptaValidacion(String codigoRptaValidacion) {
		this.codigoRptaValidacion = codigoRptaValidacion;
	}

}
