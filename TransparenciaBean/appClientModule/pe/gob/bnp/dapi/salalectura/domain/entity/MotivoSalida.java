package pe.gob.bnp.dapi.salalectura.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class MotivoSalida implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long idMotivoSalida;
	private String motivoSalida;
	
	public MotivoSalida() {
		this.idMotivoSalida = Constants.EMPTY_LONG;
		this.motivoSalida 	= Constants.EMPTY_STRING;
	}

	public Long getIdMotivoSalida() {
		return idMotivoSalida;
	}

	public void setIdMotivoSalida(Long idMotivoSalida) {
		this.idMotivoSalida = idMotivoSalida;
	}

	public String getMotivoSalida() {
		return motivoSalida;
	}

	public void setMotivoSalida(String motivoSalida) {
		this.motivoSalida = motivoSalida;
	}
	
	

}
