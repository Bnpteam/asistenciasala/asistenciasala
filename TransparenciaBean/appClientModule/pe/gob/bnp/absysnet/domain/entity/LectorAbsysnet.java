package pe.gob.bnp.absysnet.domain.entity;

import java.io.Serializable;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.domain.entity.RegistroUsuario;
import pe.gob.bnp.dapi.domain.entity.base.EntityGeneral;
import pe.gob.servir.systems.util.validator.VO;

public class LectorAbsysnet extends EntityGeneral implements Serializable{
	private static final long serialVersionUID = 1L;
	private long codBarrasLector;
	private String codBiblioteca;
	private String codSucursal;
	private String flagAsignadoBibl;
	private String esAdulto;
	private String estado;
	private Date   fechaInicio;
	private Date   fechaFin;
	private String tipoDocIdentidad;
	private String numDocIdentidad;
	private String nombres;
	private String apellidos;
	private Date   fechaNacimiento;
	private String sexo;
	private String direccion;
	private String telefonoFijo;
	private String telefonoMovil;
	private String correo;
	private String centroEstudios;
	private String direccionCentroEstudios;
	private String institucionAval;
	private String temaInvestigacion;
	private String tipoLector;
	private String tipoCarnet;
	private String departProvincia;
	private String distrito;
	private String nivelEstudio;
	private String telefonoCentroEstudio;
	private Date   fechaSuspension;
	private String codPais;

	
	
	public LectorAbsysnet() {
		this.codBarrasLector 	= Constants.EMPTY_LONG;
		this.codBiblioteca 		= Constants.EMPTY_STRING;
		this.codSucursal 		= Constants.EMPTY_STRING;
		this.flagAsignadoBibl 	= Constants.EMPTY_STRING;
		this.esAdulto 			= Constants.EMPTY_STRING;
		this.estado 			= Constants.EMPTY_STRING;
		this.fechaInicio 		= null;
		this.fechaFin 			= null;
		this.tipoDocIdentidad 	= Constants.EMPTY_STRING;
		this.numDocIdentidad 	= Constants.EMPTY_STRING;
		this.nombres 			= Constants.EMPTY_STRING;
		this.apellidos 			= Constants.EMPTY_STRING;
		this.fechaNacimiento 	= null;
		this.sexo 				= Constants.EMPTY_STRING;
		this.direccion 			= Constants.EMPTY_STRING;
		this.telefonoFijo 		= Constants.EMPTY_STRING;
		this.telefonoMovil 		= Constants.EMPTY_STRING;
		this.correo 			= Constants.EMPTY_STRING;
		this.centroEstudios 	= Constants.EMPTY_STRING;
		this.direccionCentroEstudios = Constants.EMPTY_STRING;
		this.institucionAval 	= Constants.EMPTY_STRING;
		this.temaInvestigacion 	= Constants.EMPTY_STRING;
		this.tipoLector 		= Constants.EMPTY_STRING;
		this.tipoCarnet 		= Constants.EMPTY_STRING;
		this.departProvincia 		= Constants.EMPTY_STRING;
		this.distrito 			= Constants.EMPTY_STRING;
		this.nivelEstudio 		= Constants.EMPTY_STRING;
		this.telefonoCentroEstudio = Constants.EMPTY_STRING;
		this.fechaSuspension 	= null;
		this.codPais 			= Constants.EMPTY_STRING;
	}
	
	public long obtenerCodigoLectorAbsysnet(String codLector){
		String codLectorCadena = codLector;
		long resultado = 0L;
		if (codLector == null) return 0L;
		resultado = codLectorCadena.equals(Constants.EMPTY_STRING)?0L:Long.parseLong(codLectorCadena);
		return resultado;
	}
	
	public String obtenerStringRecortado(String valor, int longitud){
		String resultado = "";
		String cadena = VO.getStringFormatted(valor);
		if (cadena.length()>50){
			resultado = cadena.substring(0, longitud);
		}else{
			resultado = cadena;
		}
		return resultado;
	}		
	
	public String obtenerEstadoLector(String estadoLectorSIRU){
		String respuesta = "";
		if (estadoLectorSIRU.equals(Constants.REGISTER_USER_STATE_ENABLED)){
			respuesta = Constants.ABSYSNET_LECONF_ACTIVE;
		}else{
			respuesta = Constants.ABSYSNET_LECONF_INACTIVE;
		}
		return respuesta;
	}
	
	public String obtenerTipoDocIdentidadLector(String tipoDocIdentidadSIRU){
		String respuesta = Constants.EMPTY_STRING;
		if (tipoDocIdentidadSIRU.equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE)){
			respuesta = Constants.ABSYSNET_LECTOR_LECOL3_DNI;
		}
		if (tipoDocIdentidadSIRU.equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE)){
			respuesta = Constants.ABSYSNET_LECTOR_LECOL3_CE;
		}
		if (tipoDocIdentidadSIRU.equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_CODE)){
			respuesta = Constants.ABSYSNET_LECTOR_LECOL3_PASAPORTE;
		}
		return respuesta;
	}
	
	public String obtenerTipoLector(String tipoLectorSIRU){
		String respuesta = Constants.EMPTY_STRING;
		if (tipoLectorSIRU.equals(Constants.USER_TYPE_RESEARCHER)){
			respuesta = Constants.ABSYSNET_LECOLP_INVESTIGADOR;
		}
		if (tipoLectorSIRU.equals(Constants.USER_TYPE_GENERAL)){
			respuesta = Constants.ABSYSNET_LECOLP_GENERAL;
		}
		return respuesta;
	}	
	
	public String obtenerTipoCarnet(String tipoLectorSIRU){
		String respuesta = Constants.EMPTY_STRING;
		if (tipoLectorSIRU.equals(Constants.USER_TYPE_RESEARCHER)){
			respuesta = Constants.ABSYSNET_LECOL1_INVESTIGADOR;
		}
		if (tipoLectorSIRU.equals(Constants.USER_TYPE_GENERAL)){
			respuesta = Constants.ABSYSNET_LECOL1_GENERAL;
		}
		return respuesta;
	}		
	
	public String obtenerCodigoPais(String codigoPaisSIRU){
		String resultado = Constants.EMPTY_STRING;
		if (codigoPaisSIRU.equals(Constants.COUNTRY_CODE_PERU)){
			resultado = "PE";
		}
		return resultado;
	}
	public void cargarObjetoParaCambioEstado(String codBarrasLector, String nuevoEstadoSIRU, String tipoDocIdentidadSIRU, String numDocIdentidadSIRU, Date fechaInicioSIRU, Date fechaFinSIRU, Date fechaInicioSuspensionSIRU){
		this.codBarrasLector 	= obtenerCodigoLectorAbsysnet(VO.getString(codBarrasLector,10));
		this.estado 			= VO.getString(this.obtenerEstadoLector(nuevoEstadoSIRU),1);
		this.tipoDocIdentidad 	= VO.getString(this.obtenerTipoDocIdentidadLector(tipoDocIdentidadSIRU),5);
		this.numDocIdentidad 	= VO.getString(numDocIdentidadSIRU,20);
		this.fechaInicio 		= fechaInicioSIRU;
		this.fechaFin 			= fechaFinSIRU;
		this.fechaSuspension 	= fechaInicioSuspensionSIRU;
	}
	
	public void cargarObjetoLector(RegistroUsuario usuarioSIRU){
		if (usuarioSIRU!=null){
			this.codBarrasLector 	= obtenerCodigoLectorAbsysnet(VO.getString(usuarioSIRU.getCodBarrasAbsysnet(),10));
			this.codBiblioteca 		= usuarioSIRU.getCodBiblioteca();
			this.codSucursal 		= usuarioSIRU.getCodSucursal();
			this.flagAsignadoBibl 	= "1";
			this.esAdulto 			= "1";
			this.estado 			= VO.getString(this.obtenerEstadoLector(usuarioSIRU.getEstadoRegistro()),1);
			this.fechaInicio 		= usuarioSIRU.getFechaInicio();
			this.fechaFin 			= usuarioSIRU.getFechaFin();
			this.tipoDocIdentidad 	= VO.getString(this.obtenerTipoDocIdentidadLector(usuarioSIRU.getUser().getTipoDocumentoIdentidadId()),5);
			this.numDocIdentidad 	= VO.getString(usuarioSIRU.getUser().getNumeroDocumentoIdentidad(),20);
			this.nombres 			= VO.getStringUpper(usuarioSIRU.getUser().getNombre(),80);
			this.apellidos 			= VO.getStringUpper(usuarioSIRU.getUser().obtenerApellidoCompleto(),80);
			this.fechaNacimiento 	= usuarioSIRU.getUser().getFechaNacimiento();
			this.sexo 				= VO.getString(usuarioSIRU.getUser().getSexo(),4);
			this.direccion 			= VO.getStringUpper(usuarioSIRU.getUser().getDireccion().getDireccionResidencia().getDireccion(),50);
			this.telefonoFijo 		= VO.getString(usuarioSIRU.getUser().getTelefonoFijo(),15);
			this.telefonoMovil 		= VO.getString(usuarioSIRU.getUser().getTelefonoMovil(),15);
			this.correo 			= VO.getStringUpper(usuarioSIRU.getUser().getCorreoElectronico(),100);
			this.centroEstudios 	= VO.getStringUpper(usuarioSIRU.getUser().getCentroEstudio().getCentroEstudioNombre(),50);
			this.direccionCentroEstudios = VO.getStringUpper(usuarioSIRU.getUser().getCentroEstudio().getDireccion().getDireccion(),50);
			this.institucionAval 	= VO.getStringUpper(usuarioSIRU.getUser().getNombreInstitucionAvala(),255);
			this.temaInvestigacion 	= VO.getStringUpper(usuarioSIRU.getUser().getTemaInvestigacion(),255);
			this.tipoLector 		= VO.getString(this.obtenerTipoLector(usuarioSIRU.getUser().getTipoUsuarioBiblioteca()),5);
			this.tipoCarnet 		= VO.getString(this.obtenerTipoCarnet(usuarioSIRU.getUser().getTipoUsuarioBiblioteca()),5);
			this.departProvincia	= VO.getStringUpper(usuarioSIRU.getUser().getDireccion().getDireccionResidencia().getNombreDep()+"/"+usuarioSIRU.getUser().getDireccion().getDireccionResidencia().getNombrePro(),50);
			this.distrito 			= VO.getStringUpper(usuarioSIRU.getUser().getDireccion().getDireccionResidencia().getNombreDis(),50);
			this.nivelEstudio 		= VO.getStringUpper(usuarioSIRU.getUser().getNombreGradoInstruccion(),50);
			this.telefonoCentroEstudio = VO.getString(usuarioSIRU.getUser().getCentroEstudio().getTelefono(),15);
			this.fechaSuspension 	= usuarioSIRU.getFechaInicioSuspension();
			this.codPais			= VO.getString(this.obtenerCodigoPais(usuarioSIRU.getUser().getDireccion().getDireccionResidencia().getCodPais()),5);
		}
	}
	
	public long getCodBarrasLector() {
		return codBarrasLector;
	}
	public void setCodBarrasLector(long codBarrasLector) {
		this.codBarrasLector = codBarrasLector;
	}
	public String getCodBiblioteca() {
		return codBiblioteca;
	}
	public void setCodBiblioteca(String codBiblioteca) {
		this.codBiblioteca = codBiblioteca;
	}
	public String getCodSucursal() {
		return codSucursal;
	}
	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}
	public String getFlagAsignadoBibl() {
		return flagAsignadoBibl;
	}
	public void setFlagAsignadoBibl(String flagAsignadoBibl) {
		this.flagAsignadoBibl = flagAsignadoBibl;
	}
	public String getEsAdulto() {
		return esAdulto;
	}
	public void setEsAdulto(String esAdulto) {
		this.esAdulto = esAdulto;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getTipoDocIdentidad() {
		return tipoDocIdentidad;
	}
	public void setTipoDocIdentidad(String tipoDocIdentidad) {
		this.tipoDocIdentidad = tipoDocIdentidad;
	}
	public String getNumDocIdentidad() {
		return numDocIdentidad;
	}
	public void setNumDocIdentidad(String numDocIdentidad) {
		this.numDocIdentidad = numDocIdentidad;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefonoFijo() {
		return telefonoFijo;
	}
	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}
	public String getTelefonoMovil() {
		return telefonoMovil;
	}
	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCentroEstudios() {
		return centroEstudios;
	}
	public void setCentroEstudios(String centroEstudios) {
		this.centroEstudios = centroEstudios;
	}
	public String getDireccionCentroEstudios() {
		return direccionCentroEstudios;
	}
	public void setDireccionCentroEstudios(String direccionCentroEstudios) {
		this.direccionCentroEstudios = direccionCentroEstudios;
	}
	public String getInstitucionAval() {
		return institucionAval;
	}
	public void setInstitucionAval(String institucionAval) {
		this.institucionAval = institucionAval;
	}
	public String getTemaInvestigacion() {
		return temaInvestigacion;
	}
	public void setTemaInvestigacion(String temaInvestigacion) {
		this.temaInvestigacion = temaInvestigacion;
	}
	public String getTipoLector() {
		return tipoLector;
	}
	public void setTipoLector(String tipoLector) {
		this.tipoLector = tipoLector;
	}
	public String getTipoCarnet() {
		return tipoCarnet;
	}
	public void setTipoCarnet(String tipoCarnet) {
		this.tipoCarnet = tipoCarnet;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getNivelEstudio() {
		return nivelEstudio;
	}
	public void setNivelEstudio(String nivelEstudio) {
		this.nivelEstudio = nivelEstudio;
	}
	public String getTelefonoCentroEstudio() {
		return telefonoCentroEstudio;
	}
	public void setTelefonoCentroEstudio(String telefonoCentroEstudio) {
		this.telefonoCentroEstudio = telefonoCentroEstudio;
	}
	public Date getFechaSuspension() {
		return fechaSuspension;
	}
	public void setFechaSuspension(Date fechaSuspension) {
		this.fechaSuspension = fechaSuspension;
	}

	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getDepartProvincia() {
		return departProvincia;
	}

	public void setDepartProvincia(String departProvincia) {
		this.departProvincia = departProvincia;
	}

}
