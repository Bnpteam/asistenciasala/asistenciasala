package pe.gob.bnp.absysnet.domain.entity.dto;

import java.io.Serializable;
import java.util.Date;

public class FiltroBusquedaReporte implements Serializable {
	private static final long serialVersionUID = 1L;
	public Date rangoFechaDesde;
	public Date rangoFechaHasta;
	
	public Date getRangoFechaDesde() {
		return rangoFechaDesde;
	}
	public void setRangoFechaDesde(Date rangoFechaDesde) {
		this.rangoFechaDesde = rangoFechaDesde;
	}
	public Date getRangoFechaHasta() {
		return rangoFechaHasta;
	}
	public void setRangoFechaHasta(Date rangoFechaHasta) {
		this.rangoFechaHasta = rangoFechaHasta;
	}
	
	
}
