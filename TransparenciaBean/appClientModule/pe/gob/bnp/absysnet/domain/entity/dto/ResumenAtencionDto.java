package pe.gob.bnp.absysnet.domain.entity.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class ResumenAtencionDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String year;
	private String mes;
	private String tipoUsuario;
	private String tipoRegistro;
	private String dia01;
	private String dia02;
	private String dia03;
	private String dia04;
	private String dia05;
	private String dia06;
	private String dia07;
	private String dia08;
	private String dia09;
	private String dia10;
	private String dia11;
	private String dia12;
	private String dia13;
	private String dia14;
	private String dia15;
	private String dia16;
	private String dia17;
	private String dia18;
	private String dia19;
	private String dia20;
	private String dia21;
	private String dia22;
	private String dia23;
	private String dia24;
	private String dia25;
	private String dia26;
	private String dia27;
	private String dia28;
	private String dia29;
	private String dia30;
	private String dia31;
	private String totalFila;
	
	public ResumenAtencionDto() {
		this.year = Constants.EMPTY_STRING;
		this.mes = Constants.EMPTY_STRING;
		this.tipoUsuario = Constants.EMPTY_STRING;
		this.tipoRegistro = Constants.EMPTY_STRING;
		this.dia01 = Constants.ZERO_VALUE_STRING;
		this.dia02 = Constants.ZERO_VALUE_STRING;
		this.dia03 = Constants.ZERO_VALUE_STRING;
		this.dia04 = Constants.ZERO_VALUE_STRING;
		this.dia05 = Constants.ZERO_VALUE_STRING;
		this.dia06 = Constants.ZERO_VALUE_STRING;
		this.dia07 = Constants.ZERO_VALUE_STRING;
		this.dia08 = Constants.ZERO_VALUE_STRING;
		this.dia09 = Constants.ZERO_VALUE_STRING;
		this.dia10 = Constants.ZERO_VALUE_STRING;
		this.dia11 = Constants.ZERO_VALUE_STRING;
		this.dia12 = Constants.ZERO_VALUE_STRING;
		this.dia13 = Constants.ZERO_VALUE_STRING;;
		this.dia14 = Constants.ZERO_VALUE_STRING;;
		this.dia15 = Constants.ZERO_VALUE_STRING;;
		this.dia16 = Constants.ZERO_VALUE_STRING;;
		this.dia17 = Constants.ZERO_VALUE_STRING;;
		this.dia18 = Constants.ZERO_VALUE_STRING;;
		this.dia19 = Constants.ZERO_VALUE_STRING;;
		this.dia20 = Constants.ZERO_VALUE_STRING;;
		this.dia21 = Constants.ZERO_VALUE_STRING;;
		this.dia22 = Constants.ZERO_VALUE_STRING;;
		this.dia23 = Constants.ZERO_VALUE_STRING;;
		this.dia24 = Constants.ZERO_VALUE_STRING;;
		this.dia25 = Constants.ZERO_VALUE_STRING;;
		this.dia26 = Constants.ZERO_VALUE_STRING;;
		this.dia27 = Constants.ZERO_VALUE_STRING;;
		this.dia28 = Constants.ZERO_VALUE_STRING;;
		this.dia29 = Constants.ZERO_VALUE_STRING;;
		this.dia30 = Constants.ZERO_VALUE_STRING;;
		this.dia31 = Constants.ZERO_VALUE_STRING;;
		this.totalFila = Constants.ZERO_VALUE_STRING;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getDia01() {
		return dia01;
	}

	public Integer getDia01Number() {
		return  Integer.parseInt(dia01);
	}
	
	public void setDia01(String dia01) {
		this.dia01 = dia01;
	}

	public String getDia02() {
		return dia02;
	}

	public Integer getDia02Number() {
		return  Integer.parseInt(dia02);
	}
	
	public void setDia02(String dia02) {
		this.dia02 = dia02;
	}

	public String getDia03() {
		return dia03;
	}

	public Integer getDia03Number() {
		return  Integer.parseInt(dia03);
	}

	
	public void setDia03(String dia03) {
		this.dia03 = dia03;
	}

	public String getDia04() {
		return dia04;
	}

	public Integer getDia04Number() {
		return  Integer.parseInt(dia04);
	}

	
	public void setDia04(String dia04) {
		this.dia04 = dia04;
	}

	public String getDia05() {
		return dia05;
	}

	public Integer getDia05Number() {
		return  Integer.parseInt(dia05);
	}

	
	public void setDia05(String dia05) {
		this.dia05 = dia05;
	}

	public String getDia06() {
		return dia06;
	}

	public Integer getDia06Number() {
		return  Integer.parseInt(dia06);
	}
	
	public void setDia06(String dia06) {
		this.dia06 = dia06;
	}

	public String getDia07() {
		return dia07;
	}

	public Integer getDia07Number() {
		return  Integer.parseInt(dia07);
	}
	
	public void setDia07(String dia07) {
		this.dia07 = dia07;
	}

	public String getDia08() {
		return dia08;
	}

	public Integer getDia08Number() {
		return  Integer.parseInt(dia08);
	}
	
	public void setDia08(String dia08) {
		this.dia08 = dia08;
	}

	public String getDia09() {
		return dia09;
	}
	
	public Integer getDia09Number() {
		return  Integer.parseInt(dia09);
	}
	

	public void setDia09(String dia09) {
		this.dia09 = dia09;
	}

	public String getDia10() {
		return dia10;
	}

	public Integer getDia10Number() {
		return  Integer.parseInt(dia10);
	}
	
	public void setDia10(String dia10) {
		this.dia10 = dia10;
	}

	public String getDia11() {
		return dia11;
	}
	
	public Integer getDia11Number() {
		return  Integer.parseInt(dia11);
	}
	

	public void setDia11(String dia11) {
		this.dia11 = dia11;
	}

	public String getDia12() {
		return dia12;
	}

	public Integer getDia12Number() {
		return  Integer.parseInt(dia12);
	}
	
	public void setDia12(String dia12) {
		this.dia12 = dia12;
	}

	public String getDia13() {
		return dia13;
	}

	public Integer getDia13Number() {
		return  Integer.parseInt(dia13);
	}
	
	public void setDia13(String dia13) {
		this.dia13 = dia13;
	}

	public String getDia14() {
		return dia14;
	}

	public Integer getDia14Number() {
		return  Integer.parseInt(dia14);
	}
	
	public void setDia14(String dia14) {
		this.dia14 = dia14;
	}

	public String getDia15() {
		return dia15;
	}

	public Integer getDia15Number() {
		return  Integer.parseInt(dia15);
	}
	
	public void setDia15(String dia15) {
		this.dia15 = dia15;
	}

	public String getDia16() {
		return dia16;
	}

	public Integer getDia16Number() {
		return  Integer.parseInt(dia16);
	}
	
	public void setDia16(String dia16) {
		this.dia16 = dia16;
	}

	public String getDia17() {
		return dia17;
	}

	public Integer getDia17Number() {
		return  Integer.parseInt(dia17);
	}
	
	public void setDia17(String dia17) {
		this.dia17 = dia17;
	}

	public String getDia18() {
		return dia18;
	}

	public Integer getDia18Number() {
		return  Integer.parseInt(dia18);
	}
	
	public void setDia18(String dia18) {
		this.dia18 = dia18;
	}

	public String getDia19() {
		return dia19;
	}

	public Integer getDia19Number() {
		return  Integer.parseInt(dia19);
	}
	
	public void setDia19(String dia19) {
		this.dia19 = dia19;
	}

	public String getDia20() {
		return dia20;
	}

	public Integer getDia20Number() {
		return  Integer.parseInt(dia20);
	}

	public void setDia20(String dia20) {
		this.dia20 = dia20;
	}

	public String getDia21() {
		return dia21;
	}
	
	public Integer getDia21Number() {
		return  Integer.parseInt(dia21);
	}

	public void setDia21(String dia21) {
		this.dia21 = dia21;
	}

	public String getDia22() {
		return dia22;
	}

	public Integer getDia22Number() {
		return  Integer.parseInt(dia22);
	}
	
	public void setDia22(String dia22) {
		this.dia22 = dia22;
	}

	public String getDia23() {
		return dia23;
	}

	public Integer getDia23Number() {
		return  Integer.parseInt(dia23);
	}
	
	public void setDia23(String dia23) {
		this.dia23 = dia23;
	}

	public String getDia24() {
		return dia24;
	}

	public Integer getDia24Number() {
		return  Integer.parseInt(dia24);
	}
	
	public void setDia24(String dia24) {
		this.dia24 = dia24;
	}

	public String getDia25() {
		return dia25;
	}

	public Integer getDia25Number() {
		return  Integer.parseInt(dia25);
	}
	
	public void setDia25(String dia25) {
		this.dia25 = dia25;
	}

	public String getDia26() {
		return dia26;
	}

	public Integer getDia26Number() {
		return  Integer.parseInt(dia26);
	}
	
	public void setDia26(String dia26) {
		this.dia26 = dia26;
	}

	public String getDia27() {
		return dia27;
	}

	public Integer getDia27Number() {
		return  Integer.parseInt(dia27);
	}
	
	public void setDia27(String dia27) {
		this.dia27 = dia27;
	}

	public String getDia28() {
		return dia28;
	}

	public Integer getDia28Number() {
		return  Integer.parseInt(dia28);
	}
	
	public void setDia28(String dia28) {
		this.dia28 = dia28;
	}

	public String getDia29() {
		return dia29;
	}

	public Integer getDia29Number() {
		return  Integer.parseInt(dia29);
	}
	
	public void setDia29(String dia29) {
		this.dia29 = dia29;
	}

	public String getDia30() {
		return dia30;
	}

	public Integer getDia30Number() {
		return  Integer.parseInt(dia30);
	}
	
	public void setDia30(String dia30) {
		this.dia30 = dia30;
	}

	public String getDia31() {
		return dia31;
	}

	public Integer getDia31Number() {
		return  Integer.parseInt(dia31);
	}
	
	public void setDia31(String dia31) {
		this.dia31 = dia31;
	}

	public String getTotalFila() {
		return totalFila;
	}

	public Integer getTotalFilaNumber() {
		return Integer.parseInt(totalFila);
	}	
	
	public void setTotalFila(String totalFila) {
		this.totalFila = totalFila;
	}

	
}
