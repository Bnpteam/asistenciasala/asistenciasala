package pe.gob.servir.sistemas.transparencia.model.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class FiltroRegistroUsuarioDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tipoUsuarioBiblioteca;
	private String tipoDocumentoId;
	private String numeroDocumento;
	private String estadoRegistro;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;

	public FiltroRegistroUsuarioDto() {
		this.tipoUsuarioBiblioteca = Constants.EMPTY_STRING;
		this.tipoDocumentoId = Constants.EMPTY_STRING;
		this.numeroDocumento = Constants.EMPTY_STRING;
		this.estadoRegistro = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.apellidoPaterno = Constants.EMPTY_STRING;
		this.apellidoMaterno = Constants.EMPTY_STRING;
	}

	public FiltroRegistroUsuarioDto(String tipoDocumentoId,
			String numeroDocumento, String estadoRegistro, String nombre,
			String apellidoPaterno, String apellidoMaterno, String tipoUsuarioBiblioteca) {
		super();
		this.tipoDocumentoId = tipoDocumentoId;
		this.numeroDocumento = numeroDocumento;
		this.estadoRegistro = estadoRegistro;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.tipoUsuarioBiblioteca = tipoUsuarioBiblioteca;
	}

	public String getTipoDocumentoId() {
		return tipoDocumentoId;
	}

	public void setTipoDocumentoId(String tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTipoUsuarioBiblioteca() {
		return tipoUsuarioBiblioteca;
	}

	public void setTipoUsuarioBiblioteca(String tipoUsuarioBiblioteca) {
		this.tipoUsuarioBiblioteca = tipoUsuarioBiblioteca;
	}

}
