package pe.gob.servir.sistemas.transparencia.model.administracion;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class Perfil extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String nombre;
	
	public Perfil(){
		this.init();
	}
	
	public Perfil(Long id){
		super.setId(id);
		this.init();
	}
	
	private void init(){
		this.setCodigo("");
		this.setNombre("");
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
