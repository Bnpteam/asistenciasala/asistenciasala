package pe.gob.servir.sistemas.transparencia.model.dto;

import java.io.Serializable;
import java.util.Date;

public class HistorialRegistroDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String fechaMovimiento;
	private String tipoUsuarioBiblioteca;
	private String tipoRegistro;
	private String estado;
	private String vigencia;
	private String fechaInicio;
	private String fechaFin;
	private Double costo;
	private String moneda;
	private String fechaPago;
	private String reciboPago;
	private String nombreUsuarioInterno;
	private String fechaInicioSuspension;
	private String fechaFinSuspension;
	private String periodoSuspension; 	
	
	public String getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(String fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public String getTipoUsuarioBiblioteca() {
		return tipoUsuarioBiblioteca;
	}
	public void setTipoUsuarioBiblioteca(String tipoUsuarioBiblioteca) {
		this.tipoUsuarioBiblioteca = tipoUsuarioBiblioteca;
	}
	public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Double getCosto() {
		return costo;
	}
	public void setCosto(Double costo) {
		this.costo = costo;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getReciboPago() {
		return reciboPago;
	}
	public void setReciboPago(String reciboPago) {
		this.reciboPago = reciboPago;
	}
	public String getNombreUsuarioInterno() {
		return nombreUsuarioInterno;
	}
	public void setNombreUsuarioInterno(String nombreUsuarioInterno) {
		this.nombreUsuarioInterno = nombreUsuarioInterno;
	}
	public String getPeriodoSuspension() {
		return periodoSuspension;
	}
	public void setPeriodoSuspension(String periodoSuspension) {
		this.periodoSuspension = periodoSuspension;
	}
	public String getFechaInicioSuspension() {
		return fechaInicioSuspension;
	}
	public void setFechaInicioSuspension(String fechaInicioSuspension) {
		this.fechaInicioSuspension = fechaInicioSuspension;
	}
	public String getFechaFinSuspension() {
		return fechaFinSuspension;
	}
	public void setFechaFinSuspension(String fechaFinSuspension) {
		this.fechaFinSuspension = fechaFinSuspension;
	}

	
}
