package pe.gob.servir.sistemas.transparencia.model.negocio;

import java.io.Serializable;
import java.util.Date;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class ReporteBandeja extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;
	//*------- TBL_SOLICITUD_TRANSPARENCIA -----------*****
	private long solicitud_transparencia_id;
	private String funcionario_resp_entrginfo;
	private String correlativo_solicitud;
	private int	   version_solicitud;
	private String anio_solicitud;
	private String tipo_documento_id; //No utilizado siempre es 0;
	private long tipo_documento_identidad_id;
	private String tipo_doc_identidad_detalle;
	private String numero_documento_identidad;
	private String apellido_paterno;
	private String apellido_materno;
	private String nombres;
	private String direccion_domicilio;
	private String coddep;
	private String coddep_detalle;
	private String codpro;
	private String codpro_detalle;
	private String coddis;
	private String coddis_detalle;
	private String telefono;
	private String fax;
	private String celular;
	private String correo_electronico;
	private String detalle_solicitud;
	private String dependencia_de_informacion;
	private String dependencia_nformacion_det;
	private String forma_de_entrega;
	private String flag_autoriza_recojo;
	private String pers_auto_nro_documento;
	private String pers_auto_nombre_apellidos;
	private String observacion;
	private String flag_declaracion_jurada;
	private String flag_notif_electronica;
	private String estado;
	private String situacion;
	private long usuario_id_registro;
	private String fecha_registro;
	private String usuario_id_modificacion;
	private String fecha_modificacion;
	private String fecha_ingreso_std;
	private String asunto_std;
	private String codigo;
	private String fecha_ingreso_oaj;
	private String ruta_documento_acceso;
	private String fecha_entrega_courier;
	private String fecha_cargo_courier;
	private String fecha_cargo_ciudadano;
	private String sumilla_cargo_ciudadano;
	private long   usuario_id_responsable;
	private String usuario_nombre_responsable;
	
	//*------- TBL_SOLICITUD_MOVIMIENTO -----------*****
	private long   solicitud_movimiento_id;
	private String t01_und_org_emisor_id;
	private String t01_und_org_emisor_deta;
	private String t01_und_org_receptor_id;
	private String t01_und_org_receptor_deta;
	private long   estado_solicitud_id;
	private String estado_solicitud_detall;
	private long   situacion_solicitud_id;
	private String situacion_solicitud_detall;
	private String correo;
	private String asunto;
	private String detalle_respuesta;
	private String detalle_asignacion;
	private String fecha_asignacion;
	private String listaUndOrgDeriv;
	
	private Date fechaIngreso;
	private Integer diasPlazoAtencion; 
	
	private Date fechaRegistroDesde;
	private Date fechaRegistroHasta;
	
	private RegistroSISTRA registroSISTRA;
	
	public ReporteBandeja() {
		super();
		/*Calendar cal= Calendar.getInstance();
		String year= String.valueOf(cal.get(Calendar.YEAR));*/
		this.setSolicitud_transparencia_id(0L);
		this.setAnio_solicitud("0");
		this.setNumero_documento_identidad("");
		this.setApellido_paterno("");
		this.setApellido_materno("");
		this.setNombres("");
		this.setSituacion_solicitud_id(0L);
		this.setCoddep("%");
		this.setCodpro("%");
		this.setCoddis("%");
		this.setRegistroSISTRA(new RegistroSISTRA());
	}

	public long getSolicitud_transparencia_id() {
		return solicitud_transparencia_id;
	}

	public void setSolicitud_transparencia_id(long solicitud_transparencia_id) {
		this.solicitud_transparencia_id = solicitud_transparencia_id;
	}

	public String getFuncionario_resp_entrginfo() {
		return funcionario_resp_entrginfo;
	}

	public void setFuncionario_resp_entrginfo(String funcionario_resp_entrginfo) {
		this.funcionario_resp_entrginfo = funcionario_resp_entrginfo;
	}

	public String getCorrelativo_solicitud() {
		return correlativo_solicitud;
	}

	public void setCorrelativo_solicitud(String correlativo_solicitud) {
		this.correlativo_solicitud = correlativo_solicitud;
	}

	public int getVersion_solicitud() {
		return version_solicitud;
	}

	public void setVersion_solicitud(int version_solicitud) {
		this.version_solicitud = version_solicitud;
	}

	public String getAnio_solicitud() {
		return anio_solicitud;
	}

	public void setAnio_solicitud(String anio_solicitud) {
		this.anio_solicitud = anio_solicitud;
	}

	public String getTipo_documento_id() {
		return tipo_documento_id;
	}

	public void setTipo_documento_id(String tipo_documento_id) {
		this.tipo_documento_id = tipo_documento_id;
	}

	public long getTipo_documento_identidad_id() {
		return tipo_documento_identidad_id;
	}

	public void setTipo_documento_identidad_id(long tipo_documento_identidad_id) {
		this.tipo_documento_identidad_id = tipo_documento_identidad_id;
	}

	public String getTipo_doc_identidad_detalle() {
		return tipo_doc_identidad_detalle;
	}

	public void setTipo_doc_identidad_detalle(String tipo_doc_identidad_detalle) {
		this.tipo_doc_identidad_detalle = tipo_doc_identidad_detalle;
	}

	public String getNumero_documento_identidad() {
		return numero_documento_identidad;
	}

	public void setNumero_documento_identidad(String numero_documento_identidad) {
		this.numero_documento_identidad = numero_documento_identidad;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getDireccion_domicilio() {
		return direccion_domicilio;
	}

	public void setDireccion_domicilio(String direccion_domicilio) {
		this.direccion_domicilio = direccion_domicilio;
	}

	public String getCoddep() {
		return coddep;
	}

	public void setCoddep(String coddep) {
		this.coddep = coddep;
	}

	public String getCoddep_detalle() {
		return coddep_detalle;
	}

	public void setCoddep_detalle(String coddep_detalle) {
		this.coddep_detalle = coddep_detalle;
	}

	public String getCodpro() {
		return codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	public String getCodpro_detalle() {
		return codpro_detalle;
	}

	public void setCodpro_detalle(String codpro_detalle) {
		this.codpro_detalle = codpro_detalle;
	}

	public String getCoddis() {
		return coddis;
	}

	public void setCoddis(String coddis) {
		this.coddis = coddis;
	}

	public String getCoddis_detalle() {
		return coddis_detalle;
	}

	public void setCoddis_detalle(String coddis_detalle) {
		this.coddis_detalle = coddis_detalle;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public String getDetalle_solicitud() {
		return detalle_solicitud;
	}

	public void setDetalle_solicitud(String detalle_solicitud) {
		this.detalle_solicitud = detalle_solicitud;
	}

	public String getDependencia_de_informacion() {
		return dependencia_de_informacion;
	}

	public void setDependencia_de_informacion(String dependencia_de_informacion) {
		this.dependencia_de_informacion = dependencia_de_informacion;
	}

	public String getDependencia_nformacion_det() {
		return dependencia_nformacion_det;
	}

	public void setDependencia_nformacion_det(String dependencia_nformacion_det) {
		this.dependencia_nformacion_det = dependencia_nformacion_det;
	}

	public String getForma_de_entrega() {
		return forma_de_entrega;
	}

	public void setForma_de_entrega(String forma_de_entrega) {
		this.forma_de_entrega = forma_de_entrega;
	}


	public String getFlag_autoriza_recojo() {
		return flag_autoriza_recojo;
	}


	public void setFlag_autoriza_recojo(String flag_autoriza_recojo) {
		this.flag_autoriza_recojo = flag_autoriza_recojo;
	}


	public String getPers_auto_nro_documento() {
		return pers_auto_nro_documento;
	}


	public void setPers_auto_nro_documento(String pers_auto_nro_documento) {
		this.pers_auto_nro_documento = pers_auto_nro_documento;
	}


	public String getPers_auto_nombre_apellidos() {
		return pers_auto_nombre_apellidos;
	}


	public void setPers_auto_nombre_apellidos(String pers_auto_nombre_apellidos) {
		this.pers_auto_nombre_apellidos = pers_auto_nombre_apellidos;
	}


	public String getObservacion() {
		return observacion;
	}


	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}


	public String getFlag_declaracion_jurada() {
		return flag_declaracion_jurada;
	}


	public void setFlag_declaracion_jurada(String flag_declaracion_jurada) {
		this.flag_declaracion_jurada = flag_declaracion_jurada;
	}


	public String getFlag_notif_electronica() {
		return flag_notif_electronica;
	}


	public void setFlag_notif_electronica(String flag_notif_electronica) {
		this.flag_notif_electronica = flag_notif_electronica;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getSituacion() {
		return situacion;
	}


	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}


	public long getUsuario_id_registro() {
		return usuario_id_registro;
	}


	public void setUsuario_id_registro(long usuario_id_registro) {
		this.usuario_id_registro = usuario_id_registro;
	}


	public String getFecha_registro() {
		return fecha_registro;
	}


	public void setFecha_registro(String fecha_registro) {
		this.fecha_registro = fecha_registro;
	}


	public String getUsuario_id_modificacion() {
		return usuario_id_modificacion;
	}


	public void setUsuario_id_modificacion(String usuario_id_modificacion) {
		this.usuario_id_modificacion = usuario_id_modificacion;
	}


	public String getFecha_modificacion() {
		return fecha_modificacion;
	}


	public void setFecha_modificacion(String fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}


	public String getFecha_ingreso_std() {
		return fecha_ingreso_std;
	}


	public void setFecha_ingreso_std(String fecha_ingreso_std) {
		this.fecha_ingreso_std = fecha_ingreso_std;
	}


	public String getAsunto_std() {
		return asunto_std;
	}


	public void setAsunto_std(String asunto_std) {
		this.asunto_std = asunto_std;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getFecha_ingreso_oaj() {
		return fecha_ingreso_oaj;
	}


	public void setFecha_ingreso_oaj(String fecha_ingreso_oaj) {
		this.fecha_ingreso_oaj = fecha_ingreso_oaj;
	}


	public String getRuta_documento_acceso() {
		return ruta_documento_acceso;
	}


	public void setRuta_documento_acceso(String ruta_documento_acceso) {
		this.ruta_documento_acceso = ruta_documento_acceso;
	}


	public String getFecha_entrega_courier() {
		return fecha_entrega_courier;
	}


	public void setFecha_entrega_courier(String fecha_entrega_courier) {
		this.fecha_entrega_courier = fecha_entrega_courier;
	}


	public String getFecha_cargo_courier() {
		return fecha_cargo_courier;
	}


	public void setFecha_cargo_courier(String fecha_cargo_courier) {
		this.fecha_cargo_courier = fecha_cargo_courier;
	}


	public String getFecha_cargo_ciudadano() {
		return fecha_cargo_ciudadano;
	}


	public void setFecha_cargo_ciudadano(String fecha_cargo_ciudadano) {
		this.fecha_cargo_ciudadano = fecha_cargo_ciudadano;
	}


	public String getSumilla_cargo_ciudadano() {
		return sumilla_cargo_ciudadano;
	}


	public void setSumilla_cargo_ciudadano(String sumilla_cargo_ciudadano) {
		this.sumilla_cargo_ciudadano = sumilla_cargo_ciudadano;
	}
	
	public long getUsuario_id_responsable() {
		return usuario_id_responsable;
	}

	public void setUsuario_id_responsable(long usuario_id_responsable) {
		this.usuario_id_responsable = usuario_id_responsable;
	}

	public String getUsuario_nombre_responsable() {
		return usuario_nombre_responsable;
	}

	public void setUsuario_nombre_responsable(String usuario_nombre_responsable) {
		this.usuario_nombre_responsable = usuario_nombre_responsable;
	}

	public long getSolicitud_movimiento_id() {
		return solicitud_movimiento_id;
	}

	public void setSolicitud_movimiento_id(long solicitud_movimiento_id) {
		this.solicitud_movimiento_id = solicitud_movimiento_id;
	}

	public String getT01_und_org_emisor_id() {
		return t01_und_org_emisor_id;
	}

	public void setT01_und_org_emisor_id(String t01_und_org_emisor_id) {
		this.t01_und_org_emisor_id = t01_und_org_emisor_id;
	}

	public String getT01_und_org_emisor_deta() {
		return t01_und_org_emisor_deta;
	}

	public void setT01_und_org_emisor_deta(String t01_und_org_emisor_deta) {
		this.t01_und_org_emisor_deta = t01_und_org_emisor_deta;
	}

	public String getT01_und_org_receptor_id() {
		return t01_und_org_receptor_id;
	}

	public void setT01_und_org_receptor_id(String t01_und_org_receptor_id) {
		this.t01_und_org_receptor_id = t01_und_org_receptor_id;
	}

	public String getT01_und_org_receptor_deta() {
		return t01_und_org_receptor_deta;
	}

	public void setT01_und_org_receptor_deta(String t01_und_org_receptor_deta) {
		this.t01_und_org_receptor_deta = t01_und_org_receptor_deta;
	}

	public long getEstado_solicitud_id() {
		return estado_solicitud_id;
	}

	public void setEstado_solicitud_id(long estado_solicitud_id) {
		this.estado_solicitud_id = estado_solicitud_id;
	}

	public String getEstado_solicitud_detall() {
		return estado_solicitud_detall;
	}

	public void setEstado_solicitud_detall(String estado_solicitud_detall) {
		this.estado_solicitud_detall = estado_solicitud_detall;
	}

	public long getSituacion_solicitud_id() {
		return situacion_solicitud_id;
	}


	public void setSituacion_solicitud_id(long situacion_solicitud_id) {
		this.situacion_solicitud_id = situacion_solicitud_id;
	}


	public String getSituacion_solicitud_detall() {
		return situacion_solicitud_detall;
	}


	public void setSituacion_solicitud_detall(String situacion_solicitud_detall) {
		this.situacion_solicitud_detall = situacion_solicitud_detall;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getDetalle_asignacion() {
		return detalle_asignacion;
	}

	public void setDetalle_asignacion(String detalle_asignacion) {
		this.detalle_asignacion = detalle_asignacion;
	}

	public String getFecha_asignacion() {
		return fecha_asignacion;
	}

	public void setFecha_asignacion(String fecha_asignacion) {
		this.fecha_asignacion = fecha_asignacion;
	}

	public String getListaUndOrgDeriv() {
		return listaUndOrgDeriv;
	}

	public void setListaUndOrgDeriv(String listaUndOrgDeriv) {
		this.listaUndOrgDeriv = listaUndOrgDeriv;
	}

	public String getDetalle_respuesta() {
		return detalle_respuesta;
	}

	public void setDetalle_respuesta(String detalle_respuesta) {
		this.detalle_respuesta = detalle_respuesta;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Integer getDiasPlazoAtencion() {
		return diasPlazoAtencion;
	}

	public void setDiasPlazoAtencion(Integer diasPlazoAtencion) {
		this.diasPlazoAtencion = diasPlazoAtencion;
	}

	public Date getFechaRegistroDesde() {
		return fechaRegistroDesde;
	}

	public void setFechaRegistroDesde(Date fechaRegistroDesde) {
		this.fechaRegistroDesde = fechaRegistroDesde;
	}

	public Date getFechaRegistroHasta() {
		return fechaRegistroHasta;
	}

	public void setFechaRegistroHasta(Date fechaRegistroHasta) {
		this.fechaRegistroHasta = fechaRegistroHasta;
	}

	public RegistroSISTRA getRegistroSISTRA() {
		return registroSISTRA;
	}

	public void setRegistroSISTRA(RegistroSISTRA registroSISTRA) {
		this.registroSISTRA = registroSISTRA;
	}

	
}
