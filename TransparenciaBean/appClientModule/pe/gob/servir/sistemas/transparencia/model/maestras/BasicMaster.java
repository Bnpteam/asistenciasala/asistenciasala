package pe.gob.servir.sistemas.transparencia.model.maestras;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class BasicMaster extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private String descripcion;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
