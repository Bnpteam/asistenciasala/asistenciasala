package pe.gob.servir.sistemas.transparencia.model.negocio;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class ReporteDerivacion extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private String 			nroSolicitudId;
	private String 		  	areaDerivada;
	private String			personaDerivada;
	private String 			estadoDerivacion;

	public ReporteDerivacion() {
		super();

	}

	public String getNroSolicitudId() {
		return nroSolicitudId;
	}

	public void setNroSolicitudId(String nroSolicitudId) {
		this.nroSolicitudId = nroSolicitudId;
	}

	public String getAreaDerivada() {
		return areaDerivada;
	}

	public void setAreaDerivada(String areaDerivada) {
		this.areaDerivada = areaDerivada;
	}

	public String getPersonaDerivada() {
		return personaDerivada;
	}

	public void setPersonaDerivada(String personaDerivada) {
		this.personaDerivada = personaDerivada;
	}

	public String getEstadoDerivacion() {
		return estadoDerivacion;
	}

	public void setEstadoDerivacion(String estadoDerivacion) {
		this.estadoDerivacion = estadoDerivacion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
