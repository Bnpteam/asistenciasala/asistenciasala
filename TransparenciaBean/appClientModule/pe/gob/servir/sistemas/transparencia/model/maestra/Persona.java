package pe.gob.servir.sistemas.transparencia.model.maestra;

import java.io.Serializable;
import java.util.Date;


public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	private long personaId;

	private String apCasada;

	private String apMaterno;

	private String apPaterno;

	private String direccion;

	private String estado;

	private String estadoCivil;

	private Date fechaCaducidad;

	private Date fechaFallecimiento;

	private Date fechaModificacion;

	private Date fechaNacimiento;

	private Date fechaRegistro;

	private String flagValidadoReniec;

	private String nomDepartamento;

	private String nomDistrito;

	private String nomMadre;

	private String nomPadre;

	private String nomProvincia;

	private String nombres;

	private String nroDocumento;

	private String sexo;

	private long tipoDocumentoId;
	
	private String ubgDepartamento;

	private String ubgDistrito;

	private String ubgProvincia;

	private String usuarioModificacion;

	private String usuarioRegistro;
	
	private boolean resultTransaction;
	
	/**temporal**/
	
	private String nombreCompleto;
	
	private String tipoDocumentoNom;
	
	private String sexoNom;
	
	public Persona() {
	}

	public Long getPersonaId() {
		return this.personaId;
	}

	public void setPersonaId(long personaId) {
		this.personaId = personaId;
	}

	public String getApCasada() {
		return this.apCasada;
	}

	public void setApCasada(String apCasada) {
		this.apCasada = apCasada;
	}

	public String getApMaterno() {
		return this.apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getApPaterno() {
		return this.apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Date getFechaCaducidad() {
		return this.fechaCaducidad;
	}

	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public Date getFechaFallecimiento() {
		return this.fechaFallecimiento;
	}

	public void setFechaFallecimiento(Date fechaFallecimiento) {
		this.fechaFallecimiento = fechaFallecimiento;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFlagValidadoReniec() {
		return this.flagValidadoReniec;
	}

	public void setFlagValidadoReniec(String flagValidadoReniec) {
		this.flagValidadoReniec = flagValidadoReniec;
	}

	public String getNomDepartamento() {
		return this.nomDepartamento;
	}

	public void setNomDepartamento(String nomDepartamento) {
		this.nomDepartamento = nomDepartamento;
	}

	public String getNomDistrito() {
		return this.nomDistrito;
	}

	public void setNomDistrito(String nomDistrito) {
		this.nomDistrito = nomDistrito;
	}

	public String getNomMadre() {
		return this.nomMadre;
	}

	public void setNomMadre(String nomMadre) {
		this.nomMadre = nomMadre;
	}

	public String getNomPadre() {
		return this.nomPadre;
	}

	public void setNomPadre(String nomPadre) {
		this.nomPadre = nomPadre;
	}

	public String getNomProvincia() {
		return this.nomProvincia;
	}

	public void setNomProvincia(String nomProvincia) {
		this.nomProvincia = nomProvincia;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNroDocumento() {
		return this.nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public long getTipoDocumentoId() {
		return this.tipoDocumentoId;
	}

	public void setTipoDocumentoId(long tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	public String getUbgDepartamento() {
		return this.ubgDepartamento;
	}

	public void setUbgDepartamento(String ubgDepartamento) {
		this.ubgDepartamento = ubgDepartamento;
	}

	public String getUbgDistrito() {
		return this.ubgDistrito;
	}

	public void setUbgDistrito(String ubgDistrito) {
		this.ubgDistrito = ubgDistrito;
	}

	public String getUbgProvincia() {
		return this.ubgProvincia;
	}

	public void setUbgProvincia(String ubgProvincia) {
		this.ubgProvincia = ubgProvincia;
	}

	public String getUsuarioModificacion() {
		return this.usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getUsuarioRegistro() {
		return this.usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getNombreCompleto() {
		//return nombreCompleto;
		return getApPaterno()+" "+getApMaterno()+", "+getNombres();
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTipoDocumentoNom() {
		return tipoDocumentoNom;
	}

	public void setTipoDocumentoNom(String tipoDocumentoNom) {
		this.tipoDocumentoNom = tipoDocumentoNom;
	}

	public boolean isResultTransaction() {
		return resultTransaction;
	}

	public void setResultTransaction(boolean resultTransaction) {
		this.resultTransaction = resultTransaction;
	}

	@Override
	public String toString() {
		return "Persona [personaId=" + personaId + ",\n apCasada=" + apCasada
				+ ",\n apMaterno=" + apMaterno + ",\n apPaterno=" + apPaterno
				+ ",\n direccion=" + direccion + ",\n estado=" + estado
				+ ",\n estadoCivil=" + estadoCivil + ",\n fechaCaducidad="
				+ fechaCaducidad + ",\n fechaFallecimiento="
				+ fechaFallecimiento + ",\n fechaModificacion="
				+ fechaModificacion + ",\n fechaNacimiento=" + fechaNacimiento
				+ ",\n fechaRegistro=" + fechaRegistro
				+ ",\n flagValidadoReniec=" + flagValidadoReniec
				+ ",\n nomDepartamento=" + nomDepartamento + ",\n nomDistrito="
				+ nomDistrito + ",\n nomMadre=" + nomMadre + ",\n nomPadre="
				+ nomPadre + ",\n nomProvincia=" + nomProvincia
				+ ",\n nombres=" + nombres + ",\n nroDocumento=" + nroDocumento
				+ ",\n sexo=" + sexo + ",\n tipoDocumentoId=" + tipoDocumentoId
				+ ",\n ubgDepartamento=" + ubgDepartamento + ",\n ubgDistrito="
				+ ubgDistrito + ",\n ubgProvincia=" + ubgProvincia
				+ ",\n usuarioModificacion=" + usuarioModificacion
				+ ",\n usuarioRegistro=" + usuarioRegistro
				+ ",\n nombreCompleto=" + nombreCompleto + "]";
	}

	public String getSexoNom() {
		return sexoNom;
	}

	public void setSexoNom(String sexoNom) {
		this.sexoNom = sexoNom;
	}

	
	
}