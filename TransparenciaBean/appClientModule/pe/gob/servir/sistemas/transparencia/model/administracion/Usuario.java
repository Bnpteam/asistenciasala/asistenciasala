package pe.gob.servir.sistemas.transparencia.model.administracion;

import java.io.Serializable;

import pe.gob.bnp.dapi.salalectura.domain.entity.SesionSalaUsuario;
import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class Usuario extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private String			usuario;
	private String			nombreCompleto;
	private String 			clave;
	//private String			nroDNI;
	private Perfil			perfil;
	private String			idUnidadOrganica;
	private String			correo;
	private Boolean			swDisponible;
	
	private Long			consultasAsignadas;
	
	private SesionSalaUsuario sesionSalaUsuario;
	
	public Usuario(){
		this.init();
		this.sesionSalaUsuario = new SesionSalaUsuario();
	}
	
	public Usuario(Long id){
		super.setId(id);
		this.init();
	}
	
	private void init(){
		this.setPerfil(new Perfil());
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
/*
	public String getNroDNI() {
		return nroDNI;
	}

	public void setNroDNI(String nroDNI) {
		this.nroDNI = nroDNI;
	}
*/
	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	
	public String getIdUnidadOrganica() {
		return idUnidadOrganica;
	}

	public void setIdUnidadOrganica(String idUnidadOrganica) {
		this.idUnidadOrganica = idUnidadOrganica;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Boolean getSwDisponible() {
		return swDisponible;
	}

	public void setSwDisponible(Boolean swDisponible) {
		this.swDisponible = swDisponible;
	}

	public Long getConsultasAsignadas() {
		return consultasAsignadas;
	}

	public void setConsultasAsignadas(Long consultasAsignadas) {
		this.consultasAsignadas = consultasAsignadas;
	}

	public SesionSalaUsuario getSesionSalaUsuario() {
		return sesionSalaUsuario;
	}

	public void setSesionSalaUsuario(SesionSalaUsuario sesionSalaUsuario) {
		this.sesionSalaUsuario = sesionSalaUsuario;
	}

	
}
