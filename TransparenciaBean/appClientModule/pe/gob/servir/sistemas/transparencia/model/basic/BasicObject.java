package pe.gob.servir.sistemas.transparencia.model.basic;

import java.io.Serializable;
import java.util.Date;

public class BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long 			id;
	private String 			estado;
	
	private String 			situacion;
	
	private Long 			usuarioIdRegistro;
	private Date 			fechaRegistro;
	private String 			ipRegistro;

	private Long 			usuarioIdModificacion;
	private Date 			fechaModificacion;
	
	private String 			ipModificacion;
	private String 			obsModificacion;
	
	private Boolean			resultTransaction;
	private Exception		exception;
	private Boolean			swException;
	
	public Boolean getResultTransaction() {
		return resultTransaction;
	}

	public void setResultTransaction(Boolean resultTransaction) {
		this.resultTransaction = resultTransaction;
	}

	public BasicObject() {
		super();
		this.init();
	}
	
	private void init(){
		this.setId(0L);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}
	
	public Boolean getSwException() {
		return swException;
	}

	public void setSwException(Boolean swException) {
		this.swException = swException;
	}
	
	public void setStackException(Exception exception,Boolean swException) {
		this.setException(exception);
		this.setSwException(swException);
	}
	
	public long getUsuarioIdRegistro() {
		return usuarioIdRegistro;
	}

	public void setUsuarioIdRegistro(long usuarioIdRegistro) {
		this.usuarioIdRegistro = usuarioIdRegistro;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public long getUsuarioIdModificacion() {
		return usuarioIdModificacion;
	}

	public void setUsuarioIdModificacion(long usuarioIdModificacion) {
		this.usuarioIdModificacion = usuarioIdModificacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}
	

	public String getIpModificacion() {
		return ipModificacion;
	}

	public void setIpModificacion(String ipModificacion) {
		this.ipModificacion = ipModificacion;
	}

	public String getObsModificacion() {
		return obsModificacion;
	}

	public void setObsModificacion(String obsModificacion) {
		this.obsModificacion = obsModificacion;
	}	

	public String getIpRegistro() {
		return ipRegistro;
	}

	public void setIpRegistro(String ipRegistro) {
		this.ipRegistro = ipRegistro;
	}

	@Override
	public String toString() {
		return "BasicObject [id=" + id + ", estado=" + estado
				+ ", usuarioIdRegistro=" + usuarioIdRegistro
				+ ", fechaRegistro=" + fechaRegistro
				+ ", usuarioIdModificacion=" + usuarioIdModificacion
				+ ", fechaModificacion=" + fechaModificacion
				+ ", resultTransaction=" + resultTransaction + ", exception="
				+ exception + ", swException=" + swException + "]";
	}
	
}
