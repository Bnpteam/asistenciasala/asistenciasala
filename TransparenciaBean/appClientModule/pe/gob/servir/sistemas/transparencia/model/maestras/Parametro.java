package pe.gob.servir.sistemas.transparencia.model.maestras;

import java.io.Serializable;

public class Parametro extends BasicMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String codigo;
	private Long valor;
	private String tipoConsultaExterna;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Long getValor() {
		return valor;
	}
	public void setValor(Long valor) {
		this.valor = valor;
	}
	public String getTipoConsultaExterna() {
		return tipoConsultaExterna;
	}
	public void setTipoConsultaExterna(String tipoConsultaExterna) {
		this.tipoConsultaExterna = tipoConsultaExterna;
	}
		
	
}
