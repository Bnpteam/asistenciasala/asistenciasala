package pe.gob.servir.sistemas.transparencia.model.administracion;

import java.io.Serializable;
import java.util.Date;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class Ausencia extends BasicObject implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Usuario			usuario;
	private Date			fechaInicio;
	private Date			fechaFin;
	private String			comentario;
	
	public Ausencia(){
		this.init();
	}
	
	public Ausencia(Long id){
		super.setId(id);
		this.init();
	}
	
	private void init(){
		this.setUsuario(new Usuario());
	}
		
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
