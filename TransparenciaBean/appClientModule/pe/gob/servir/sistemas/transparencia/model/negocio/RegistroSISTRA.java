package pe.gob.servir.sistemas.transparencia.model.negocio;

import java.io.Serializable;
import java.util.Date;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class RegistroSISTRA extends BasicObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numeroSistra;
	private String numeroDocumento;
	private String fechaRegistroSistra;
	private String usuarioRegistro;
	private String descripcionTipoOrigen;
	private String descripcionTramite;
	private String nroFolios;
	private String observacion;
	private String asunto;
	private String clasificador;
	private String nombreDocumento;
	private String descripcionTipoDocumento;
	private String oficinaOrigen;
	private String remite;
	private String plazoMaximo;
	private String rutaPdf;
	private String nombrePdf;
	private String descripcionTipoDestino;
	private String estado;
	private String oficinaDestino;
	
	private String codigoClasificador;
	private String codigoOficinaDestino;
	private String codigoTipoDestino;
	private String codigoTipoDocumento;
	private String codigoOficnaOrigen;
	private String codigoTipoOrigen;
	private String codigoTramite;
	private String codigoEstado;
	
	private Date fechaRegistroSistraDate;
	
	public RegistroSISTRA() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistroSISTRA(String numeroSistra, String numeroDocumento,
			String fechaRegistroSistra, String usuarioRegistro,
			String descripcionTipoOrigen, String descripcionTramite,
			String nroFolios, String observacion, String asunto,
			String clasificador, String nombreDocumento,
			String descripcionTipoDocumento, String oficinaOrigen,
			String remite, String plazoMaximo, String rutaPdf,
			String nombrePdf, String descripcionTipoDestino, String estado,
			String oficinaDestino, String codigoClasificador,
			String codigoOficinaDestino, String codigoTipoDestino,
			String codigoTipoDocumento, String codigoOficnaOrigen,
			String codigoTipoOrigen, String codigoTramite, String codigoEstado) {
		super();
		this.numeroSistra = numeroSistra;
		this.numeroDocumento = numeroDocumento;
		this.fechaRegistroSistra = fechaRegistroSistra;
		this.usuarioRegistro = usuarioRegistro;
		this.descripcionTipoOrigen = descripcionTipoOrigen;
		this.descripcionTramite = descripcionTramite;
		this.nroFolios = nroFolios;
		this.observacion = observacion;
		this.asunto = asunto;
		this.clasificador = clasificador;
		this.nombreDocumento = nombreDocumento;
		this.descripcionTipoDocumento = descripcionTipoDocumento;
		this.oficinaOrigen = oficinaOrigen;
		this.remite = remite;
		this.plazoMaximo = plazoMaximo;
		this.rutaPdf = rutaPdf;
		this.nombrePdf = nombrePdf;
		this.descripcionTipoDestino = descripcionTipoDestino;
		this.estado = estado;
		this.oficinaDestino = oficinaDestino;
		this.codigoClasificador = codigoClasificador;
		this.codigoOficinaDestino = codigoOficinaDestino;
		this.codigoTipoDestino = codigoTipoDestino;
		this.codigoTipoDocumento = codigoTipoDocumento;
		this.codigoOficnaOrigen = codigoOficnaOrigen;
		this.codigoTipoOrigen = codigoTipoOrigen;
		this.codigoTramite = codigoTramite;
		this.codigoEstado = codigoEstado;
	}

	public String getNumeroSistra() {
		return numeroSistra;
	}

	public void setNumeroSistra(String numeroSistra) {
		this.numeroSistra = numeroSistra;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getFechaRegistroSistra() {
		return fechaRegistroSistra;
	}

	public void setFechaRegistroSistra(String fechaRegistroSistra) {
		this.fechaRegistroSistra = fechaRegistroSistra;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getDescripcionTipoOrigen() {
		return descripcionTipoOrigen;
	}

	public void setDescripcionTipoOrigen(String descripcionTipoOrigen) {
		this.descripcionTipoOrigen = descripcionTipoOrigen;
	}

	public String getDescripcionTramite() {
		return descripcionTramite;
	}

	public void setDescripcionTramite(String descripcionTramite) {
		this.descripcionTramite = descripcionTramite;
	}

	public String getNroFolios() {
		return nroFolios;
	}

	public void setNroFolios(String nroFolios) {
		this.nroFolios = nroFolios;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getClasificador() {
		return clasificador;
	}

	public void setClasificador(String clasificador) {
		this.clasificador = clasificador;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	public String getDescripcionTipoDocumento() {
		return descripcionTipoDocumento;
	}

	public void setDescripcionTipoDocumento(String descripcionTipoDocumento) {
		this.descripcionTipoDocumento = descripcionTipoDocumento;
	}

	public String getOficinaOrigen() {
		return oficinaOrigen;
	}

	public void setOficinaOrigen(String oficinaOrigen) {
		this.oficinaOrigen = oficinaOrigen;
	}

	public String getRemite() {
		return remite;
	}

	public void setRemite(String remite) {
		this.remite = remite;
	}

	public String getPlazoMaximo() {
		return plazoMaximo;
	}

	public void setPlazoMaximo(String plazoMaximo) {
		this.plazoMaximo = plazoMaximo;
	}

	public String getRutaPdf() {
		return rutaPdf;
	}

	public void setRutaPdf(String rutaPdf) {
		this.rutaPdf = rutaPdf;
	}

	public String getNombrePdf() {
		return nombrePdf;
	}

	public void setNombrePdf(String nombrePdf) {
		this.nombrePdf = nombrePdf;
	}

	public String getDescripcionTipoDestino() {
		return descripcionTipoDestino;
	}

	public void setDescripcionTipoDestino(String descripcionTipoDestino) {
		this.descripcionTipoDestino = descripcionTipoDestino;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getOficinaDestino() {
		return oficinaDestino;
	}

	public void setOficinaDestino(String oficinaDestino) {
		this.oficinaDestino = oficinaDestino;
	}

	public String getCodigoClasificador() {
		return codigoClasificador;
	}

	public void setCodigoClasificador(String codigoClasificador) {
		this.codigoClasificador = codigoClasificador;
	}

	public String getCodigoOficinaDestino() {
		return codigoOficinaDestino;
	}

	public void setCodigoOficinaDestino(String codigoOficinaDestino) {
		this.codigoOficinaDestino = codigoOficinaDestino;
	}

	public String getCodigoTipoDestino() {
		return codigoTipoDestino;
	}

	public void setCodigoTipoDestino(String codigoTipoDestino) {
		this.codigoTipoDestino = codigoTipoDestino;
	}

	public String getCodigoTipoDocumento() {
		return codigoTipoDocumento;
	}

	public void setCodigoTipoDocumento(String codigoTipoDocumento) {
		this.codigoTipoDocumento = codigoTipoDocumento;
	}

	public String getCodigoOficnaOrigen() {
		return codigoOficnaOrigen;
	}

	public void setCodigoOficnaOrigen(String codigoOficnaOrigen) {
		this.codigoOficnaOrigen = codigoOficnaOrigen;
	}

	public String getCodigoTipoOrigen() {
		return codigoTipoOrigen;
	}

	public void setCodigoTipoOrigen(String codigoTipoOrigen) {
		this.codigoTipoOrigen = codigoTipoOrigen;
	}

	public String getCodigoTramite() {
		return codigoTramite;
	}

	public void setCodigoTramite(String codigoTramite) {
		this.codigoTramite = codigoTramite;
	}

	public String getCodigoEstado() {
		return codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}

	public Date getFechaRegistroSistraDate() {
		return fechaRegistroSistraDate;
	}

	public void setFechaRegistroSistraDate(Date fechaRegistroSistraDate) {
		this.fechaRegistroSistraDate = fechaRegistroSistraDate;
	}
	
	
	
}
