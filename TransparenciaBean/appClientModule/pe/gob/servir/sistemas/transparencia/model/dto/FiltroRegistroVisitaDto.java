package pe.gob.servir.sistemas.transparencia.model.dto;

import java.io.Serializable;
import java.util.Date;

public class FiltroRegistroVisitaDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String numeroDocumentoIdentidad;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private Date fechaVisitaHasta;
	private Date fechaVisitaDesde;
	
	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}
	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public Date getFechaVisitaHasta() {
		return fechaVisitaHasta;
	}
	public void setFechaVisitaHasta(Date fechaVisitaHasta) {
		this.fechaVisitaHasta = fechaVisitaHasta;
	}
	public Date getFechaVisitaDesde() {
		return fechaVisitaDesde;
	}
	public void setFechaVisitaDesde(Date fechaVisitaDesde) {
		this.fechaVisitaDesde = fechaVisitaDesde;
	}
		
	

}
