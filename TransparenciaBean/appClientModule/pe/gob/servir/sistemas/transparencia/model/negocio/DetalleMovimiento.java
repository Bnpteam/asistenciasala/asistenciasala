package pe.gob.servir.sistemas.transparencia.model.negocio;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;

public class DetalleMovimiento extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long 			solicitudMovimientoId;
	private String			t01UndOrgEmiId;
	private String			t01UndOrgEmiDetaCort;
	private String			t01UndOrgEmiDetaLarg;
	private String 		  	tipoDocumentoId;
	private FileAlfresco  	fileAlfresco;
	private String 			nombreDocumento;
	private String 			uuidDocumento;

	public DetalleMovimiento() {
		super();
		this.setSolicitudMovimientoId(0L);
		this.setTipoDocumentoId("");
		this.setFileAlfresco(null);
		this.setNombreDocumento("");
		this.setUuidDocumento("");
	}

	public Long getSolicitudMovimientoId() {
		return solicitudMovimientoId;
	}

	public void setSolicitudMovimientoId(Long solicitudMovimientoId) {
		this.solicitudMovimientoId = solicitudMovimientoId;
	}

	public String getT01UndOrgEmiId() {
		return t01UndOrgEmiId;
	}

	public void setT01UndOrgEmiId(String t01UndOrgEmiId) {
		this.t01UndOrgEmiId = t01UndOrgEmiId;
	}
	
	public String getT01UndOrgEmiDetaCort() {
		return t01UndOrgEmiDetaCort;
	}

	public void setT01UndOrgEmiDetaCort(String t01UndOrgEmiDetaCort) {
		this.t01UndOrgEmiDetaCort = t01UndOrgEmiDetaCort;
	}

	public String getT01UndOrgEmiDetaLarg() {
		return t01UndOrgEmiDetaLarg;
	}

	public void setT01UndOrgEmiDetaLarg(String t01UndOrgEmiDetaLarg) {
		this.t01UndOrgEmiDetaLarg = t01UndOrgEmiDetaLarg;
	}

	public String getTipoDocumentoId() {
		return tipoDocumentoId;
	}

	public void setTipoDocumentoId(String tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	public FileAlfresco getFileAlfresco() {
		return fileAlfresco;
	}

	public void setFileAlfresco(FileAlfresco fileAlfresco) {
		this.fileAlfresco = fileAlfresco;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	public String getUuidDocumento() {
		return uuidDocumento;
	}

	public void setUuidDocumento(String uuidDocumento) {
		this.uuidDocumento = uuidDocumento;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
