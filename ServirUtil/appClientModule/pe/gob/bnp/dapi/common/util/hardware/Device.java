package pe.gob.bnp.dapi.common.util.hardware;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public abstract class Device {
	
	public static String getIpAddress() {
		InetAddress ip;
		String ipAddress = "";
		try {
			ip = InetAddress.getLocalHost();
			ipAddress = ip.getHostAddress();
		} catch ( UnknownHostException e) {
			e.printStackTrace();
		}	
		return ipAddress;
	}
	public static String getMacAddress() {
		InetAddress ip;
		String macAddress = "";
		try {
			ip = InetAddress.getLocalHost();
			System.out.println("Current IP address : " + ip.getHostAddress());
			
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			
			byte[] mac = network.getHardwareAddress();
				
			System.out.print("Current MAC address : ");
				
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));		
			}	
			macAddress = sb.toString();
		} catch (SocketException  e) {
			e.printStackTrace();
		} catch ( UnknownHostException e) {
			e.printStackTrace();
		}	
		return macAddress;
	}

}
