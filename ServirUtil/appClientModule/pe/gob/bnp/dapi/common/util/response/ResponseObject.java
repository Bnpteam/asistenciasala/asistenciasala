package pe.gob.bnp.dapi.common.util.response;

import java.io.Serializable;

public class ResponseObject implements Serializable {
	private static final long serialVersionUID = 1L;

	private Boolean resultado;
	private String mensaje;
	private Long id;
	private String detalleMensaje;

	public ResponseObject() {
		super();
	}

	public Boolean getResultado() {
		return resultado;
	}

	public void setResultado(Boolean resultado) {
		this.resultado = resultado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDetalleMensaje() {
		return detalleMensaje;
	}

	public void setDetalleMensaje(String detalleMensaje) {
		this.detalleMensaje = detalleMensaje;
	}
	
	

}
