package pe.gob.servir.systems.util.constante;

public interface Constantes {

	/*public static final String USR_ADMIN = "admin";

	public static final String RUTA_CONFIG_PROPERTIES = "config";*/
	
	// PARAMETROS DE CONEXION AL SERVICIO RENIEC
/*	public static final String reniecws_usuario = message.getMessage("reniec.ws.usuario");
	public static final String reniecws_codigo = message.getMessage("reniec.ws.codigo");
	public static final String reniecws_codTxEmp = message.getMessage("reniec.ws.codTxEmp");
	public static final String reniecws_dniUserEmp = message.getMessage("reniec.ws.dniUserEmp");
	public static final String reniecws_baseURL = message.getMessage("reniec.wsbaseURL");*/
	
	// PARAMETROS DE CONEXION AL SERVICIO ALFRESCO
	/*
	public static final String RUTA_GENERAL_ALFRESCO="/GG/PROCURADURIA/";
	public static final String RUTA_ESCRITOS_ALFRESCO="ESC";
	public static final String RUTA_INFORMES_ALFRESCO="INF";
	public static final String RUTA_MEMOS_ALFRESCO="MEM";
	public static final String RUTA_RESOLUCIONES_ALFRESCO="RES";
	public static final String RUTA_CEDULAS_ALFRESCO="CED";
	public static final String RUTA_OFICIO_ALFRESCO="OFIC";*/
	
	//public static final String ALFWS_CODAPLICATIVO = "00001";
/*	public static final String ALFWS_USUARIO = message.getMessage("alfresco.ws.usuario");
	public static final String ALFWS_PASSWORD = message.getMessage("alfresco.ws.passsword");
	public static final String ALFWS_WSDL = message.getMessage("alfresco.ws.wsdl");
	public static final String ALFWS_ENDPOINT = message.getMessage("alfresco.ws.endPoint");
	public static final String ALFWS_URI = message.getMessage("alfresco.ws.uri");
	public static final String ALFWS_QNAME = "AlfrescoService";*/
	
	//PARAMETROS DE CONEXION AL SERVIDOR SMTP
	//public static final String smtp_accountMail = "dvera@servir.gob.pe";
	//public static final String smtp_accountPass = "servir2015";
	//public static final String smtp_personalName = "Consultas Externas - Externas";
	//public static final String smtp_host = "smtp.gmail.com";
	//public static final String smtp_port = "587";
	//public static final String smtp_auth = "true";
	//public static final String smtp_enable = "true";
	
	// PARAMETROS DE CONEXION AL SERVICIO STD	
	//public static final String STDWS_CODAPLICATIVO = "00001";  //codifo asignado a procuraduria
	//public static final String STDWS_WSDL = "http://172.16.19.124:8080/STD_WS1/STDServiceInterface?wsdl"; //debe ir con el FacesContext std.ws.wsdl
	//public static final String STDWS_ENDPOINT = "http://172.16.19.124:8080/STD_WS1/STDServiceInterface"; //debe ir con el FacesContext std.ws.endPoint
	//public static final String STDWS_URI = "ws.service.std.servir.gob.pe"; //debe ir con el FacesContext std.ws.uri
	//public static final String STDWS_QNAME = "STDWebService";
	
	/*public static final String STD										= "STD";
	public static final String GPGSC									= "GPGSC";*/
	
	public static final String OPERACION_EXITOSA_ALFRESCO				= "00000";
	public static final String OPERACION_EXITOSA_RENIEC				= "0000";
	
	public static final String SOLICITUD_FISICAS				="F";
	public static final String SOLICITUD_VIRTUAL				="V";
	
	/*// SIGLAS TIPO DE DOCUMENTO SUBIDO A ALFRESCO
	public static final String SIGLA_INFORME_TECNICO					= "IT";
	public static final String SIGLA_INFORME_RESPUESTA					= "IR";
	
	// ECE : ESTADO CONSULTA ESCRITA
	public static final String ECE_REGISTRADO_DE_STD_POR_REC_RESP		= "REGS_STD_REC_RES_E";
	public static final String ECE_DERIVADO_A_PRI_REV_POR_REC_RES		= "DERV_PRI_REV_X_REC_RES_E";
	public static final String ECE_DERIVADO_A_ULT_REV					= "DERV_ULT_REV_E";
	public static final String ECE_DERIVADO_A_PRI_REV_POR_ULT_REV		= "DERV_PRI_REV_X_ULT_REV_E";
	public static final String ECE_DERIVADO_A_ATENCION  				= "DERV_ATE_E";
	public static final String ECE_PROYECTADO_A_PRI_REV					= "PROY_PRI_REV_E";
	public static final String ECE_OBSERVADO_PRI_REV					= "OBS_PRI_REV_E";
	public static final String ECE_PROYECTADO_A_ULT_REV					= "PROY_ULT_REV_E";
	public static final String ECE_OBSERVADO_ULT_REV					= "OBS_ULT_REV_E";
	public static final String ECE_PROYECTADO_A_PE 						= "PROY_PE_E";
	public static final String ECF_OBSERVADO_PE 						= "OBS_PE_F";
	public static final String ECE_ENVIADO_A_CIUDADANO 					= "ENV_CIU_E";
	public static final String ECE_NOTIFICADO_CIUDADANO 				= "NOTI_CIU_E";
	public static final String ECE_DEVUELTO_POR_REC_RES					= "DEV_REC_RES_E";
	public static final String ECE_DEVUELTO_POR_ULT_REV					= "DEV_ULT_REV_E";
	public static final String ECE_DEVUELTO_POR_PRI_REV_A_ULT_REV		= "DEV_PRI_REV_A_ULT_REV_E";
	public static final String ECE_DEVUELTO_POR_PRI_REV_A_REC_RES		= "DEV_PRI_REV_A_REC_RES_E";
	public static final String ECE_DEVUELTO_POR_ATENCION				= "DEV_ATE_E";
	public static final String ECE_FINALIZADO_PRI_REV					= "FIN_PRI_REV_E";
	
	public static final String ECE_REGISTRADO_DE_GPGSC_POR_REC_RESP		= "REGS_GPGSC_REC_RES_E";
	
	
	//PARAMETROS BANDEJAS APLICATIVO EXTERNO VIRTUAL	
	public static final String BANDEJA_ATENCION_GENERAL_VIRTUAL 	= "SISINT_BANDEJA_ATENCION_GENERAL_V";	
	public static final	String BANDEJA_PRIMER_REVISOR_VIRTUAL 		= "SISINT_BANDEJA_PRIM_REVIS_V";
	
	//public static final String BANDEJA_ATENCION_NLSC_VIRTUAL 		= "SISINT_BANDEJA_ATENCION_NLSC_V";
	//public static final String BANDEJA_ATENCION_LSC_VIRTUAL 		= "SISINT_BANDEJA_ATENCION_LSC_V";

	
	
	//PARAMETROS ESTADOS APLICATIVO EXTERNO VIRTUAL
	public static final String ECV_RECEPCIONADO_POR_ATENCION_GENERAL		= "RECP_ATE_GEN_V";
	public static final String ECV_NOTIFICADO_CIUDADANO						= "NOTI_CIU_V";
	public static final String ECV_OBSERVADO_A_ATENCION_GENERAL				= "OBS_ATE_GEN_V";
	public static final String ECV_PROYECTADO_A_REVISOR_ATENCION			= "PROY_ULT_REV_ATE_V";
	
	//public static final String ECV_RECEPCIONADO_POR_ATENCION_REVISOR		= "RECP_ATE_REV_V";
	//public static final String ECV_RECEPCIONADO_POR_PRIMER_REVISOR			= "RECP_PRI_REV_V";
	//public static final String ECV_OBSERVADO_A_PRIMER_REVISOR				= "OBS_PRI_REV_V";

	
	//PARAMETROS GENERALES
	public static final String ECV_NUMERO_CARACTERES_CONSULTA_VIRTUAL		= "NUMCAR";
	
	//ID USUARIO REMITENTE CIUDADANO
	public static final String CIUDADANO_ID									= "-1";
	
	//PARAMETROS TIPOS DE EXPORTACION
	public static final String ECV_IMPORTACION_HISTORICO				= "1";
	public static final String ECV_IMPORTACION_BANDEJA_SECRETARIA		= "2";
	public static final String ECV_IMPORTACION_BANDEJA_COLABORADOR		= "3";*/
		
	
	//PLANTILLA DE CORREOS
	/*public static final String PL_EMAIL_REGISTRO_CONSULTA			= "<html>"
			+ "<body>"
			+ "<table align='center' bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' style='font-family:Arial,Helvetica,sans-serif;font-size:12px;color:rgb(51,51,51)' width='700'>"
			+ "<tbody><tr><td bgcolor='#b80400' height='40' style='border-top-width:1px;border-top-style:solid;border-top-color:rgb(233,55,55);padding:2px 15px;background:linear-gradient(rgb(187,4,0) 0%,rgb(155,5,1) 100%)' valign='middle'><img align='left' border='0' class='CToWUd' src='https://ci3.googleusercontent.com/proxy/tMxMGKbp2lOrMaG7l0ViSW6ARUSoOTLhMLcezdTiRSdkse8F2zHnqRv6aAQ1IwbhRdYtwJYyl11CSEpPtBRH76Ymc4dvwDEUXEGNHYn6MebzgOyGzbUyRQCzBEXrjrt43jNQzgojza_lvRci0d1Hl74fbA=s0-d-e1-ft#http://servir.azurewebsites.net/wp-content/themes/Shigo%20Servir%20Theme%20v1/images/logo.png' /><br /><br /></td></tr>"
			+ "<tr><td style='text-align:right;color:rgb(209,207,207);line-height:15px;padding-right:15px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;display:block' bgcolor='#000000'>GPGSC </td></tr>"
			+ "</tbody></table><br />"
			+ "<table align='center' width='700'>"
			+ "<tr><td align='center'>"
			+ "<table align='center'>"
			+ "<tr><th style='text-align:right'>&nbsp;</th><td style='text-align:left'> <CUERPO>  </td></tr><br />"
			+ "<tr><td style='text-align:right'>&nbsp;</td><td style='text-align:left'><b>Gerencia de Pol�ticas de Gesti�n del Servicio Civil - SERVIR</b></td></tr>"
			+ "</table></td></tr><br />"
			+ "</table><br />"
			+ "<table align='center' border='0' cellpadding='0' cellspacing='0' style='border:1px solid rgb(221,221,221);font-family:Arial,Helvetica,sans-serif;font-size:12px;color:rgb(51,51,51)' width='700'>"
			+ "<tbody><tr><td bgcolor='#b80400' width='100%'>"
			+ "<p style='margin:0px;text-align:center;color:rgb(255,255,255);padding:5px;line-height:14px;font-family:Arial,Helvetica,sans-serif'>Autoridad Nacional del Servicio Civil &ndash; SERVIR<br />Pje. Francisco de Zela 150 piso 10, Jes�s Mar�a<br />Lima - Per�</p></td></tr>"
			+ "</tbody>"
			+ "</table><p>&nbsp;</p></body>"
			+ "</html>";*/
	
		
	/*public static final String RECEPCIONADO_POR_ESPECIALISTA_VIRTUAL 	= "RECP_ESP_V";
	public static final String RECEPCIONADO_POR_ASISTENTE_VIRTUAL		= "RECP_ASI_V";
	public static final String RECEPCIONADO_POR_ANALISTA_VIRTUAL 		= "RECP_ANA_V";
	public static final String PROYECTADO_A_ESPECIALISTA_VIRTUAL 		= "PROY_ESP_V";
	public static final String OBSERVADO_POR_ESPECIALISTA_VIRTUAL		= "OBS_ESP_V";*/
	
	/*//GENERICOS
	public static final String CONSULTA_EXT_ESCRITA = "E";
	public static final String CONSULTA_EXT_VIRTUAL = "V";

	public static final String CONSULTA_ACTIVA = "1";
	public static final String CONSULTA_INACTIVA = "0";*/
	
	
	//PERFILES
	//public static final String PERFIL_ADMINISTRADOR = "ADM";
	//public static final String PERFIL_SECRETARIA = "SEC";
	//public static final String PERFIL_GERENTE = "GER";
	//public static final String PERFIL_ESPECIALISTA = "ESP";
	//public static final String PERFIL_ANALISTA = "ANA";
	//public static final String PERFIL_ASISTENTE = "ASI";

}
