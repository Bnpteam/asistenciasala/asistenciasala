package pe.gob.servir.systems.util.alfresco;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import pe.gob.servir.systems.util.date.DateUtil;

public class CargaMasivaArchivo {

	private List<String> lstCargaInicial;
	private List<String> lstRegistroExitoso;
	private List<String> lstRegistroErroneoUpdateBD;
	private List<String> lstRegistroErroneoAlfresco;
	private List<String> lstRegistroErroneoBusqueda;
	
	private String msgError;
	private boolean swError;
	
	public CargaMasivaArchivo(){
		lstCargaInicial = new ArrayList<String>();
		lstRegistroExitoso = new ArrayList<String>();
		lstRegistroErroneoUpdateBD = new ArrayList<String>();
		lstRegistroErroneoAlfresco = new ArrayList<String>();
		lstRegistroErroneoBusqueda = new ArrayList<String>();
		swError = false;
	}
	
	public String generarReporteCarga(Date horaInicioCarga, boolean swCargaOK){

		String fileName = DateUtil.toNameFileWithDateTime("CargaConsultasPDF.txt");
		StringBuilder sb = new StringBuilder();
		
		
		try {
			File file = new File(this.getString("ruta.documentos.reporte.carga") + fileName);
			
			//File file = new File("D:\\consultas_externas\\adjunto_email\\"+fileName);
			
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			//bw.write(content);
			
			String stHoraInicioCarga = DateUtil.getStringDateTime(horaInicioCarga);
			String stHoraFinCarga = DateUtil.getStringDateTime(new Date());
			
			sb.append("----------------------------------------- CARGA DE ARCHIVOS --------------------------------------------------------\n").
				append("Archivos Cargados al Servidor : ").append(lstCargaInicial.size()).append("\n").
				append("Hora Inicio : ").append(stHoraInicioCarga).append("\n").
				append("Hora Fin : ").append(stHoraFinCarga).append("\n");

			
			if(swCargaOK){
				/*ARCHIVOS CARGADOS CON EXITO*/
				sb.append("\n--------------------------------------------------------------------------------------------------------------------\n");
				sb.append("Archivos Cargados con Exito :").append(lstRegistroExitoso.size()).append("\n");
				sb.append("CONSULTA_ID : CODIGO : NUMERO_REGISTRO : ESTADO : DOCUMENTO_CONSULTA (UIID CONSULTA): NOMBRE_DOCUMENTO_CONSULTA").append("\n");
				
				/*
				bw.write(sb.toString());
				
				for(String registro : lstRegistroExitoso){
					bw.write(registro);
					bw.write(System.lineSeparator());
				}*/
				
				for(String registro : lstRegistroExitoso){
					sb.append(registro).append("\n");
				}
				
				/*ARCHIVOS CON ERROR DE ACTUALIZACION DE BD*/
				sb.append("\n--------------------------------------------------------------------------------------------------------------------\n");
				sb.append("Archivos con error al actualizar en BD:").append(lstRegistroErroneoUpdateBD.size()).append("\n");
				sb.append("CONSULTA_ID : CODIGO : NUMERO_REGISTRO : ESTADO : DOCUMENTO_CONSULTA (UIID CONSULTA) : NOMBRE_DOCUMENTO_CONSULTA").append("\n");
				
				/*
				bw.write(sb.toString());
				
				for(String registro : lstRegistroErroneoUpdateBD){
					bw.write(registro);
					bw.write(System.lineSeparator());
				}
				*/
					for(String registro : lstRegistroErroneoUpdateBD){
						sb.append(registro).append("\n");
					}
				
				/*ARCHIVOS CON PROBLEMAS DE CARGA EN ALFRESCO*/
				sb.append("\n--------------------------------------------------------------------------------------------------------------------\n");
				sb.append("Archivos con problemas de carga en Alfresco:").append(lstRegistroErroneoAlfresco.size()).append("\n");
				sb.append("CONSULTA_ID : CODIGO : NUMERO_REGISTRO : ESTADO : DOCUMENTO_CONSULTA (UIID CONSULTA) : NOMBRE_DOCUMENTO_CONSULTA : CODIGO ALFRESCO : DESCRIPCION ALFRESCO").append("\n");
				/*
				bw.write(sb.toString());
				
				for(String registro : lstRegistroErroneoAlfresco){
					bw.write(registro);
					bw.write(System.lineSeparator());
				}
				*/
				
				for(String registro : lstRegistroErroneoAlfresco){
					sb.append(registro).append("\n");
				}
				
				/*ARCHIVOS CUYO NOMBRE NO FUE ENCONTRADO EN LA BD*/
				sb.append("\n-----------------------------------------------------------------------------------------------------------------\n");
				sb.append("Archivos no encontrados en la BD (o que ya fueron cargados):").append(lstRegistroErroneoBusqueda.size()).append("\n");
				sb.append("NOMBRE_DOCUMENTO_CONSULTA").append("\n");
				/*
				bw.write(sb.toString());
				
				for(String registro : lstRegistroErroneoBusqueda){
					bw.write(registro);
					bw.write(System.lineSeparator());
				}
				*/
				for(String registro : lstRegistroErroneoBusqueda){
					sb.append(registro).append("\n");
				}
				
			} else {
				//bw.write(this.getMsgError());
				sb.append(this.getMsgError()).append("\n");
			}

			
	/*		
			sbRegistroErroneoUpdateBD.append(tmpConsultaBD.getId()).append(" : ").append(tmpConsultaBD.getNumRegistro()).append(" : ").
			append(tmpConsultaBD.getEstado()).append(" : ").append(tmpConsultaBD.getDocConsulta()).append(" : ").
			append(tmpConsultaBD.getNombreDocumentoConsulta());
			
			for (String mensaje : cadena) {
				bw.write(mensaje);
				bw.write(System.lineSeparator());
			}
	*/		
			bw.write(sb.toString());
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return fileName;
	}

	public List<String> getLstCargaInicial() {
		return lstCargaInicial;
	}

	public void setLstCargaInicial(List<String> lstCargaInicial) {
		this.lstCargaInicial = lstCargaInicial;
	}

	public List<String> getLstRegistroExitoso() {
		return lstRegistroExitoso;
	}

	public void setLstRegistroExitoso(List<String> lstRegistroExitoso) {
		this.lstRegistroExitoso = lstRegistroExitoso;
	}

	public List<String> getLstRegistroErroneoUpdateBD() {
		return lstRegistroErroneoUpdateBD;
	}

	public void setLstRegistroErroneoUpdateBD(
			List<String> lstRegistroErroneoUpdateBD) {
		this.lstRegistroErroneoUpdateBD = lstRegistroErroneoUpdateBD;
	}

	public List<String> getLstRegistroErroneoAlfresco() {
		return lstRegistroErroneoAlfresco;
	}

	public void setLstRegistroErroneoAlfresco(
			List<String> lstRegistroErroneoAlfresco) {
		this.lstRegistroErroneoAlfresco = lstRegistroErroneoAlfresco;
	}

	public List<String> getLstRegistroErroneoBusqueda() {
		return lstRegistroErroneoBusqueda;
	}

	public void setLstRegistroErroneoBusqueda(
			List<String> lstRegistroErroneoBusqueda) {
		this.lstRegistroErroneoBusqueda = lstRegistroErroneoBusqueda;
	}

	public String getMsgError() {
		return msgError;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	public boolean isSwError() {
		return swError;
	}

	public void setSwError(boolean swError) {
		this.swError = swError;
	}
	
	private String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream("config.properties"));
			return properties.getProperty(key); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "";
	}
}
