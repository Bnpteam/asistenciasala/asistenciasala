package pe.gob.servir.systems.util.alfresco;

public class ResultAlfresco {

	private String uiid;
	private String codigo;
	private String descripcion;
	
	private byte[] rawFile;		// Al usar la descarga con ruta
	
	public ResultAlfresco(){
		this.setCodigo("");
		this.setUiid("");
		this.setDescripcion("");
	}
	
	public String getUiid() {
		return uiid;
	}
	
	public void setUiid(String uiid) {
		this.uiid = uiid;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public byte[] getRawFile() {
		return rawFile;
	}

	public void setRawFile(byte[] rawFile) {
		this.rawFile = rawFile;
	}
	
}
