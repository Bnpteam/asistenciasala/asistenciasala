package pe.gob.servir.systems.util.enums;

public class Enums {
	public enum TemplateMail{
		ADJUNTO_PDF_REGISTRO_SISTRA("/pe/gob/servir/sistemas/transparenciaexterno/templates/adjuntoPDFRegistroSISTRA.template"),
		CORREO_NOTIFICACION_CIUDADANO("/pe/gob/servir/sistemas/transparenciaexterno/templates/notificacionCiudadano.template"),
		CORREO_CREACION_USUARIO_GENERAL("/pe/gob/servir/sistemas/transparenciaexterno/templates/correoCreacionUsuarioGeneral.template"),
		CORREO_CREACION_USUARIO_INVESTIGADOR("/pe/gob/servir/sistemas/transparenciaexterno/templates/correoCreacionUsuarioInvestigador.template");
		
		String rutaTemplate;
		
		private TemplateMail(String ruta){
			this.rutaTemplate = ruta;
		}
		
		public String getRuta(){
			return rutaTemplate;
		}
	}
	
	
	public enum PerfilInterno{
		
		SECRETARIA_UO("4");
		
		String id;
		
		private PerfilInterno(String id){
			this.id = id;
		}
		
		public Long getId(){
			return Long.parseLong(id);
		}
	}
}
