package pe.gob.servir.systems.util.converter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import pe.gob.servir.systems.util.validator.VO;

public class Convert {
	
	public static Date toDate(String obj) 
	{
		Date parsedDate;
		if (!(VO.isEmpty(obj))) 
		{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			try 
			{
				parsedDate = new Date(sdf.parse(obj).getTime());
			} catch (Exception e) 
			{
				e.printStackTrace();
				parsedDate = null;
			}
		}else {
			parsedDate = null;
		}
		return parsedDate;
	}
	
	public static String toTitleCase(String prmString) {
		
		if (prmString.isEmpty()) {
		    return prmString;            
		} else {
			String result = "";
			prmString = prmString.trim().toLowerCase();
			String []  var = prmString.split(" ");
			
			for (String foobar : var ) {
				if (VO.isEmpty(result)) {
					result = Character.toUpperCase(foobar.charAt(0)) + foobar.substring(1);
				} else {
					result = result + " " + Character.toUpperCase(foobar.charAt(0)) + foobar.substring(1);
				}
			}
			return result;
		}
	}
	
	public static String toString(Date prmDate)
	{
	   //System.out.println("Date ToString(dd/MM/yyyy):prmDate:"+prmDate);
	   String parsedDateString = null;
	   if (!VO.isEmpty(prmDate)) {
		   SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		   /*you can also use DateFormat reference instead of SimpleDateFormat 
		    * like this: DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
		    */
		   try{
			parsedDateString = sdf.format( prmDate );
		   }catch (Exception ex ){
			ex.printStackTrace();
			parsedDateString = null;
		   }
	   } 
	   else 
	   {
		parsedDateString = null;	
	   }
	   
	   return parsedDateString;
	}
	
	public static String toString(String obj) {
		
		if (obj==null) {
			return "";
		}
		
		return obj.toString().trim();
	}
	
	public static java.sql.Date toSqlDate(java.util.Date prmDate)
	{
		java.sql.Date returnDate = null;
	   String parsedDateString = null;
	   SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	   /*you can also use DateFormat reference instead of SimpleDateFormat 
	    * like this: DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
	    */
	   try{
		parsedDateString = sdf.format( prmDate );
		returnDate = java.sql.Date.valueOf( parsedDateString );
	   }catch (Exception ex ){
		ex.printStackTrace();
		parsedDateString = null;
	   }
	   
	   return returnDate;
	}
	
	public static Timestamp toTimestamp(String prmStrDate) 
	{
		if (!VO.isEmpty(prmStrDate)) 
		{
			try 
			{
				Date tmpDate = toDate(prmStrDate);
				
				Timestamp timestamp = new Timestamp(tmpDate.getTime());
				return timestamp;
			} 
			catch (Exception e) 
			{
				System.out.println("Convert.toTimestamp:"+prmStrDate+":Error");
				e.printStackTrace();
				return null;
			}
		}
		else 
		{
			return null;
		}
		
	}
	
	public static int toInt(Object prmObj) 
	{
		int rsInt = 0;		
		if (!VO.isEmpty(prmObj)) {
			try {
				rsInt = Integer.parseInt(prmObj.toString());
			} catch (Exception e) {
				e.printStackTrace();
				rsInt = 0;
			}
		}		
		return rsInt;
	}
		
}
