package pe.gob.servir.systems.util.validator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class VO {

//	private String obtenerStringDePreferencias(List<Preference> preferencias){
//		if (preferencias == null || preferencias.size()==0) return Constants.EMPTY_STRING; 
//		String resultado = "(";
//		for (int i = 0; i < preferencias.size(); i++) {
//			long prefId = preferencias.get(0).getPreferenciaId();
//			if (prefId>0){
//				if (i==preferencias.size()-1){
//					resultado += String.valueOf(prefId)+")";
//				}else{
//					resultado += String.valueOf(prefId)+",";
//				}
//			}
//		}
//		return resultado;
//	}
	
	public static Date sumarFecha(Date fecha, int campo, int valor){
	      if (valor==0) return fecha;
	      Calendar calendar = Calendar.getInstance();
	      calendar.setTime(fecha); 
	      calendar.add(campo, valor); 
	      return calendar.getTime(); 
	}
	
	public static InputStream getInputStream(byte[] byteArray){
		if (byteArray==null) return null;
		return new ByteArrayInputStream(byteArray);		
	}

	//InputStrem to byte[]
	public static byte[] getByteArray(java.io.InputStream inputStream) throws IOException{
		if (inputStream==null) return null;
		byte[] targetArray = new byte[inputStream.available()];
		return targetArray;		
	}
	
  public static byte[] getBytes(InputStream is) throws IOException {
	    int len;
	    int size = 1024;
	    byte[] buf ;
	    if (is == null) return null;
	    if (is instanceof ByteArrayInputStream) {
	      size = is.available();
	      buf = new byte[size];
	      len = is.read(buf, 0, size);
	    } else {
	      ByteArrayOutputStream bos = new ByteArrayOutputStream();
	      buf = new byte[size];
	      while ((len = is.read(buf, 0, size)) != -1)
	        bos.write(buf, 0, len);
	      buf = bos.toByteArray();
	    }
	    return buf;
	  }
	
	
	//byte[] to InputStrem
	public static InputStream byteArrayToInputStream(byte[] byteArray){
		if (byteArray==null) return null;
		return new ByteArrayInputStream(byteArray);		
	}
	
	public static java.sql.Date getSQLDate(java.util.Date utilDate) {
	    if (utilDate==null) return null;
	    return new java.sql.Date(utilDate.getTime());
	}

	public static java.util.Date getUtilDate(java.sql.Date sqlDate) {
	    if (sqlDate==null) return null;
	    return new java.util.Date(sqlDate.getTime());
	}
	
	public static boolean isEmpty(Object obj) {
		if ((obj == null) || (obj.toString().trim().length() == 0)) {
			return true;
		}
		return false;
	}
	
	public static String getStringFormatted(Object obj) {
		return isEmpty(obj)?Constants.EMPTY_STRING:obj.toString().trim().toUpperCase();
	}	

	public static String getString(String obj) {
		if (obj == null) {
			return Constants.EMPTY_STRING;
		} else {
			return obj.trim();
		}
	}
	
	public static String getString(String valor, int longitud){
		String resultado = Constants.EMPTY_STRING;
		String cadena = VO.getString(valor);
		if (cadena.length()>longitud){
			resultado = cadena.substring(0, longitud);
		}else{
			resultado = cadena;
		}
		return resultado;
	}

	public static String getStringUpper(String valor, int longitud){
		String resultado = Constants.EMPTY_STRING;
		String cadena = VO.getString(valor);
		if (cadena.length()>longitud){
			resultado = cadena.substring(0, longitud);
		}else{
			resultado = cadena;
		}
		return resultado.toUpperCase();
	}	
	
	public static Double getDouble(Object obj) {
		if (obj == null) {
			return Constants.ZERO_DOUBLE;
		} else {
			return (Double)obj;
		}
	}	
	
	public static String getDateFormatted(Object obj) {
		return isEmpty(obj)?Constants.EMPTY_STRING:obj.toString().trim();
	}	
	
	public static boolean isNull(Object obj) {
		if ((obj == null)) {
			return true;
		}
		return false;
	}

	public static boolean isEmptyList(List<?> obj) {
		if ((obj == null) || (obj.size() == 0)) {
			return true;
		}
		return false;
	}

	public static boolean isObjectsEquals(Object obj, Object object) {
		if (obj.equals(object)) {
			return true;
		}
		return false;
	}

	public static boolean isEquals(String obj, String object) {
		if (obj.equals(object)) {
			return true;
		}
		return false;
	}

	public static String getLike(String obj, Boolean sw) {

		if (sw) {
			return obj;
		} else {
			return "%" + obj + "%";
		}
	}

	public static String getVoid(String obj) {

		if (obj == null) {
			return "";
		} else {
			return obj;
		}
	}


	public static String getEmpty(String obj) {

		if (obj == null) {
			return "";
		} else {
			return obj;
		}
	}



	public static Integer getInteger(Integer obj) {
		if (obj == null) {
			return 0;
		} else {
			return obj;
		}
	}

	// public static void setCodigoFull(SystemObject so) {
	//
	// if(so==null){
	// so= new SystemObject();
	// }
	//
	// if(so.getEO()==null){
	// so.setEO(new SystemEO());
	// }
	//
	// if (so.getEO().getORG()==null){
	// so.getEO().setORG("");
	// }
	//
	// if (so.getEO().getEMP()==null){
	// so.getEO().setEMP("");
	// }
	//
	// if (so.getEO().getSED()==null){
	// so.getEO().setSED("");
	// }
	//
	// if (so.getCodigo()==null){
	// so.setCodigo("");
	// }
	//
	// if (so.getVersion()==null){
	// so.setVersion("");
	// }
	//
	//
	// }

	// public static void setCodigoFullProc(ProcessObject so) {
	//
	// if(so==null){
	// so= new ProcessObject();
	// }
	//
	// if(so.getEO()==null){
	// so.setEO(new SystemEO());
	// }
	//
	// if (so.getEO().getORG()==null){
	// so.getEO().setORG("");
	// }
	//
	// if (so.getEO().getEMP()==null){
	// so.getEO().setEMP("");
	// }
	//
	// if (so.getEO().getSED()==null){
	// so.getEO().setSED("");
	// }
	//
	// if (so.getPeriodo()==null){
	// so.setPeriodo("");
	// }
	//
	// if (so.getCodigo()==null){
	// so.setCodigo("");
	// }
	//
	// if (so.getVersion()==null){
	// so.setVersion("");
	// }
	// }

	// public static void setVoidCodReg(Tabla tabla) {
	//
	// if(tabla==null){
	// tabla= new Tabla();
	// tabla.setCodReg("");
	// }else if (tabla.getCodReg()==null){
	// tabla.setCodReg("");
	// }
	// }
	
	public static String encodeURL(String input) {
		 StringBuilder resultStr = new StringBuilder();
	     for (char ch : input.toCharArray()) {
	    	 if (isUnsafe(ch)) {
	    		 resultStr.append('%');
	             resultStr.append(toHex(ch / 16));
	             resultStr.append(toHex(ch % 16));
	         } else {
	        	 resultStr.append(ch);
	         }
	     }
	     return resultStr.toString();
	}
	
	private static boolean isUnsafe(char ch) {
		if (ch > 128 || ch < 0)
			return true;
		return " %$&+,/:;=?@<>#%".indexOf(ch) >= 0;
	}
	
	private static char toHex(int ch) {
		return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
	}
	
	public static String getParseLike(Object obj, Boolean sw) {
		return isEmpty(obj) ? "%" : getLike(obj.toString(), sw);
	}

	public static Boolean oldLike(String val) {
		if (!isEmpty(val)) {
			return val.equals("%") || val.equals("%%") || val.equals("%%%");
		}
		return false;
	}
	
	/********************************************************************************************
	 * Verification Methods
	 ********************************************************************************************/
	public static boolean validateEmail(String email) {
		final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+"[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"; 
        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
 
        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
 
    }
	
	public static String convertNameFile(String fileNameComplete){
		StringBuilder fileNameFull = new StringBuilder();
		
		String fileName = fileNameComplete.substring(0, fileNameComplete.lastIndexOf("."));
		String extension = fileNameComplete.substring(fileNameComplete.lastIndexOf(".") + 1);
		
		Date fechaHora = new Date();
        
		SimpleDateFormat formatDia = new SimpleDateFormat("dd");
        SimpleDateFormat formatMes = new SimpleDateFormat("MM");
        SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
		
        String dia = formatDia.format(fechaHora);
        String mes = formatMes.format(fechaHora).toUpperCase();
        String anio = formatAnio.format(fechaHora);
        
        SimpleDateFormat formatHora = new SimpleDateFormat("HH");
        SimpleDateFormat formatMin = new SimpleDateFormat("mm");
        SimpleDateFormat formatSeg = new SimpleDateFormat("ss");
        
        String hh = formatHora.format(fechaHora);
        String mm = formatMin.format(fechaHora);
        String ss = formatSeg.format(fechaHora);
        
        fileNameFull.append(fileName);
        
        fileNameFull.append("_");
        fileNameFull.append(dia);
        fileNameFull.append("-");
        fileNameFull.append(mes);
        fileNameFull.append("-");
        fileNameFull.append(anio);
        
        fileNameFull.append("_");
        fileNameFull.append(hh);
        fileNameFull.append("-");
        fileNameFull.append(mm);
        fileNameFull.append("-");
        fileNameFull.append(ss);
        fileNameFull.append(".");
        fileNameFull.append(extension);
		
		return fileNameFull.toString();
	}
	
}
