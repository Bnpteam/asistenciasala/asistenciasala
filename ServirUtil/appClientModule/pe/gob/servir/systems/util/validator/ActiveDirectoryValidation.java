package pe.gob.servir.systems.util.validator;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class ActiveDirectoryValidation {
	
    public boolean confirmUser(String user) throws NamingException {
        
        boolean result = false;
        
    	//final String ldapAdServer = ;
    	final String ldapSearchBase = "dc=ansc,dc=gob,dc=pe";
        
    	final String ldapUsername = "administrador@ansc.gob.pe";
    	final String ldapPassword = "ANSC$3rv1r..3370";
        
    	final String ldapAccountToLookup = user;
        
        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.SECURITY_AUTHENTICATION, this.getString("LDAP.SECURITY_AUTHENTICATION"));
        if(ldapUsername != null) {
            env.put(Context.SECURITY_PRINCIPAL, ldapUsername);
        }
        if(ldapPassword != null) {
            env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
        }
        env.put(Context.INITIAL_CONTEXT_FACTORY, this.getString("LDAP.INITIAL_CONTEXT_FACTORY"));
        env.put(Context.PROVIDER_URL, this.getString("LDAP.PROVIDER_URL"));

        //ensures that objectSID attribute values
        //will be returned as a byte[] instead of a String
        env.put("java.naming.ldap.attributes.binary", "objectSID");
        
        // the following is helpful in debugging errors
        //env.put("com.sun.jndi.ldap.trace.ber", System.err);
        
        DirContext ctx = new InitialDirContext(env);
        
        //LDAPTest ldap = new LDAPTest();
        
        //1) lookup the ldap account
 //       SearchResult srLdapUser = ldap.findAccountByAccountName(ctx, ldapSearchBase, ldapAccountToLookup);
        
/*        
        String nameUserServir = srLdapUser.getAttributes().get("cn").toString();
        String mailUserServir = srLdapUser.getAttributes().get("mail").toString();
        String companyUserServir = srLdapUser.getAttributes().get("company").toString();
        String accountUserServir = srLdapUser.getAttributes().get("samaccountname").toString();
        
        System.out.println(nameUserServir);
        System.out.println(mailUserServir);
        System.out.println(companyUserServir);
        System.out.println(accountUserServir);
   */     
        //System.out.println(srLdapUser);
        //2) get the SID of the users primary group
        //String primaryGroupSID = ldap.getPrimaryGroupSID(srLdapUser);
        
        //3) get the users Primary Group
        //String primaryGroupName = ldap.findGroupBySID(ctx, ldapSearchBase, primaryGroupSID);
        
        
        String searchFilter = "(&(objectClass=user)(sAMAccountName=" + ldapAccountToLookup + "))";

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);

        SearchResult searchResult = null;
        if(results.hasMoreElements()) {
             searchResult = (SearchResult) results.nextElement();

            //make sure there is not another item available, there should be only 1 match
            if(results.hasMoreElements()) {
                System.err.println("Matched multiple users for the accountName: " + ldapAccountToLookup);
                searchResult = null;
            }
        }
        
        if(!VO.isEmpty(searchResult)){
    		result = true;
    	}
        
        return result;
    }
    
    public ActiveDirectoryObject obtenerUsuarioAD(String ldapAccountToLookup) throws NamingException{
    	
    	ActiveDirectoryObject ado = new ActiveDirectoryObject();
    	
        //final String ldapAdServer = "ldap://172.16.19.16:389";
        //final String ldapSearchBase = "dc=ansc,dc=gob,dc=pe";
        
        //final String ldapUsername = "administrador@ansc.gob.pe";
        //final String ldapPassword = "ANSC$3rv1r..3370";
        
        
        //final String ldapAdServer = "ldap://172.16.19.93:389";
        final String ldapAdServer = this.getString("LDAP.PROVIDER_URL");
        
        //final String ldapSearchBase = "dc=testdom,dc=pruebas,dc=com";
        final String ldapSearchBase = this.getString("LDAP.SEARCH_BASE");
        
        //final String ldapUsername = "administrador@testdom.pruebas.com";
        final String ldapUsername = this.getString("LDAP.ADMIN_USER");
        
        //final String ldapPassword = "$3rv1r2017";
        final String ldapPassword = this.getString("LDAP.ADMIN_PASSWORD");
        
        //final String ldapAccountToLookup = "ecastro";
        
        
        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        if(ldapUsername != null) {
            env.put(Context.SECURITY_PRINCIPAL, ldapUsername);
        }
        if(ldapPassword != null) {
            env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
        }
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapAdServer);

        //ensures that objectSID attribute values
        //will be returned as a byte[] instead of a String
        env.put("java.naming.ldap.attributes.binary", "objectSID");
        
        // the following is helpful in debugging errors
        //env.put("com.sun.jndi.ldap.trace.ber", System.err);
        
        DirContext ctx = new InitialDirContext(env);
        System.out.println(env);
        
        //LDAPTest ldap = new LDAPTest();
        
        //1) lookup the ldap account
        SearchResult srLdapUser = this.findAccountByAccountName(ctx, ldapSearchBase, ldapAccountToLookup);
        
        String nameUserServir = null;
        String mailUserServir = null;
        String accountUserServir = null;
        
        if(!VO.isEmpty(srLdapUser)){
	        nameUserServir =  VO.isEmpty(srLdapUser.getAttributes().get("cn")) ? null : srLdapUser.getAttributes().get("cn").toString();
	        mailUserServir = VO.isEmpty(srLdapUser.getAttributes().get("mail")) ? null : srLdapUser.getAttributes().get("mail").toString();
	        //String companyUserServir = srLdapUser.getAttributes().get("company").toString();
	        accountUserServir = VO.isEmpty(srLdapUser.getAttributes().get("samaccountname")) ? null : srLdapUser.getAttributes().get("samaccountname").toString();
        }
        
        if(!VO.isEmpty(accountUserServir)){
        	ado.setUsuario(accountUserServir.substring(accountUserServir.lastIndexOf(":") + 2));
        }
        
        if(!VO.isEmpty(mailUserServir)){
        	ado.setCorreo(mailUserServir.substring(mailUserServir.lastIndexOf(":") + 2));
        }
        
        if(!VO.isEmpty(nameUserServir)){
        	ado.setNombres(nameUserServir.substring(nameUserServir.lastIndexOf(":") + 2));
        }
        
        //ado.setUsuario(accountUserServir.substring(accountUserServir.lastIndexOf(":") + 2));
        //ado.setNombres(nameUserServir.substring(nameUserServir.lastIndexOf(":") + 2));
        //ado.setCorreo(mailUserServir.substring(mailUserServir.lastIndexOf(":") + 2));
        
        //System.out.println(nameUserServir);
        //System.out.println(mailUserServir);
        //.out.println(companyUserServir);
        //System.out.println(accountUserServir);
    	
    	return ado;
    }
    
    public SearchResult findAccountByAccountName(DirContext ctx, String ldapSearchBase, String accountName) throws NamingException {

        String searchFilter = "(&(objectClass=user)(sAMAccountName=" + accountName + "))";

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);

        SearchResult searchResult = null;
        if(results.hasMoreElements()) {
             searchResult = (SearchResult) results.nextElement();

            //make sure there is not another item available, there should be only 1 match
            if(results.hasMoreElements()) {
                System.err.println("Matched multiple users for the accountName: " + accountName);
                return null;
            }
        }
        
        return searchResult;
    }
    
	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream("config.properties"));
			return properties.getProperty(key); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
}
