package pe.gob.servir.systems.util.ipaddress;

import java.io.FileInputStream;
import java.io.InputStream;

public class IpAddressPc {

	public String[] obtenerAuditoria() {

		java.util.Properties dbProps;
		InputStream inputStream = null;
		String archivo = "C:\\Windows\\System32\\Drivers\\etc\\config.properties";

		String mac = "";
		String ip = "";

		String[] auditoria = new String[2];

		try {

			inputStream = new FileInputStream(archivo);
			dbProps = new java.util.Properties();
			dbProps.load(inputStream);
			inputStream.close();

			ip = dbProps.getProperty("host.ip");
			mac = dbProps.getProperty("host.mac");

			auditoria[0] = ip;
			auditoria[1] = mac;

		} catch (Exception e) {
			System.out.println("El archivo " + archivo + "no ha sido cargado exitosamente");
			auditoria = null;
		}

		return auditoria;

	}

	public static void main(String[] args) {
		// 

		IpAddressPc file = new IpAddressPc();
		String[] audit = file.obtenerAuditoria();
		System.out.println(audit[0]);
		System.out.println(audit[1]);
	}

}
