package pe.gob.servir.sistemas.transparencia.presentacion.administracion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.AccesoServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.PerfilServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Acceso;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@ManagedBean(name = "accesoMB")
@SessionScoped
public class AccesoMB extends BasicMB {

	private static final long serialVersionUID = 1L;
	
	private final Logger logger = Logger.getLogger(this.getClass());

	private List<Perfil>			lstPerfil;
	
	private List<Acceso>			lstMenus;
	private List<Acceso>			lstSubMenus;
	
	private Long					perfilId;
	
	@EJB(lookup = Constants.RUTA_EJB+"AccesoServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.AccesoServiceRemote")	
	private AccesoServiceRemote accesoServiceRemote;
	
	@EJB(lookup = Constants.RUTA_EJB+"PerfilServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.PerfilServiceRemote")	
	private PerfilServiceRemote perfilServiceRemote;

	public AccesoMB(){
		
	}
	
	@PostConstruct
	public void init(){
		//this.cargarCombos();
	}
	
	public void iniciarPagina(){
		this.setPerfilId(0L);
		this.setLstMenus(null);
		this.setLstSubMenus(null);
		this.cargarCombos();
	}
	
	public String listarMenus() {
		try {
			if (perfilId <= 0) {
				this.setMensajeAlerta("Por favor seleccione un perfil");
				return "acceso";
			} 
			lstMenus= this.getAccesoServiceRemote().buscarMenus(perfilId);
			lstSubMenus = new ArrayList<Acceso>();
		} catch (Exception e) {
			this.setMensajeError("Error al listar Menus");
			logger.error(e);
			e.printStackTrace();
		}
		return "acceso";	
	}
	
	public void listarSubMenus(Acceso acceso) {
		
		try {			
			Long componentePadreId = acceso.getComponente().getId();
			lstSubMenus = this.getAccesoServiceRemote().buscarSubMenus(perfilId, componentePadreId);
		} catch (Exception e) {
			this.setMensajeError("Error al listar Sub Menus");
			logger.error(e);
			e.printStackTrace();
		}
	
	//return "acceso";	
	}
	
	public void verificarMenuGeneral(String prmComponentePadreId) {
		
		try {
			int cantidadSubMenus = this.getLstSubMenus().size();
			int submenusActivos = 0;
			int submenusInactivos = 0;
			
			for (int i = 0; i < this.getLstSubMenus().size(); i++) 
			{
				Acceso tmpSubMenu = this.getLstSubMenus().get(i);
				
				if (tmpSubMenu.getComponente().getPadreId().equals(prmComponentePadreId)) {
					
					if (tmpSubMenu.isAsignado()) {
						submenusActivos+=1;
					}else {
						submenusInactivos+=1;
					}
				}
			}

			if ((submenusActivos > 0)) 
			{
				
				for (int j = 0; j < this.getLstMenus().size(); j++) {
					Acceso tmpMenu = this.getLstMenus().get(j);
					if (tmpMenu.getComponente().getId()==(Long.valueOf(prmComponentePadreId))) {
						this.getLstMenus().get(j).setAsignado(true);
					}
				}
				
			}
			
			if ((submenusInactivos == cantidadSubMenus)) 
			{
				
				for (int j = 0; j < this.getLstMenus().size(); j++) {
					Acceso tmpMenu = this.getLstMenus().get(j);
					if (tmpMenu.getComponente().getId()==(Long.valueOf(prmComponentePadreId))) {
						this.getLstMenus().get(j).setAsignado(false);
					}
				}
				
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			super.setMensajeAlerta("Error al enlazar accesos desde submen� hacia men� general");
		}
		
	}
	
	public void verificarSubMenus(Acceso acceso, long prmComponenteId, boolean asignado) {
		
		this.listarSubMenus(acceso);
		
		try {
			for (int i = 0; i < this.getLstSubMenus().size(); i++) {
				Acceso tmpSubMenu = this.getLstSubMenus().get(i);
				
				if (tmpSubMenu.getComponente().getPadreId().equals(String.valueOf(prmComponenteId))) {
					this.getLstSubMenus().get(i).setAsignado(asignado);
				}
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			super.setMensajeAlerta("Error al enlazar accesos desde men� general hacia submen�s");
		}
	}
	
	public String grabar() {
		boolean sw= false;
		if (!validar()) {
			return "acceso";
		}
		try {
			
			if (lstMenus==null || lstMenus.size()==0) {
				this.setMensajeAviso("No existen men�s a actualizar");
				return "acceso";
			}
			for (Acceso menu : lstMenus) {
				super.setUsuarioModificacion(menu);
				sw=this.getAccesoServiceRemote().actualizar(menu);				
			}
			
			if (sw) {
				
				if (lstSubMenus.size() == 0) {
					if (sw) {
						this.setMensajeAviso("�xito al actualizar los accesos");
					}else{
						this.setMensajeError("Error al actualizar los accesos");
					}
					return "acceso";
				}
				
				for (Acceso subMenu : lstSubMenus) {
					if (subMenu.getId() == 0) {
						subMenu.setPerfil(new Perfil());
						subMenu.getPerfil().setId(perfilId);
						super.setUsuarioRegistro(subMenu);
						ReturnObject ret = this.getAccesoServiceRemote().insertar(subMenu);
						if (ret.getId() > 0) {
							sw=true;
						}else{
							sw=false;
						}
					}else{
						super.setUsuarioModificacion(subMenu);
						sw=this.getAccesoServiceRemote().actualizar(subMenu);	
					}
								
				}
				
				if (sw) {
					this.setMensajeAviso("�xito al actualizar los accesos");
				}else{
					this.setMensajeError("Error al actualizar los accesos");
				}
			}else{
				this.setMensajeError("Error al actualizar los accesos");
				
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			this.setMensajeError("Error al actualizar los accesos");
		}
			
		return "acceso";	
	}
	
	private boolean validar(){
		
		if (this.getPerfilId()==0) {
			super.setMensajeAlerta("Debe seleccionar un perfil");
			return false;
		}
		
		return true;
	}
	
	private void cargarCombos(){
		try {
			this.lstPerfil	= this.getPerfilServiceRemote().listarTodos();
		} catch (ServicioException e) {
			this.setMensajeError("Error al listar Perfiles");
			logger.error(e);
			e.printStackTrace();
		}
	}

	public List<Perfil> getLstPerfil() {
		return lstPerfil;
	}


	public void setLstPerfil(List<Perfil> lstPerfil) {
		this.lstPerfil = lstPerfil;
	}


	public List<Acceso> getLstMenus() {
		return lstMenus;
	}


	public void setLstMenus(List<Acceso> lstMenus) {
		this.lstMenus = lstMenus;
	}


	public List<Acceso> getLstSubMenus() {
		return lstSubMenus;
	}


	public void setLstSubMenus(List<Acceso> lstSubMenus) {
		this.lstSubMenus = lstSubMenus;
	}


	public Long getPerfilId() {
		return perfilId;
	}


	public void setPerfilId(Long perfilId) {
		this.perfilId = perfilId;
	}


	public AccesoServiceRemote getAccesoServiceRemote() {
		return accesoServiceRemote;
	}


	public void setAccesoServiceRemote(AccesoServiceRemote accesoServiceRemote) {
		this.accesoServiceRemote = accesoServiceRemote;
	}


	public PerfilServiceRemote getPerfilServiceRemote() {
		return perfilServiceRemote;
	}


	public void setPerfilServiceRemote(PerfilServiceRemote perfilServiceRemote) {
		this.perfilServiceRemote = perfilServiceRemote;
	}
	
	
}
