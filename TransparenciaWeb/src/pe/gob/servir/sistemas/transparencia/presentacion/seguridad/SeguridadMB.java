package pe.gob.servir.sistemas.transparencia.presentacion.seguridad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote;
import pe.gob.bnp.dapi.salalectura.domain.entity.SesionSalaUsuario;
import pe.gob.bnp.dapi.salalectura.dto.CredencialUsuarioDto;
import pe.gob.bnp.domain.entity.SalaBiblioteca;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.AccesoServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.UsuarioServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.administracion.Acceso;
import pe.gob.servir.sistemas.transparencia.model.administracion.Componente;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "seguridadMB")
@SessionScoped
public class SeguridadMB extends BasicMB {

	private static final Logger logger = Logger.getLogger(SeguridadMB.class);
	
	private static final long serialVersionUID = 1L;

	private Usuario			usuario;
	private String			claveActual;

	private Usuario			usuarioLogin;
	private boolean			swClave;
	
	private List<Acceso>	lstAccesos;
	/*
	private String 			defaultReal="defaultReal";
	private String 			defaultRealHash;
	*/
	private int 			match;
	
	@EJB(lookup = Constants.RUTA_EJB+"UsuarioServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.UsuarioServiceRemote")				  
	private UsuarioServiceRemote usuarioServiceRemote;
	
	@EJB(lookup =Constants.RUTA_EJB+"AccesoServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.AccesoServiceRemote")	
	private AccesoServiceRemote accesoServiceRemote;
	
	@EJB(lookup = Constants.RUTA_EJB+"RegistroServiceImpl!pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote")
	private RegistroServiceRemote registroServiceRemote;
	
	
	private List<SalaBiblioteca> listaSalas;
	
	public SeguridadMB(){
		
	}
	
	@PostConstruct
	public void init(){
		this.setUsuario(new Usuario());
		this.listaSalas = new ArrayList<>();
		this.obtenerListasBase();		
	}
	
	public void obtenerListasBase() {
		try {
			this.listaSalas = this.registroServiceRemote.listarSalasDisponibles(Constants.ABSYSNET_LECTOR_LECOBI_BNP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}		
	
	//validar inicio de sesion de usuario
	public String validar(){
		
		String respuesta = "login";
		
		try {
			
/*			
			Boolean swCaptcha = validarCaptchaRespuesta();
			
			if (!swCaptcha) {
				return respuesta;
			}
*/
			
			Boolean swLDAP = this.validarUsuarioLDAP(usuario.getUsuario(), usuario.getClave());
			
			if(swLDAP){
					
				usuarioLogin = this.getUsuarioServiceRemote().validarAcceso(usuario);
				
				if(usuarioLogin.getId() != null && usuarioLogin.getId() != 0){	
					if (usuarioLogin.getPerfil().getId()==4L || usuarioLogin.getPerfil().getId()==6L){//atencion en sala de lectura
						String ipMaquina = this.getIpMaquina();
						CredencialUsuarioDto credencialUsuario = new CredencialUsuarioDto(usuarioLogin.getId(), usuarioLogin.getUsuario(), ipMaquina, usuario.getSesionSalaUsuario().getSala().getIdSala());
						SesionSalaUsuario sesionSalaUsuario = registroServiceRemote.validarAccesoReferencistaASala(credencialUsuario);
						sesionSalaUsuario.getSesionUsuario().setIpMaquina(ipMaquina);
						if (sesionSalaUsuario.getCodigoRptaValidacion().equals(Constants.CODIGO_EXITO_TRANSACCION)){
							usuarioLogin.setSesionSalaUsuario(sesionSalaUsuario);
							respuesta = iniciarSesion(usuarioLogin);
							this.cargarAccesos(usuarioLogin);
						}else{
							this.setMensajeError(sesionSalaUsuario.getDescripcionRptaValidacion());
							respuesta = "login";
						}
					}else{
						if (usuarioLogin.getPerfil().getId()==2L || usuarioLogin.getPerfil().getId()==1L) {
							respuesta = iniciarSesion(usuarioLogin);
							this.cargarAccesos(usuarioLogin);
						}else {
							this.setMensajeError("No cuenta con permisos para acceder al Sistema.");
							respuesta = "login";
						}						
					}
				} else {
					this.setMensajeError("No cuenta con permisos para acceder al Sistema.");
					respuesta = "login";
				}
								
			} else {
				this.setMensajeError("Usuario o clave incorrecta, por favor int�ntelo nuevamente.");
				respuesta = "login";
			}

		
		} catch (Exception e) {
			this.setMensajeError("Error al validar acceso");
			logger.error(e);
			e.printStackTrace();
		}
			
		return respuesta;
	}
	
	public String login() {
		return "login";
	}
	
    @SuppressWarnings({ "unchecked", "unused" })
	private boolean validarUsuarioLDAP(String usuario, String clave) {
        @SuppressWarnings("rawtypes")
		Hashtable env = new Hashtable(11);
        boolean ret = false;
        DirContext ctx = null;
        try {
        	        	
            env.put(Context.INITIAL_CONTEXT_FACTORY, super.getString("LDAP.INITIAL_CONTEXT_FACTORY"));
            env.put(Context.SECURITY_AUTHENTICATION, super.getString("LDAP.SECURITY_AUTHENTICATION"));
            env.put(Context.SECURITY_PRINCIPAL, usuario + super.getString("LDAP.SECURITY_PRINCIPAL"));
            env.put(Context.SECURITY_CREDENTIALS, clave);
            env.put(Context.PROVIDER_URL, super.getString("LDAP.PROVIDER_URL"));
            ctx = new InitialDirContext(env);
            if (ctx == null) {
                ret = false;
            } else {
                ret = true;
            }
            ctx.close();
        } catch (NamingException ne) {
            logger.error("Error al validar Usuario LDAP "+ ne.getMessage());
            ne.printStackTrace();
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                } catch (NamingException ne) {
                    logger.error(ne.getMessage());
                    ne.printStackTrace();
                }
            }
        }
        
        System.out.println("RESULTADO LDAP: " + ret);
        
        return ret;
    }
	
    private String iniciarSesion(Usuario usuario){
    	
		FacesContext fctx = FacesContext.getCurrentInstance();
		ExternalContext ectx = fctx.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
		HttpSession session = request.getSession(true);
		session.setAttribute("ID", session.getId());
		session.setAttribute("usuario",usuario);
		
		if (usuario.getPerfil().getId()==2L || usuario.getPerfil().getId() == 1L){
			return "homeMonitoreo";
		}else{
			return "home";
		}
    	
    }
    
    
    public void cargarAccesos(Usuario tmpUsuario){
    	Long perfilId = tmpUsuario.getPerfil().getId();
		try {
			lstAccesos= this.getAccesoServiceRemote().buscarAccesos(perfilId);			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			this.setMensajeError("Error al cargar Accesos");
		}
    }
    
	public void cerrarSesion() {

		FacesContext fctx = FacesContext.getCurrentInstance();
		ExternalContext ectx = fctx.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
		
		HttpSession session = request.getSession();
		session.removeAttribute("ID");
		session.removeAttribute("usuario");
		session.invalidate();
		
		/*try {
			
		} catch (Exception e) {
			this.setMensajeError("Error al cerrar sesi�n");
		}*/
		
		try {
			//cambiar antes de pase a prod
			//FacesContext.getCurrentInstance().getExternalContext().redirect("/ControlAccesoSala");
			
			FacesContext.getCurrentInstance().getExternalContext().redirect("/ControlAccesoSala/faces/seguridad/login.xhtml");
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		
	}
	
	public boolean validarCaptchaRespuesta() {

		match = 1;
		FacesContext fctx = FacesContext.getCurrentInstance();
		ExternalContext ectx = fctx.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
		if (rpHash(request.getParameter("defaultReal")).equals(
				request.getParameter("defaultRealHash"))) {
			match = 1;
		} else {
			match = 0;
			this.setMensajeAlerta("C�digo de seguridad incorrecto");
			return false;
		}
		match = 1;
		return true;
	}
	
	private String rpHash(String value) {
		int hash = 5381;
		value = value.toUpperCase();
		for (int i = 0; i < value.length(); i++) {
			hash = ((hash << 5) + hash) + value.charAt(i);
		}
		return String.valueOf(hash);
	}
	
	public void sesionExpirada() throws IOException{
		FacesContext fctx = FacesContext.getCurrentInstance();
		ExternalContext ectx = fctx.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
		
		HttpSession session = request.getSession();
		session.removeAttribute("ID");
		session.removeAttribute("usuario");
		session.invalidate();
		
		/*try {
			
		} catch (Exception e) {
			this.setMensajeError("Error al cerrar sesi�n");
		}*/
		
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("/ControlAccesoSala/faces/error/sesion_expired.xhtml");
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}		
		
		
	}

	public boolean getAcceso(String nombreInterno){
		
		if(lstAccesos == null){
			
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/ControlAccesoSala/faces/error/sesion_expired.xhtml");
			} catch (IOException e) {
				// 
				e.printStackTrace();
			}
			
		} else {
			
			for (Acceso acceso : lstAccesos) {
				Componente componente =acceso.getComponente();
				if (componente==null) {
					return false;
				}			
				if (componente.getNombreInterno().equals(nombreInterno)) {
					return acceso.isAsignado();
				} 				
			}
			
		}

		return false;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getClaveActual() {
		return claveActual;
	}

	public void setClaveActual(String claveActual) {
		this.claveActual = claveActual;
	}

	public Usuario getUsuarioLogin() {
		return usuarioLogin;
	}

	public void setUsuarioLogin(Usuario usuarioLogin) {
		this.usuarioLogin = usuarioLogin;
	}

	public boolean isSwClave() {
		return swClave;
	}

	public void setSwClave(boolean swClave) {
		this.swClave = swClave;
	}

	public int getMatch() {
		return match;
	}

	public void setMatch(int match) {
		this.match = match;
	}

	public UsuarioServiceRemote getUsuarioServiceRemote() {
		return usuarioServiceRemote;
	}

	public void setUsuarioServiceRemote(UsuarioServiceRemote usuarioServiceRemote) {
		this.usuarioServiceRemote = usuarioServiceRemote;
	}

	public List<Acceso> getLstAccesos() {
		return lstAccesos;
	}

	public void setLstAccesos(List<Acceso> lstAccesos) {
		this.lstAccesos = lstAccesos;
	}

	public AccesoServiceRemote getAccesoServiceRemote() {
		return accesoServiceRemote;
	}

	public void setAccesoServiceRemote(AccesoServiceRemote accesoServiceRemote) {
		this.accesoServiceRemote = accesoServiceRemote;
	}

	public List<SalaBiblioteca> getListaSalas() {
		return listaSalas;
	}

	public void setListaSalas(List<SalaBiblioteca> listaSalas) {
		this.listaSalas = listaSalas;
	}
	
	
	
}
