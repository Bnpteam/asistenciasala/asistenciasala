package pe.gob.servir.sistemas.transparencia.presentacion.administracion;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.PerfilServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.UsuarioServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.validator.ActiveDirectoryObject;
import pe.gob.servir.systems.util.validator.ActiveDirectoryValidation;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "usuarioMB")
@SessionScoped
public class UsuarioMB extends BasicMB {

	private static final Logger logger = Logger.getLogger(UsuarioMB.class);
	private static final long serialVersionUID = 1L;

	private Usuario					usuario;
	private List<Usuario>			lstUsuario;
	
	private List<Perfil>			lstPerfil;
	private List<T01Maestro>		lstT01UnidadOrganica;
	private boolean					swBtnGrabar;
	private boolean					swBtnValidarUser;
	
	private boolean					swDisableCampos;
	
	private String					tituloFormulario;
	
	RequestContext 					ReqContext;
	private String					mensajePopUp;

	@EJB(lookup = Constants.RUTA_EJB+"UsuarioServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.UsuarioServiceRemote")	
	private UsuarioServiceRemote usuarioServiceRemote;
	
	@EJB(lookup = Constants.RUTA_EJB+"PerfilServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.PerfilServiceRemote")	
	private PerfilServiceRemote perfilServiceRemote;
	
	@PostConstruct
	public void init(){
		this.inicializarValores();
		//this.setUsuario(new Usuario());
		//this.setLstUsuario(new ArrayList<Usuario>());
		//this.getLstUsuario();
		//this.setSwBtnGrabar(false);
		//this.setSwBtnValidarUser(false);
	}

	public void inicializarValores(){
		this.setUsuario(new Usuario());
		this.setLstUsuario(new ArrayList<Usuario>());
		//this.getLstUsuario();
		
		this.getUsuario().setUsuario("");
		this.getUsuario().getPerfil().setId(0L);
		this.listarUsuarios();	
		
		this.setSwBtnGrabar(false);
		this.setSwBtnValidarUser(false);
		
		this.setTituloFormulario("");
	}
	
	public String listado() {
		this.setUsuario(new Usuario());
		return this.listarUsuarios();	
	}

	public String listarUsuarios() {
		
		try {			
			lstUsuario=this.getUsuarioServiceRemote().listar(this.getUsuario());
		} catch (ServicioException e) {
			logger.error(e);
			e.printStackTrace();
		}

		return "usuario_listado";	
	}
	
	public String nuevo() {
		this.setUsuario(new Usuario());
		this.setSwBtnGrabar(true);
		this.setSwBtnValidarUser(false);
		this.setSwDisableCampos(false);
		this.setTituloFormulario("Registro de Usuario");
		return "usuario_registro";	
	}

	public String limpiar() {
		this.setUsuario(new Usuario());
		return "usuario_registro";	
	}
	
	public void validarActiveDirectory(){
		boolean sw = false;
		
		try {
			
			if(!VO.isEmpty(this.getUsuario().getUsuario())){
				
				sw = this.getUsuarioServiceRemote().consultarExistencia(this.getUsuario());
				
				if(!sw){
					
					ActiveDirectoryObject ado = new ActiveDirectoryObject();
					ActiveDirectoryValidation validation = new ActiveDirectoryValidation();
					
					try {
						
						ado = validation.obtenerUsuarioAD(this.getUsuario().getUsuario());
						
						if(!VO.isEmpty(ado.getUsuario())){
							
							super.setMensajeAviso("Usuario encontrado");
							this.getUsuario().setNombreCompleto(ado.getNombres());
							this.getUsuario().setCorreo(ado.getCorreo());
							this.setSwBtnGrabar(true);
							this.setSwDisableCampos(true);
							
						} else {
							super.setMensajeAlerta("El usuario no existe en el Directorio Activo");
						}
						
					} catch (NamingException e) {
						logger.error(e);
						e.printStackTrace();
						super.setMensajeError("Error al consultar la informaci�n en el Active Directory.");
					}
					
				} else {
					super.setMensajeAlerta("El usuario ya se encuentra registrado");
				}
				
			} else {
				super.setMensajeAlerta("Debe ingresar el Usuario.");
			}
		
		} catch (ServicioException e) {
			logger.error(e);
			e.printStackTrace();
		}
	}
	
	private boolean validar(){
	/*	
		if(VO.isEmpty(this.getUsuario().getNroDNI())){
			super.setMensajeAlerta("Ingrese el DNI");
			return false;
		}
	*/	
		/*if (VO.isEmpty(this.getUsuario().getNombreCompleto())) {
			super.setMensajeAlerta("El nombre completo es requerido");
			return false;
		}*/
		
		
		if (VO.isEmpty(this.getUsuario().getUsuario())) {
			super.setMensajeAlerta("El usuario es requerido");
			return false;
		}
		
//dactivo
		/*
		if (this.getUsuario().getId()==0) {
			if (VO.isEmpty(this.getUsuario().getClave())) {
				super.setMensajeAlerta("La clave es requerida");
				return false;
			}
			
		} else {

			boolean resultado; 
			ActiveDirectoryValidation validation = new ActiveDirectoryValidation();
			try {
				resultado = validation.confirmUser(this.getUsuario().getUsuario());
				if (!resultado) {
					super.setMensajeAlerta("El usuario no existe en el Directorio Activo");
					return false;
				}
			} catch (NamingException e) {
				e.printStackTrace();
				super.setMensajeAlerta("Ocurri� un error al consultar la informaci�n del Directorio Activo");
				return false;
			}
		
		}	
	*/
		
		if(VO.isEmpty(this.getUsuario().getCorreo())){
			super.setMensajeAlerta("El correo es requerido");
			return false;
		}
		if(validarFormatoCorreo(this.getUsuario().getCorreo())==false){
			super.setMensajeAlerta("El correo ingresado no es v�lido, ingrese en el formato: correo@dominio");
			return false;
		}
		
		if (this.getUsuario().getPerfil().getId()==0) {
			super.setMensajeAlerta("El perfil es requerido");
			return false;
		}
			
		return true;
	}
	
	public String grabar() {
		String pagina="usuario_registro";
		
		ReturnObject sw = new ReturnObject();
		
		try {
			if (!this.validar()) {
				return "usuario_registro";
			}
			if (this.getUsuario().getId()==0) {
				this.setUsuarioRegistro(this.getUsuario());

				/*				
				if (this.getUsuario().getUsuario().trim().length()<3) {
					this.setMensajeAlerta("El usuario debe contener al menos 4 caracteres como m�nimo");
					return "usuario_registro";
				}	

				String msg=Clave.isValid(this.getUsuario().getClave(),this.getUsuario().getUsuario());	
				if (msg.trim().length()>0) {
					this.setMensajeAlerta(msg);
					return "usuario_registro";
				}	
				

				this.getUsuario().setClave(Encrypt.encrypt(this.getUsuario().getClave()));
*/			
				sw= this.getUsuarioServiceRemote().insertar(this.getUsuario());
				
				if (sw.getId() != 0) {
					super.setMensajeAviso("�xito al registrar el usuario");
				}else{
					super.setMensajeError("Error al registrar el usuario");
				}
			} else {
				
				this.setUsuarioModificacion(this.getUsuario());
				
				sw.setSw(this.getUsuarioServiceRemote().actualizar(this.getUsuario()));
				if (sw.getSw()) {
					super.setMensajeAviso("�xito al modificar el usuario");
				}else{
					super.setMensajeError("Error al modificar el usuario");
				}
			}
		} catch (Exception e) {
			super.setMensajeError("Error al modificar el usuario");
			logger.error(e);
			e.printStackTrace();
		}
		if(sw.getSw()){
			pagina="usuario_listado";
		}		
		return pagina;	
	}
	
	 public boolean validarFormatoCorreo(String correo) {
	        Pattern pat = null;
	        Matcher mat = null;        
	        pat = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
	        mat = pat.matcher(correo);
	        if (mat.find()) {
	            return true;
	        }else{
	            return false;
	        }        
	  }
	 

	
	public String eliminar() {
		boolean sw=false;
		this.setUsuario(usuario);
		try {
			this.setUsuarioModificacion(this.getUsuario());
			sw=this.getUsuarioServiceRemote().eliminar(this.getUsuario());			
			if (sw) {
				this.listarUsuarios();
				super.setMensajeAviso("El usuario fue eliminado satisfactoriamente.");		
			}else{
				super.setMensajeError("Error al eliminar Usuario");		
			}
		} catch (Exception e) {
			super.setMensajeError("Error al eliminar Usuario");
			logger.error(e);
			e.printStackTrace();
		}
		
		return "usuario_listado";	
	}
	
	public List<Perfil> getLstPerfil() {
		try {
			lstPerfil=this.getPerfilServiceRemote().listarTodos();
		} catch (Exception e) {
			this.setMensajeError("Error al cargar Perfiles");
			logger.error(e);
			e.printStackTrace();
		}
		return lstPerfil;
	}

	public List<T01Maestro> getLstT01UnidadOrganica() {
		try {
			lstT01UnidadOrganica = super.getT01MaestroValuesNL("T01_UNIDAD_ORGANICA");
		} catch (Exception e) {
			this.setMensajeError("Error al cargar Unidades Organicas");
			logger.error(e);
			e.printStackTrace();
		}
		return lstT01UnidadOrganica;
	}

	public void setLstT01UnidadOrganica(List<T01Maestro> lstT01UnidadOrganica) {
		this.lstT01UnidadOrganica = lstT01UnidadOrganica;
	}

	public String modificar(Usuario usuario) {
		this.setUsuario(usuario);
		this.setTituloFormulario("Modificaci�n de Usuario");
		try {
			Usuario tmpUsuario= this.getUsuarioServiceRemote().buscarXId(this.getUsuario());
			this.setUsuario(tmpUsuario);
			this.setSwBtnGrabar(true);
			this.setSwBtnValidarUser(false);	
			this.setSwDisableCampos(true);
		} catch (Exception e) {
			super.setMensajeError("Error al cargar los datos del usuario.");
			logger.error(e);
			e.printStackTrace();
		}
		return "usuario_registro";	
	}

	public List<Usuario> getLstUsuario() {
		return lstUsuario;
	}

	public void setLstUsuario(List<Usuario> lstUsuario) {
		this.lstUsuario = lstUsuario;
	}
	
	public void setLstPerfil(List<Perfil> lstPerfil) {
		this.lstPerfil = lstPerfil;
	}

	public PerfilServiceRemote getPerfilServiceRemote() {
		return perfilServiceRemote;
	}

	public void setPerfilServiceRemote(PerfilServiceRemote perfilServiceRemote) {
		this.perfilServiceRemote = perfilServiceRemote;
	}

	public UsuarioServiceRemote getUsuarioServiceRemote() {
		return usuarioServiceRemote;
	}

	public void setUsuarioServiceRemote(UsuarioServiceRemote usuarioServiceRemote) {
		this.usuarioServiceRemote = usuarioServiceRemote;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isSwBtnGrabar() {
		return swBtnGrabar;
	}

	public void setSwBtnGrabar(boolean swBtnGrabar) {
		this.swBtnGrabar = swBtnGrabar;
	}

	public boolean isSwBtnValidarUser() {
		return swBtnValidarUser;
	}

	public void setSwBtnValidarUser(boolean swBtnValidarUser) {
		this.swBtnValidarUser = swBtnValidarUser;
	}
	
	public boolean isSwDisableCampos() {
		return swDisableCampos;
	}

	public void setSwDisableCampos(boolean swDisableCampos) {
		this.swDisableCampos = swDisableCampos;
	}
	
	public String getTituloFormulario() {
		return tituloFormulario;
	}

	public void setTituloFormulario(String tituloFormulario) {
		this.tituloFormulario = tituloFormulario;
	}

	public String getMensajePopUp() {
		return mensajePopUp;
	}

	public void setMensajePopUp(String mensajePopUp) {
		this.mensajePopUp = mensajePopUp;
	}



	/**********************************************************************************************************
	 * 	
	 * Auditoria
	 * 
	 **********************************************************************************************************/
		
	public void setUsuarioRegistro(Usuario prmUsuario) {		
		Usuario usuario=super.getUsuario();
		if (usuario!=null) {
			prmUsuario.setUsuarioIdRegistro(usuario.getId());
		}
		
	}

	public void setUsuarioModificacion(Usuario prmUsuario) {		
		Usuario usuario=super.getUsuario();
		if (usuario!=null) {
			prmUsuario.setUsuarioIdModificacion(usuario.getId());		
		}
		
	}
	
}
