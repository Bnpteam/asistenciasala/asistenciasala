package pe.gob.servir.sistemas.transparencia.presentacion.base;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.remoto.PersonaServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.remoto.T01MaestroServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.sistemas.transparencia.model.negocio.RegistroSISTRA;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;
import pe.gob.servir.std.service.ws.ResponseDatosTramiteCEType;
import pe.gob.servir.std.service.ws.STDServiceInterface;
import pe.gob.servir.std.service.ws.STDWebService;
import pe.gob.servir.systems.util.ipaddress.IpAddressPc;
import pe.gob.servir.systems.util.validator.VO;
import pe.org.bnp.wssolicitudaccesoip.ws.AccessRequestServiceProxy;
import pe.org.bnp.wssolicitudaccesoip.ws.PublicInformationAccessRequest;
import pe.org.bnp.wssolicitudaccesoip.ws.PublicInformationAccessRequestFilterInput;

@ManagedBean(name = "basicMB")
public class BasicMB implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final Logger logger = Logger.getLogger(this.getClass());

	protected 	String 					currentPage;
	protected	String					msgOperacion;
	protected 	String					msgListaVacia;
	
	protected	String					mensaje;
	
	protected	boolean					swEliminar;	
		    
    private 	StreamedContent 		streamedContent;

	private 	boolean					swRegistroHabilitado;
	
	private		String					nombreSistema;
	
	private  	Date 					fechaHoy;
	
	@EJB(lookup = Constants.RUTA_EJB+"T01MaestroServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.remoto.T01MaestroServiceRemote")	
	private T01MaestroServiceRemote t01MaestroServiceRemote;  

	@EJB(lookup = Constants.RUTA_EJB+"PersonaServiceImpl!pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.remoto.PersonaServiceRemote")				  
	private PersonaServiceRemote personaServiceRemote;
	
	
	public BasicMB() {
		super();
		this.init();
	}

	private void init(){		
		this.setMsgListaVacia("No se han encontrado registros");	
		this.setNombreSistema("Sistema de Gesti�n de Consultas Externas");
	}

	public String getMsgOperacion() {
		return msgOperacion;
	}

	public void setMsgOperacion(String msgOperacion) {
		this.msgOperacion = msgOperacion;
	}

	public String getMsgListaVacia() {
		return msgListaVacia;
	}

	public void setMsgListaVacia(String msgListaVacia) {
		this.msgListaVacia = msgListaVacia;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean isSwEliminar() {
		return swEliminar;
	}

	public void setSwEliminar(boolean swEliminar) {
		this.swEliminar = swEliminar;
	}

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}

	public void setStreamedContent(StreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}

	public boolean isSwRegistroHabilitado() {
		return swRegistroHabilitado;
	}

	public void setSwRegistroHabilitado(boolean swRegistroHabilitado) {
		this.swRegistroHabilitado = swRegistroHabilitado;
	}

	public String getNombreSistema() {
		return nombreSistema;
	}

	public void setNombreSistema(String nombreSistema) {
		this.nombreSistema = nombreSistema;
	}

	public ServletContext getServletContext() {		
		return (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
	}
	
	public String getContextPath() {
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String realPath = ctx.getContextPath();
		return realPath;
	}	

	public String getContextRealPath() {
		ServletContext ctx = this.getServletContext();
		String realPath = ctx.getRealPath("/");
		return realPath;
	}	

	
	public HttpServletResponse getHttpServletResponse() {			
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();		
		return httpServletResponse;	
	}

	public HttpServletRequest getHttpServletRequest() {			
		HttpServletRequest httpServletRequest= (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		return httpServletRequest;	
	}
		
	public HttpSession getHttpSession() {			
		return this.getHttpServletRequest().getSession(true);	
	}
	
	public Usuario getUsuario() {		
		Usuario usuario= (Usuario)this.getHttpSession().getAttribute("usuario");
		if (usuario==null) {
			usuario=new Usuario();
		}
		return usuario;
	}
	
	/**********************************************************************************************************
	 * 	
	 * Auditoria
	 * 
	 **********************************************************************************************************/
		
	public void setUsuarioRegistro(BasicObject objeto) {		
		Usuario usuario = this.getUsuario();
		if (usuario != null) {
			objeto.setUsuarioIdRegistro(usuario.getId());
		}
		
	}

	public void setUsuarioModificacion(BasicObject objeto) {		
		Usuario usuario = this.getUsuario();
		if (usuario != null) {
			objeto.setUsuarioIdModificacion(usuario.getId());		
		}
		
	}
	
	/**********************************************************************************************************
	 * 	
	 * Mensajes
	 * 
	 **********************************************************************************************************/
	
	public void setMensaje(String titulo, String mensaje) {
		FacesMessage msg = new FacesMessage(titulo, mensaje);  
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void  setMensajeAviso(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Aviso",mensaje) );
	}
	
	public void  setMensajeAlerta(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso",mensaje) );
	}

	public void  setMensajeError(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Aviso",mensaje) );
	}
	
	
	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream("config.properties"));
			return properties.getProperty(key); 
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		
		return "";
	}
	
	public String validarSesion() {

		if (this.getUsuario()==null) {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			try {
				response.sendRedirect("../../faces/seguridad/login.xhtml");
			} catch (IOException e) {
				logger.error(e);
				e.printStackTrace();
			}	
		}
			
		return "";
	}
	
	public List<T01Maestro> getT01MaestroValuesNL(String codigoTabla) {
		List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		try {	
			lstT01Maestro = this.getT01MaestroServiceRemote().listarXCodigoTablaNL(codigoTabla);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			lstT01Maestro = new ArrayList<T01Maestro>();
			this.setMensajeError("Error al consultar la Tabla Maestra 01");
		}
		return lstT01Maestro;
	}
	
	public List<T01Maestro> getUndOrg_CnSecretarias(int prmCodPerfilUser) {
		List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		try {	
			lstT01Maestro = this.getT01MaestroServiceRemote().listarXUndOrgCnSecretarias(prmCodPerfilUser);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			lstT01Maestro = new ArrayList<T01Maestro>();
			this.setMensajeError("Error al consultar la Tabla Maestra 01");
		}
		return lstT01Maestro;
	}
	
	

	//webservice SISTRA
	
	public List<RegistroSISTRA> getListaRegistrosSISTRAWS(ReporteBandeja reporteBandeja){

		/*FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String remoteHost = request.getRemoteAddr();*/ // cliente ip
		List<RegistroSISTRA> listaRegistrosSISTRAWS = new ArrayList<RegistroSISTRA>();
		
		try {
			
			AccessRequestServiceProxy accessRequestServiceProxy = new AccessRequestServiceProxy();
			accessRequestServiceProxy.setEndpoint("http://172.16.88.119:8080/services/wsSistra/");
			
			PublicInformationAccessRequestFilterInput publicInformationAccessRequestFilterInput = new PublicInformationAccessRequestFilterInput();
			publicInformationAccessRequestFilterInput.setSistraId("0");
			publicInformationAccessRequestFilterInput.setStartPeriod("01/01/2018");
			publicInformationAccessRequestFilterInput.setEndPeriod("15/06/2018");
			
			PublicInformationAccessRequest lista[] = accessRequestServiceProxy.getListAccessRequestPI(publicInformationAccessRequestFilterInput);
			//List<PublicInformationAccessRequest> lista = accessRequestServiceProxy.getListAccessRequestPI(publicInformationAccessRequestFilterInput);
			
			System.out.println(lista);
			RegistroSISTRA registro = null;
			for(int i=0; i<lista.length; i++){
				registro = new RegistroSISTRA();
				registro.setNumeroSistra(lista[i].getId());
				registro.setNumeroDocumento(lista[i].getDocumentNumber());
				registro.setFechaRegistroSistra(lista[i].getRequestDate().toString());
				registro.setFechaRegistroSistraDate(lista[i].getRequestDate().getTime());
				registro.setAsunto(lista[i].getIssue());
				registro.setRemite(lista[i].getClaimant());
				registro.setPlazoMaximo(lista[i].getDeadLine());
				registro.setEstado(lista[i].getStateName());
				listaRegistrosSISTRAWS.add(registro);				
			}
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}	
		
		return listaRegistrosSISTRAWS;
	
	}

	public T01MaestroServiceRemote getT01MaestroServiceRemote() {
		return t01MaestroServiceRemote;
	}

	public void setT01MaestroServiceRemote(
			T01MaestroServiceRemote t01MaestroServiceRemote) {
		this.t01MaestroServiceRemote = t01MaestroServiceRemote;
	}

	public PersonaServiceRemote getPersonaServiceRemote() {
		return personaServiceRemote;
	}

	public void setPersonaServiceRemote(PersonaServiceRemote personaServiceRemote) {
		this.personaServiceRemote = personaServiceRemote;
	}

	public Date getFechaHoy() {
		return fechaHoy;
	}

	public void setFechaHoy(Date fechaHoy) {
		this.fechaHoy = fechaHoy;
	}

	public String getIpMaquina(){
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String direccionIp = httpServletRequest.getRemoteAddr();
		direccionIp = VO.getString(direccionIp).equals(Constants.EMPTY_STRING)?"-":VO.getString(direccionIp);
		return direccionIp;
	}
	
}
