package pe.gob.servir.sistemas.transparencia.handler.exception;
 
import java.io.IOException;
import java.util.Iterator; 

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
  
public class CustomExceptionHandler extends ExceptionHandlerWrapper {

    private ExceptionHandler wrapped;

    CustomExceptionHandler(ExceptionHandler exception) {
        this.wrapped = exception;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    
    @Override
    public void handle() throws FacesException {
    	 
        final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
        //System.out.println("Entro ==> ");       
        
        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
         
            // get the exception from context
            Throwable t = context.getException();

            final FacesContext fc = FacesContext.getCurrentInstance();
           
            final NavigationHandler nav = fc.getApplication().getNavigationHandler();

            //here you do what ever you want with exception
            try {

                if (t instanceof ViewExpiredException) {
                	
                    //String errorPageLocation = "/faces/Info.jsp";
                    
                    try {
                    
	                    if(((ViewExpiredException) t).getViewId().equals("/seguridad/login.xhtml")){
	                    	FacesContext.getCurrentInstance().getExternalContext().redirect("/RegistroUsuario");
	                    } else  {	
							FacesContext.getCurrentInstance().getExternalContext().redirect("/RegistroUsuario/faces/error/sesion_expired.xhtml");	
	                    }
                    
                    } catch (IOException e) {
						// 
						e.printStackTrace();
					}	
                    	
                    //fc.setViewRoot(fc.getApplication().getViewHandler().createView(fc, errorPageLocation));
                    //fc.getPartialViewContext().setRenderAll(true);
                    //fc.renderResponse();
                } else {
                     
                    //nav.handleNavigation(fc, null, "/faces/Error.xhtml");
                	nav.handleNavigation(fc, null, "/faces/error/error.xhtml");
                }

                fc.renderResponse();
                
            } finally {
                //remove it from queue
                i.remove();
            }
        }
        //parent hanle
        getWrapped().handle();    
    }
    
}