package pe.gob.servir.sistemas.transparencia.servicios.alfresco;

import org.apache.log4j.Logger;

import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import pe.gob.servir.systems.util.alfresco.ResultAlfresco;
import pe.gob.servir.systems.util.validator.VO;
import ws.alfresco.servir.gob.pe.ArchivoType;
import ws.alfresco.servir.gob.pe.MetadataType;

public class AlfrescoDocumentoAdjunto extends AlfrescoBase {
	
	private static final Logger logger = Logger.getLogger(AlfrescoDocumentoAdjunto.class);
	
	public ResultAlfresco subirArchivo(FileAlfresco fileAlfresco, String proceso, String anio, String mes ){
		
		ResultAlfresco	resultAlfresco = new ResultAlfresco();	
		String			nombreArchivoAlfresco;
		
		super.setToken(super.getResponseAutenticacion().getToken());
		
        MetadataType metadata = new MetadataType();
        metadata.setParameter1("cmis:document");
        
        nombreArchivoAlfresco = VO.convertNameFile(fileAlfresco.getFileName());
        
        metadata.setParameter2(nombreArchivoAlfresco);

		ArchivoType archivoType = new ArchivoType();
		archivoType.setRutaRepositorio(this.getRutaRepositorio(proceso, anio , mes));

		archivoType.setDocumento(fileAlfresco.getRawFile());
							
        archivoType.setFormato(fileAlfresco.getExtension());
        
        super.setResponseRegistrarContenidoType(super.getPort().registrarContenido(super.getToken(), super.getSeguridad(), super.getAuditoria(), metadata, archivoType));
        
    	// Info. autenticacion.
    	String codigoAuth  = this.getResponseAutenticacion().getMensaje().getCodigoMensaje();
    	logger.debug( "CODIGO: "  + codigoAuth );
    	String mensajeAuth = this.getResponseAutenticacion().getMensaje().getDescripMensaje();
    	logger.debug( "MENSAJE: " + mensajeAuth );
        
        resultAlfresco.setUiid(super.getResponseRegistrarContenidoType().getArchivo().getUiidRecurso());
        resultAlfresco.setCodigo(super.getResponseRegistrarContenidoType().getMensaje().getCodigoMensaje());
        resultAlfresco.setDescripcion(super.getResponseRegistrarContenidoType().getMensaje().getDescripMensaje());
        
		return resultAlfresco;
	}

	public ResultAlfresco actualizarArchivo(FileAlfresco fileAlfresco){

		ResultAlfresco	resultAlfresco = new ResultAlfresco();	
		
		super.setToken(super.getResponseAutenticacion().getToken());
        MetadataType metadata = new MetadataType();
        metadata.setParameter1("cmis:document");
        metadata.setParameter2(null);
        
        ArchivoType archivoType = new ArchivoType();
        archivoType.setUiidRecurso(fileAlfresco.getUiid());
        archivoType.setDocumento(fileAlfresco.getRawFile());
        archivoType.setFormato(fileAlfresco.getExtension());
        
        super.setResponseModificarContenidoType(super.getPort().modificarContenido(super.getToken(), super.getSeguridad(), super.getAuditoria(), metadata, archivoType));
        
        resultAlfresco.setUiid(super.getResponseModificarContenidoType().getArchivo().getUiidRecurso());
        resultAlfresco.setCodigo(super.getResponseModificarContenidoType().getMensaje().getCodigoMensaje());
        resultAlfresco.setDescripcion(super.getResponseModificarContenidoType().getMensaje().getDescripMensaje());
        
		return resultAlfresco;
	}
	
	
	public String getRutaRepositorio(String proceso, String anio, String mes){
		
		StringBuilder pathAlfrescoFull = new StringBuilder();	
		pathAlfrescoFull.append(super.getString("preruta.repositorio.alfresco.transparencia"));
		//pathAlfrescoFull.append(super.getString("directorio.gestion"));
		pathAlfrescoFull.append("/"+proceso);
		pathAlfrescoFull.append("/"+anio);
		pathAlfrescoFull.append("/"+mes);
		
		//pathAlfrescoFull.append(convocatoria.getProceso().replace(' ' , '_'));
		
		return pathAlfrescoFull.toString();

	}

}
