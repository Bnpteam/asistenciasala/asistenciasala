
package pe.gob.servir.std.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuditoriaType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuditoriaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ipPc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="macAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pcName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usuarioSis" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usuarioRed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuditoriaType", propOrder = {
    "ipPc",
    "macAddress",
    "pcName",
    "usuarioSis",
    "usuarioRed"
})
public class AuditoriaType {

    @XmlElement(required = true)
    protected String ipPc;
    @XmlElement(required = true)
    protected String macAddress;
    @XmlElement(required = true)
    protected String pcName;
    @XmlElement(required = true)
    protected String usuarioSis;
    @XmlElement(required = true)
    protected String usuarioRed;

    /**
     * Gets the value of the ipPc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpPc() {
        return ipPc;
    }

    /**
     * Sets the value of the ipPc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpPc(String value) {
        this.ipPc = value;
    }

    /**
     * Gets the value of the macAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * Sets the value of the macAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacAddress(String value) {
        this.macAddress = value;
    }

    /**
     * Gets the value of the pcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPcName() {
        return pcName;
    }

    /**
     * Sets the value of the pcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPcName(String value) {
        this.pcName = value;
    }

    /**
     * Gets the value of the usuarioSis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioSis() {
        return usuarioSis;
    }

    /**
     * Sets the value of the usuarioSis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioSis(String value) {
        this.usuarioSis = value;
    }

    /**
     * Gets the value of the usuarioRed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioRed() {
        return usuarioRed;
    }

    /**
     * Sets the value of the usuarioRed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioRed(String value) {
        this.usuarioRed = value;
    }

}
