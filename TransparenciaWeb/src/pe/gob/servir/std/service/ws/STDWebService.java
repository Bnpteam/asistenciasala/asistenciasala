package pe.gob.servir.std.service.ws;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.7.7.redhat-1
 * 2018-02-06T15:38:46.640-05:00
 * Generated source version: 2.7.7.redhat-1
 * 
 */
@WebServiceClient(name = "STDWebService", 
                  wsdlLocation = "http://172.16.19.151:8080/STD_WS/STDServiceInterface?wsdl",
                  targetNamespace = "ws.service.std.servir.gob.pe") 
public class STDWebService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("ws.service.std.servir.gob.pe", "STDWebService");
    public final static QName STDServiceInterfaceImplPort = new QName("ws.service.std.servir.gob.pe", "STDServiceInterfaceImplPort");
    static {
        URL url = null;
        try {
            url = new URL("http://172.16.19.151:8080/STD_WS/STDServiceInterface?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(STDWebService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://172.16.19.151:8080/STD_WS/STDServiceInterface?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public STDWebService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public STDWebService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public STDWebService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public STDWebService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public STDWebService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public STDWebService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns STDServiceInterface
     */
    @WebEndpoint(name = "STDServiceInterfaceImplPort")
    public STDServiceInterface getSTDServiceInterfaceImplPort() {
        return super.getPort(STDServiceInterfaceImplPort, STDServiceInterface.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns STDServiceInterface
     */
    @WebEndpoint(name = "STDServiceInterfaceImplPort")
    public STDServiceInterface getSTDServiceInterfaceImplPort(WebServiceFeature... features) {
        return super.getPort(STDServiceInterfaceImplPort, STDServiceInterface.class, features);
    }

}
