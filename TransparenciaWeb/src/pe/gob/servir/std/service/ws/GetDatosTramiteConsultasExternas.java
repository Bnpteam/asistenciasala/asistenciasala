
package pe.gob.servir.std.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestSeguridad" type="{ws.service.std.servir.gob.pe}SeguridadType"/>
 *         &lt;element name="requestAuditoria" type="{ws.service.std.servir.gob.pe}AuditoriaType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestSeguridad",
    "requestAuditoria"
})
@XmlRootElement(name = "getDatosTramiteConsultasExternas")
public class GetDatosTramiteConsultasExternas {

    @XmlElement(required = true)
    protected SeguridadType requestSeguridad;
    @XmlElement(required = true)
    protected AuditoriaType requestAuditoria;

    /**
     * Obtiene el valor de la propiedad requestSeguridad.
     * 
     * @return
     *     possible object is
     *     {@link SeguridadType }
     *     
     */
    public SeguridadType getRequestSeguridad() {
        return requestSeguridad;
    }

    /**
     * Define el valor de la propiedad requestSeguridad.
     * 
     * @param value
     *     allowed object is
     *     {@link SeguridadType }
     *     
     */
    public void setRequestSeguridad(SeguridadType value) {
        this.requestSeguridad = value;
    }

    /**
     * Obtiene el valor de la propiedad requestAuditoria.
     * 
     * @return
     *     possible object is
     *     {@link AuditoriaType }
     *     
     */
    public AuditoriaType getRequestAuditoria() {
        return requestAuditoria;
    }

    /**
     * Define el valor de la propiedad requestAuditoria.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditoriaType }
     *     
     */
    public void setRequestAuditoria(AuditoriaType value) {
        this.requestAuditoria = value;
    }

}