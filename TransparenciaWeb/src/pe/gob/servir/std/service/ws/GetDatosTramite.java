
package pe.gob.servir.std.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestSeguridad" type="{ws.service.std.servir.gob.pe}SeguridadType"/>
 *         &lt;element name="requestAuditoria" type="{ws.service.std.servir.gob.pe}AuditoriaType"/>
 *         &lt;element name="requestTramite" type="{ws.service.std.servir.gob.pe}TramiteType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestSeguridad",
    "requestAuditoria",
    "requestTramite"
})
@XmlRootElement(name = "getDatosTramite")
public class GetDatosTramite {

    @XmlElement(required = true)
    protected SeguridadType requestSeguridad;
    @XmlElement(required = true)
    protected AuditoriaType requestAuditoria;
    @XmlElement(required = true)
    protected TramiteType requestTramite;

    /**
     * Gets the value of the requestSeguridad property.
     * 
     * @return
     *     possible object is
     *     {@link SeguridadType }
     *     
     */
    public SeguridadType getRequestSeguridad() {
        return requestSeguridad;
    }

    /**
     * Sets the value of the requestSeguridad property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeguridadType }
     *     
     */
    public void setRequestSeguridad(SeguridadType value) {
        this.requestSeguridad = value;
    }

    /**
     * Gets the value of the requestAuditoria property.
     * 
     * @return
     *     possible object is
     *     {@link AuditoriaType }
     *     
     */
    public AuditoriaType getRequestAuditoria() {
        return requestAuditoria;
    }

    /**
     * Sets the value of the requestAuditoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditoriaType }
     *     
     */
    public void setRequestAuditoria(AuditoriaType value) {
        this.requestAuditoria = value;
    }

    /**
     * Gets the value of the requestTramite property.
     * 
     * @return
     *     possible object is
     *     {@link TramiteType }
     *     
     */
    public TramiteType getRequestTramite() {
        return requestTramite;
    }

    /**
     * Sets the value of the requestTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link TramiteType }
     *     
     */
    public void setRequestTramite(TramiteType value) {
        this.requestTramite = value;
    }

}
