package pe.gob.bnp.dapi.salalectura.controller;

import java.util.List;

import org.primefaces.model.chart.PieChartModel;

import pe.gob.bnp.dapi.salalectura.dto.ResumenUsuariosActivoPorSalaDto;

public class SalasUsoActualDto {

	private PieChartModel model;
	
	private boolean hayUsuarios;

	public SalasUsoActualDto() {
		model = new PieChartModel();
		this.hayUsuarios = false;
//		model.set("Brand 1", 540);
//		model.set("Brand 2", 325);
//		model.set("Brand 3", 702);
//		model.set("Brand 4", 421);
		//model.setTitle("Simple Pie");
	}
	
	public void cargarDatos(List<ResumenUsuariosActivoPorSalaDto> listaResumenUsuariosActivoPorSala){
		this.hayUsuarios = false;
		for (ResumenUsuariosActivoPorSalaDto resumenUsuariosActivoPorSalaDto : listaResumenUsuariosActivoPorSala) {
			int cantidadUsuarios = Integer.parseInt(resumenUsuariosActivoPorSalaDto.getUsuariosEnSala());
			model.set(resumenUsuariosActivoPorSalaDto.getSala(), Integer.parseInt(resumenUsuariosActivoPorSalaDto.getUsuariosEnSala()));
			if (cantidadUsuarios>0){
				this.hayUsuarios = true;
			}
		}
		model.setLegendPosition("e");
		model.setShowDataLabels(true);
		model.setDataFormat("value");
		model.setMouseoverHighlight(true);
		
	}
	public PieChartModel getModel() {
		return model;
	}

	public boolean isHayUsuarios() {
		return hayUsuarios;
	}

	public void setHayUsuarios(boolean hayUsuarios) {
		this.hayUsuarios = hayUsuarios;
	}	
	
	
}
