package pe.gob.bnp.dapi.salalectura.controller;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import pe.gob.bnp.absysnet.ejb.service.remote.AbsysnetServiceRemote;
import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote;
import pe.gob.bnp.dapi.registrousuario.controller.BaseControllerExtern;
import pe.gob.bnp.dapi.salalectura.domain.entity.MotivoSalida;
import pe.gob.bnp.dapi.salalectura.dto.FiltroUsuarioEnSalaDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorConsultaAbsysnetDto;
import pe.gob.bnp.dapi.salalectura.dto.LectorFotoDto;
import pe.gob.bnp.dapi.salalectura.dto.MonitoreoEnSala;
import pe.gob.bnp.dapi.salalectura.dto.TransaccionAsistenciaSala;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "asistenciaSalaMB")
@SessionScoped
public class AsistenciaSalaMB extends BaseControllerExtern{
	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(AsistenciaSalaMB.class);

	@EJB(lookup = Constants.RUTA_EJB + "RegistroServiceImpl!pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote")
	private RegistroServiceRemote registroServiceRemote;

	@EJB(lookup = Constants.RUTA_EJB + "AbsysnetService!pe.gob.bnp.absysnet.ejb.service.remote.AbsysnetServiceRemote")				  
	private AbsysnetServiceRemote absysnetServiceRemote;


    private Date fechaActual;
	private int currentYear;
	
	private String filtroNumeroDocumentoIdentidad;
	
	private List<LectorConsultaAbsysnetDto> listaBusquedaLectores;
	
	private List<UsuarioEnSala> listaUsuariosEnSala;
	private RequestContext requestContext;    

	private List<MotivoSalida> listaMaestraMotivosSalida;
	
	private UsuarioEnSala usuarioSeleccionado;
	
	private Long idMotivoSalida;
	private String justificacionMotivoSalida;
	private String nombreUsuarioSeleccionado;
	

	private StreamedContent streamedContentPhotoReniec;
	private StreamedContent streamedContentPhotoAdjuntada;

	private boolean showFotoReniec;
	private boolean showFotoUsuario;
	
	private LectorFotoDto fotosLector;
	private LectorFotoDto fotosLectorEnSala;
	private boolean showSeccionFotoBusqueda;
	
	private boolean showSeccionFotoEnSala;
	private boolean showFotoUsuarioEnSala;
	private boolean showFotoReniecEnSala;
	
	private StreamedContent streamedContentPhotoReniecEnSala;
	private StreamedContent streamedContentPhotoAdjuntadaEnSala;
	
	private String tituloSeccionFotoLectorEnSala;
	
	private MonitoreoEnSala listaMonitoreoEnSala;
	
	private SalasUsoActualDto salasUsoActualGrafico;
	
	@PostConstruct
	public void init() {
		this.nombreSistema = this.getString(Constants.NOMBRE_SISTEMA_PROPERTIES);
		this.versionSistema = this.getString(Constants.VERSION_SISTEMA_PROPERTIES);
		this.fechaActual = new Date();
		this.currentYear = Calendar.getInstance().get(Calendar.YEAR);
		super.setStreamedContent(null);
		this.filtroNumeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.listaBusquedaLectores = new ArrayList<>();
		this.listaUsuariosEnSala = new ArrayList<>();
		this.listaMaestraMotivosSalida = new ArrayList<>();
		this.idMotivoSalida 			= Constants.EMPTY_LONG;
		this.justificacionMotivoSalida 	= Constants.EMPTY_STRING;
		this.nombreUsuarioSeleccionado  = Constants.EMPTY_STRING;
		this.showFotoReniec			= false;
		this.showFotoUsuario 		= false;
		this.fotosLector 			= null;
		this.showSeccionFotoBusqueda= false;
		this.showSeccionFotoEnSala	= false;
		this.showFotoUsuarioEnSala 	= false;
		this.showFotoReniecEnSala	= false;	
		this.fotosLectorEnSala  	= null;
		this.streamedContentPhotoReniec = null; 
		this.streamedContentPhotoAdjuntada = null;
		this.streamedContentPhotoReniecEnSala = null; 
		this.streamedContentPhotoAdjuntadaEnSala = null;
		this.tituloSeccionFotoLectorEnSala = Constants.EMPTY_STRING;
		this.listaMonitoreoEnSala = new MonitoreoEnSala();
		this.salasUsoActualGrafico = new SalasUsoActualDto();
	}
	public void obtenerListasMonitoreo() throws ServiceException{
		this.listaMonitoreoEnSala = this.registroServiceRemote.obtenerListasMonitoreoEnSala();
		this.salasUsoActualGrafico.cargarDatos(this.listaMonitoreoEnSala.getListaResumenUsuariosActivoPorSala());
	}
	
	//para bandeja de busqueda
	public void obtenerFotosLector(LectorConsultaAbsysnetDto lector) throws ServiceException{
		this.showSeccionFotoBusqueda = true;
		this.showFotoUsuario = false;
		this.showFotoReniec	 = false;
		this.fotosLector  = this.registroServiceRemote.obtenerFotosLector(String.valueOf(lector.getCodBarrasLector()));
		if (this.fotosLector.getRawFileAdjuntada()!=null){
			this.showFotoUsuario = true;
		}
		if (this.fotosLector.getRawFileReniec()!=null){
			this.showFotoReniec = true;
		}

	}

	public void ocultarSeccionFotosBusqueda(){
		this.showFotoReniec			= false;
		this.showFotoUsuario 		= false;
		this.showSeccionFotoBusqueda = false;
		this.fotosLector 			= null;		
		this.streamedContentPhotoReniec = null; 
		this.streamedContentPhotoAdjuntada = null;

	}	
	
	public StreamedContent getStreamedContentPhotoAdjuntada() {
		if (fotosLector==null) return null;
		if (this.fotosLector.getRawFileAdjuntada()!=null){
	        streamedContentPhotoAdjuntada = new DefaultStreamedContent(new ByteArrayInputStream(this.fotosLector.getRawFileAdjuntada()), Constants.FILE_CONTENT_TYPE_IMAGE_PREFIX+this.fotosLector.getFileExtensionAdjuntada(), this.fotosLector.getFileNameAdjuntada());
		}
        return streamedContentPhotoAdjuntada;		
	}

	public StreamedContent getStreamedContentPhotoReniec() {
		if (this.fotosLector.getRawFileReniec()!=null){
			streamedContentPhotoReniec = new DefaultStreamedContent(new ByteArrayInputStream(this.fotosLector.getRawFileReniec()), Constants.FILE_CONTENT_TYPE_IMAGE_PREFIX+"jpg", "fotoreniec.jpg");
		}
        return streamedContentPhotoReniec;		
	}
		
	public void obtenerFotosLectorEnSala(UsuarioEnSala lector) throws ServiceException{
		this.tituloSeccionFotoLectorEnSala = lector.getNombreUsuario()+" "+lector.getApellidosUsuario()+". Ingres� a las "+lector.getHoraIngreso()+ " el "+lector.getFechaIngreso()+".";
		this.showSeccionFotoEnSala	= true;
		this.showFotoUsuarioEnSala 	= false;
		this.showFotoReniecEnSala	= false;
		this.fotosLectorEnSala  = this.registroServiceRemote.obtenerFotosLector(String.valueOf(lector.getCodBarrasLector()));
		if (this.fotosLectorEnSala.getRawFileAdjuntada()!=null){
			this.showFotoUsuarioEnSala = true;
		}
		if (this.fotosLectorEnSala.getRawFileReniec()!=null){
			this.showFotoReniecEnSala = true;
		}

	}	
	
	public void ocultarSeccionFotosEnSala(){
		this.showFotoUsuarioEnSala			= false;
		this.showFotoReniecEnSala 		= false;
		this.showSeccionFotoEnSala = false;
		this.fotosLectorEnSala 			= null;		
		this.streamedContentPhotoReniecEnSala = null; 
		this.streamedContentPhotoAdjuntadaEnSala = null;
		this.tituloSeccionFotoLectorEnSala = Constants.EMPTY_STRING;
	}	

	public StreamedContent getStreamedContentPhotoAdjuntadaEnSala() {
		if (fotosLectorEnSala==null) return null;
		if (this.fotosLectorEnSala.getRawFileAdjuntada()!=null){
	        streamedContentPhotoAdjuntadaEnSala = new DefaultStreamedContent(new ByteArrayInputStream(this.fotosLectorEnSala.getRawFileAdjuntada()), Constants.FILE_CONTENT_TYPE_IMAGE_PREFIX+this.fotosLectorEnSala.getFileExtensionAdjuntada(), this.fotosLectorEnSala.getFileNameAdjuntada());
		}
        return streamedContentPhotoAdjuntadaEnSala;		
	}

	public StreamedContent getStreamedContentPhotoReniecEnSala() {
		if (this.fotosLectorEnSala.getRawFileReniec()!=null){
			streamedContentPhotoReniecEnSala = new DefaultStreamedContent(new ByteArrayInputStream(this.fotosLectorEnSala.getRawFileReniec()), Constants.FILE_CONTENT_TYPE_IMAGE_PREFIX+"jpg", "fotoreniec.jpg");
		}
        return streamedContentPhotoReniecEnSala;		
	}
		
	
	public void obtenerListasBase() {
		try {
			this.obtenerUsuarioEnSala();
			this.obtenerListaMaestroMotivosSalida();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public String actualizarPantallaControlAcceso(){
		return "home";
	}
	
	public void obtenerUsuarioEnSala() throws ServiceException{
		FiltroUsuarioEnSalaDto filtro = new FiltroUsuarioEnSalaDto();
		filtro.setIdSala(this.getUsuario().getSesionSalaUsuario().getSala().getIdSala());
		filtro.setEstadoAsistencia("Ingreso");
		this.listaUsuariosEnSala  = this.registroServiceRemote.listarUsuariosEnSala(filtro);
		//this.ocultarSeccionFotosEnSala();
	}	
	
	public boolean obtenerListaMaestroMotivosSalida() throws ServiceException{
		this.listaMaestraMotivosSalida  = this.registroServiceRemote.listarMaestroMotivosSalidaDeSala();
		return true;
	}	
	
	public boolean identificarUsuarioQueIngresaASala() throws ServiceException{
		this.limpiarRegistroAsistenciaSeleccionada();
		if ((VO.isEmpty(this.filtroNumeroDocumentoIdentidad.trim()))) {
			this.setAlertMessage("Ingrese un n�mero de documento v�lido.");
			return false;
		}
		this.filtroNumeroDocumentoIdentidad = this.filtroNumeroDocumentoIdentidad.trim().toUpperCase();
		this.listaBusquedaLectores = this.absysnetServiceRemote.buscarUsuarios(this.filtroNumeroDocumentoIdentidad);
		if (this.listaBusquedaLectores == null || this.listaBusquedaLectores.size()==0){
			super.setMensajeError("No se encontraron usuarios de tipo investigador o general para el n�mero de documento de identidad ingresado.");
		}
		if (this.listaBusquedaLectores != null && this.listaBusquedaLectores.size()==1){
			this.obtenerFotosLector(this.listaBusquedaLectores.get(0));
		}
		return true;
	}

	public void limpiarSeccionBusquedaUsuariosIngreso(){
		this.listaBusquedaLectores.clear();
		this.filtroNumeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.showFotoReniec			= false;
		this.showFotoUsuario 		= false;
		this.showSeccionFotoBusqueda = false;
		this.fotosLector 			= null;		
	}

	public boolean registrarIngresoASala(LectorConsultaAbsysnetDto lector) throws ServiceException{
		this.limpiarRegistroAsistenciaSeleccionada();
		lector.setIdSesionUsuario(this.getUsuario().getSesionSalaUsuario().getSesionUsuario().getIdSesionUsuario());
		TransaccionAsistenciaSala transaccion = null;
		transaccion = this.registroServiceRemote.registrarIngresoASala(lector);
		if (transaccion.getCodigoResultadoTransaccion().equals(Constants.CODIGO_EXITO_TRANSACCION)){
			this.listaUsuariosEnSala = transaccion.getListaUsuariosEnSala();
			this.limpiarSeccionBusquedaUsuariosIngreso();
			this.ocultarSeccionFotosEnSala();
			this.ocultarSeccionFotosBusqueda();
			super.setMensajeExito(transaccion.getDescripcionResultadoTransaccion());
		}else{
			if (transaccion.getCodigoResultadoTransaccion().equals("0001")){
				this.registrarLectorEnAbsysnet(lector);
			}else{
				super.setMensajeError(transaccion.getDescripcionResultadoTransaccion());
			}
		} 
		return true;
	}
	
	public boolean registrarLectorEnAbsysnet(LectorConsultaAbsysnetDto lector) throws ServiceException{
		if (lector.getFlagSiru().equals("NO")){
			//obtener los datos de absysnet
			LectorAbsysnetDto lectorAbsysnet =  absysnetServiceRemote.obtenerLectorAbsysnet(lector.getCodBarrasLector());
			if (lectorAbsysnet.getCodResultadoAbsysnet().equals(Constants.CODIGO_EXITO_TRANSACCION)){
				//registrarlo en SIRU
				ResponseObject respuesta = registroServiceRemote.registrarUsuarioMigradoDeAbsysnet(lectorAbsysnet);
				if (respuesta == null || respuesta.getMensaje() == null){
					super.setMensajeError("ERROR: Usuario ya se encuentra registrado en SIRU, verificar por n�mero de documento de identidad .");
					return false;
				}
				if (respuesta.getMensaje().equals(Constants.CODIGO_EXITO_TRANSACCION)){
					//marcar el lector en Absysnet
					ResponseObject respuestaAbsysnet = absysnetServiceRemote.actualizarLectorMigradoASIRU(lector.getCodBarrasLector());
					if (respuestaAbsysnet.getMensaje().equals(Constants.CODIGO_EXITO_TRANSACCION)){
						return this.registrarIngresoASala(lector);
					}else{
						super.setMensajeError("ERROR: No se pudo actualizar el estado del lector en Absysnet.");
						return false;
					}
					
				}else{
					super.setMensajeError("ERROR: No se pudo registrar el lector en SIRU.");
					return false;
				}
			}else{
				super.setMensajeError("ERROR: No se pudo obtener los datos del lector en Absysnet.");
				return false;
			}
		}else{
			super.setMensajeError("ERROR: No se encontr� datos del Lector en SIRU.");
		}
		return true;
	}
	
	public boolean registrarSalidadeSala(UsuarioEnSala usuario) throws ServiceException{
		this.limpiarRegistroAsistenciaSeleccionada();
		usuario.setIdSesionReferencistaRegistraSalida(this.getUsuario().getSesionSalaUsuario().getSesionUsuario().getIdSesionUsuario());
		usuario.setIdSalidaMotivo(Constants.ID_SALIDA_SALA_ESTANDAR);
		usuario.setEstadoAsistencia("Salida Estandar");
		TransaccionAsistenciaSala transaccion = null;
		transaccion = this.registroServiceRemote.registrarSalidaDeSala(usuario);
		this.listaUsuariosEnSala = transaccion.getListaUsuariosEnSala();
		if (transaccion.getCodigoResultadoTransaccion().equals(Constants.CODIGO_EXITO_TRANSACCION)){
			this.listaUsuariosEnSala = transaccion.getListaUsuariosEnSala();
			this.limpiarSeccionBusquedaUsuariosIngreso();
			this.ocultarSeccionFotosEnSala();
			this.ocultarSeccionFotosBusqueda();
			super.setMensajeExito(transaccion.getDescripcionResultadoTransaccion());
		}else{
			super.setMensajeError(transaccion.getDescripcionResultadoTransaccion());
		} 
		return true;
	}	
	

	public boolean regularizarSalidadeSala() throws ServiceException{
		if (this.usuarioSeleccionado != null){
			this.usuarioSeleccionado.setIdSesionReferencistaRegistraSalida(this.getUsuario().getSesionSalaUsuario().getSesionUsuario().getIdSesionUsuario());
			if (this.idMotivoSalida==0L){
				this.limpiarRegistroAsistenciaSeleccionada();
				super.setMensajeError("Debe seleccionar el motivo de la regularizaci�n");
				return false;
			}
			this.usuarioSeleccionado.setEstadoAsistencia("Salida Regularizada");
			this.usuarioSeleccionado.setIdSalidaMotivo(this.idMotivoSalida);
			this.usuarioSeleccionado.setSalidaJustificaci�n(this.justificacionMotivoSalida);
			TransaccionAsistenciaSala transaccion = null;
			transaccion = this.registroServiceRemote.registrarSalidaDeSala(this.usuarioSeleccionado);
			if (transaccion.getCodigoResultadoTransaccion().equals(Constants.CODIGO_EXITO_TRANSACCION)){
				this.listaUsuariosEnSala = transaccion.getListaUsuariosEnSala();
				this.limpiarSeccionBusquedaUsuariosIngreso();
				this.limpiarRegistroAsistenciaSeleccionada();
				super.setMensajeExito(transaccion.getDescripcionResultadoTransaccion());
			}else{
				super.setMensajeError(transaccion.getDescripcionResultadoTransaccion());
			} 
			return true;
		}else {
			super.setMensajeError("Debe seleccionar el usuario a regularizar");
			return false;
			
		}
	}	

	
	public boolean verVentanaRegularizacionSalida(UsuarioEnSala usuario) {
		this.limpiarRegistroAsistenciaSeleccionada();		
		this.usuarioSeleccionado = usuario;
		this.nombreUsuarioSeleccionado = this.usuarioSeleccionado.getNombreUsuario();
		requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("PF('dlgModalRegularizarSalida').show()");
		return true;
	}
	
	public void limpiarBusqueda(){
		this.filtroNumeroDocumentoIdentidad = Constants.EMPTY_STRING;
	}
	
	public void limpiarRegistroAsistenciaSeleccionada(){
		this.usuarioSeleccionado = null;
		this.idMotivoSalida = Constants.EMPTY_LONG;
		this.justificacionMotivoSalida = Constants.EMPTY_STRING;
		this.nombreUsuarioSeleccionado = Constants.EMPTY_STRING;
		this.showFotoReniec			= false;
		this.showFotoUsuario 		= false;
		this.showSeccionFotoBusqueda = false;
		this.fotosLector 			= null;		
		this.streamedContentPhotoAdjuntada = null;
		this.streamedContentPhotoReniec = null;
	}
	
	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public int getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}

	public String getFiltroNumeroDocumentoIdentidad() {
		return filtroNumeroDocumentoIdentidad;
	}

	public void setFiltroNumeroDocumentoIdentidad(
			String filtroNumeroDocumentoIdentidad) {
		this.filtroNumeroDocumentoIdentidad = filtroNumeroDocumentoIdentidad;
	}

	public void setStreamedContentPhotoReniec(
			StreamedContent streamedContentPhotoReniec) {
		this.streamedContentPhotoReniec = streamedContentPhotoReniec;
	}

	public void setStreamedContentPhotoAdjuntada(
			StreamedContent streamedContentPhotoAdjuntada) {
		this.streamedContentPhotoAdjuntada = streamedContentPhotoAdjuntada;
	}

	public List<LectorConsultaAbsysnetDto> getListaBusquedaLectores() {
		return listaBusquedaLectores;
	}

	public void setListaBusquedaLectores(
			List<LectorConsultaAbsysnetDto> listaBusquedaLectores) {
		this.listaBusquedaLectores = listaBusquedaLectores;
	}

	public List<UsuarioEnSala> getListaUsuariosEnSala() {
		return listaUsuariosEnSala;
	}

	public void setListaUsuariosEnSala(List<UsuarioEnSala> listaUsuariosEnSala) {
		this.listaUsuariosEnSala = listaUsuariosEnSala;
	}

	public List<MotivoSalida> getListaMaestraMotivosSalida() {
		return listaMaestraMotivosSalida;
	}

	public void setListaMaestraMotivosSalida(
			List<MotivoSalida> listaMaestraMotivosSalida) {
		this.listaMaestraMotivosSalida = listaMaestraMotivosSalida;
	}

	public UsuarioEnSala getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(UsuarioEnSala usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public Long getIdMotivoSalida() {
		return idMotivoSalida;
	}

	public void setIdMotivoSalida(Long idMotivoSalida) {
		this.idMotivoSalida = idMotivoSalida;
	}

	public String getJustificacionMotivoSalida() {
		return justificacionMotivoSalida;
	}

	public void setJustificacionMotivoSalida(String justificacionMotivoSalida) {
		this.justificacionMotivoSalida = justificacionMotivoSalida;
	}

	public String getNombreUsuarioSeleccionado() {
		return nombreUsuarioSeleccionado;
	}

	public void setNombreUsuarioSeleccionado(String nombreUsuarioSeleccionado) {
		this.nombreUsuarioSeleccionado = nombreUsuarioSeleccionado;
	}

	public boolean isShowFotoReniec() {
		return showFotoReniec;
	}

	public void setShowFotoReniec(boolean showFotoReniec) {
		this.showFotoReniec = showFotoReniec;
	}

	public boolean isShowFotoUsuario() {
		return showFotoUsuario;
	}

	public void setShowFotoUsuario(boolean showFotoUsuario) {
		this.showFotoUsuario = showFotoUsuario;
	}


	public boolean isShowSeccionFotoBusqueda() {
		return showSeccionFotoBusqueda;
	}


	public void setShowSeccionFotoBusqueda(boolean showSeccionFotoBusqueda) {
		this.showSeccionFotoBusqueda = showSeccionFotoBusqueda;
	}

	public boolean isShowSeccionFotoEnSala() {
		return showSeccionFotoEnSala;
	}

	public void setShowSeccionFotoEnSala(boolean showSeccionFotoEnSala) {
		this.showSeccionFotoEnSala = showSeccionFotoEnSala;
	}

	public boolean isShowFotoUsuarioEnSala() {
		return showFotoUsuarioEnSala;
	}

	public void setShowFotoUsuarioEnSala(boolean showFotoUsuarioEnSala) {
		this.showFotoUsuarioEnSala = showFotoUsuarioEnSala;
	}

	public boolean isShowFotoReniecEnSala() {
		return showFotoReniecEnSala;
	}

	public void setShowFotoReniecEnSala(boolean showFotoReniecEnSala) {
		this.showFotoReniecEnSala = showFotoReniecEnSala;
	}

	public void setStreamedContentPhotoReniecEnSala(
			StreamedContent streamedContentPhotoReniecEnSala) {
		this.streamedContentPhotoReniecEnSala = streamedContentPhotoReniecEnSala;
	}

	public void setStreamedContentPhotoAdjuntadaEnSala(
			StreamedContent streamedContentPhotoAdjuntadaEnSala) {
		this.streamedContentPhotoAdjuntadaEnSala = streamedContentPhotoAdjuntadaEnSala;
	}

	public String getTituloSeccionFotoLectorEnSala() {
		return tituloSeccionFotoLectorEnSala;
	}

	public void setTituloSeccionFotoLectorEnSala(
			String tituloSeccionFotoLectorEnSala) {
		this.tituloSeccionFotoLectorEnSala = tituloSeccionFotoLectorEnSala;
	}
	public MonitoreoEnSala getListaMonitoreoEnSala() {
		return listaMonitoreoEnSala;
	}
	public void setListaMonitoreoEnSala(MonitoreoEnSala listaMonitoreoEnSala) {
		this.listaMonitoreoEnSala = listaMonitoreoEnSala;
	}
	public SalasUsoActualDto getSalasUsoActualGrafico() {
		return salasUsoActualGrafico;
	}
	public void setSalasUsoActualGrafico(SalasUsoActualDto salasUsoActualGrafico) {
		this.salasUsoActualGrafico = salasUsoActualGrafico;
	}

	
	
}
